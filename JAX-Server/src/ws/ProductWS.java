package ws;

import java.sql.SQLException;
import java.util.List;

import javax.jws.WebService;

import dao.DAOProduct;
import entities.Product;

@WebService(endpointInterface = "ws.ProductInterface")
public class ProductWS implements ProductInterface{
	DAOProduct daoProd = new DAOProduct();

	@Override
	public Product findById(int id) {
		return daoProd.findById(id);
	}

	@Override
	public List<Product> findAll() {
		return daoProd.findAll();
	}

	@Override
	public Product findByIdSQL(int id) throws ClassNotFoundException, SQLException {
		return daoProd.findByIdSQL(id);
	}

	@Override
	public List<Product> findAllSQL() throws ClassNotFoundException, SQLException {
		return daoProd.findAllSQL();
	}

	
}
