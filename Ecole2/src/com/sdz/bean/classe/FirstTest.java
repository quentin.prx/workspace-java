package com.sdz.bean.classe;

import com.sdz.bean.Classe;
import com.sdz.bean.Eleve;
import com.sdz.bean.Matiere;
import com.sdz.bean.Professeur;
import com.sdz.connection.SdzConnection;
import com.sdz.dao.DAO;
import com.sdz.dao.implement.ClasseDAO;
import com.sdz.dao.implement.EleveDAO;
import com.sdz.dao.implement.ProfesseurDAO;

public class FirstTest { 
	  public static void main(String[] args) {
	    //Testons des �l�ves
	    DAO<Eleve> eleveDao = new EleveDAO(SdzConnection.getInstance());
	    for(int i = 1; i < 5; i++){
	      Eleve eleve = eleveDao.find(i);
	      System.out.println("El�ve N�" + eleve.getId() + "  - " + eleve.getNom() + " " + eleve.getPrenom());
	    }
	      
	    System.out.println("\n********************************\n");
	      
	    //Voyons voir les professeurs
	    DAO<Professeur> profDao = new ProfesseurDAO(SdzConnection.getInstance());
	    for(int i = 4; i < 8; i++){
	      Professeur prof = profDao.find(i);
	      System.out.println(prof.getNom() + " " + prof.getPrenom() + " enseigne : ");
	      for(Matiere mat : prof.getListMatiere())
	        System.out.println("\t * " + mat.getNom());
	    }
	      
	    System.out.println("\n********************************\n");
	      
	    //Et l�, c'est la classe
	    DAO<Classe> classeDao = new ClasseDAO(SdzConnection.getInstance());
	    Classe classe = classeDao.find(11);
	      
	    System.out.println("Classe de " + classe.getNom());
	    System.out.println("\nListe des �l�ves :");
	    for(Eleve eleve : classe.getListEleve())
	      System.out.println("  - " + eleve.getNom() + " " + eleve.getPrenom());
	      
	    System.out.println("\nListe des professeurs :");
	    for(Professeur prof : classe.getListProfesseur())
	      System.out.println("  - " + prof.getNom() + " " + prof.getPrenom());      
	  }
	}