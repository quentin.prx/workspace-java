package com.sdz.dao;

import java.sql.Connection;

import com.sdz.connection.SdzConnection;
import com.sdz.dao.AbstractDAOFactory;
import com.sdz.dao.DAO;
import com.sdz.dao.implement.ClasseDAO;
import com.sdz.dao.implement.EleveDAO;
import com.sdz.dao.implement.MatiereDAO;
import com.sdz.dao.implement.ProfesseurDAO;

//CTRL + SHIFT + O pour g�n�rer les imports
public class DAOFactory2 extends AbstractDAOFactory{
  protected static final Connection conn = SdzConnection.getInstance();   

  public DAO getClasseDAO(){
    return new ClasseDAO(conn);
  }

  public DAO getProfesseurDAO(){
    return new ProfesseurDAO(conn);
  }

  public DAO getEleveDAO(){
    return new EleveDAO(conn);
  }

  public DAO getMatiereDAO(){
    return new MatiereDAO(conn);
  }   
}