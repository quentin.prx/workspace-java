package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import DAO.DAOEnfant;
import modele.Enfant;

public class Test {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Enfant e = new Enfant("Poussin","Piou",3);
		DAOEnfant daoENfant = new DAOEnfant();
		
		daoENfant.create(e);
		e=daoENfant.findByName("Poussin");
		
		int idCreation = e.getId();
		
		System.out.println(daoENfant.findAll());
		
		System.out.println(daoENfant.findById(idCreation));
		Enfant e2 = daoENfant.findById(idCreation);
		e2.setNom("Archive");
		daoENfant.update(e2);
		System.out.println(daoENfant.findById(idCreation));
		
		daoENfant.delete(idCreation);
		
		System.out.println(daoENfant.findAll());
	}
	
	public static void statementQuery() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/SQLFormation", "root", "" );
		System.out.println("Connexion effectu�e");
		
		String sql = "SELECT * FROM enfant";
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		ResultSetMetaData rsmd = rs.getMetaData();
		
		
		List<Enfant> enfants = new ArrayList<Enfant>();
		System.out.println(rsmd.getColumnLabel(1)+" - "+rsmd.getColumnName(3)+" "+rsmd.getColumnName(2)+" "+rsmd.getColumnName(4));
		while(rs.next()) {
			System.out.println(rs.getInt("id")+" - "+rs.getString("prenom")+" "+rs.getString("nom")+" � "+rs.getInt("age")+ " ans !");
			System.out.println(rs.getInt(1)+" - "+rs.getString(3)+" "+rs.getString(2)+" � "+rs.getInt(4)+ " ans !");
			System.out.println(rs.getObject("id")+" - "+rs.getObject("prenom")+" "+rs.getObject("nom")+" � "+rs.getObject("age")+ " ans !");
			System.out.println(rs.getObject(1)+" - "+rs.getObject(3)+" "+rs.getObject(2)+" � "+rs.getObject(4)+ " ans !");
			enfants.add(new Enfant(rs.getInt("id"),rs.getString("nom"),rs.getString("prenom"),rs.getInt("age")));			
		}
		
		for(Enfant e : enfants) {
			System.out.println(e);
		}
		
		
		rs.close();
		st.close();
		conn.close();
	}
	
	
	public static void preparedStatementQuery(int id) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/SQLFormation", "root", "" );
		System.out.println("Connexion effectu�e");
		
		String sql = "SELECT * FROM enfant where id=?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		
		List<Enfant> enfants = new ArrayList<Enfant>();
		while(rs.next()) {
			enfants.add(new Enfant(rs.getInt("id"),rs.getString("nom"),rs.getString("prenom"),rs.getInt("age")));			
		}
		
		for(Enfant e : enfants) {
			System.out.println(e);
		}
		
		
		rs.close();
		conn.close();
	}
	
	
	public static void statementUpdate() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/SQLFormation", "root", "" );
		System.out.println("Connexion effectu�e");
		
		String sql = "INSERT INTO enfant (nom,prenom,age) VALUES ('YennaPas','Toto',5)";
		Statement st = conn.createStatement();
		st.executeUpdate(sql);
		
		
		st.close();
		conn.close();
	}

}
