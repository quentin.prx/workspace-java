package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.Connexion;
import modele.Enfant;

public class DAOEnfant implements DAO<Enfant, Integer> {

	
	public void create(Enfant e) throws ClassNotFoundException, SQLException {
		/*Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/SQLFormation", "root", "" );*/
		
		Connection conn = Connexion.getInstance();
		
        PreparedStatement ps=conn.prepareStatement("INSERT INTO enfant (nom,prenom,age) VALUES (?,?,?)");
        ps.setString(1,e.getNom());
        ps.setString(2,e.getPrenom());
        ps.setInt(3,e.getAge());
        
        ps.executeUpdate();
        ps.close();
        conn.close();
	}
	
	public Enfant findById(Integer id) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/SQLFormation", "root", "" );
		
		String sql = "SELECT * FROM enfant where id=?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		
		Enfant e=null;
		while(rs.next()) {
			e = new Enfant(rs.getInt("id"),rs.getString("nom"),rs.getString("prenom"),rs.getInt("age"));			
		}
				
		rs.close();
		ps.close();
		conn.close();
		return e;
	}
	
	public Enfant findByName(String nom) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/SQLFormation", "root", "");
		
		PreparedStatement ps=conn.prepareStatement("select * from enfant where nom=?");
		ps.setString(1,nom);
		ResultSet rs=ps.executeQuery();
		Enfant e=null;
		while(rs.next())
		{
			e=new Enfant(rs.getInt("id"),rs.getString("nom"),rs.getString("prenom"),rs.getInt("age"));
		}
		rs.close();
		ps.close();
		conn.close();
		return e;
	}
	
	public List<Enfant> findAll() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/SQLFormation", "root", "" );
		
		String sql = "SELECT * FROM enfant";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		List<Enfant> enfants = new ArrayList<Enfant>();
		while(rs.next()) {
			enfants.add(new Enfant(rs.getInt("id"),rs.getString("nom"),rs.getString("prenom"),rs.getInt("age")));			
		}
		
		rs.close();
		ps.close();
		conn.close();
		return enfants;
	}
	
	public void update(Enfant e) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/SQLFormation", "root", "" );		
		
        PreparedStatement ps=conn.prepareStatement("UPDATE enfant SET nom=?,prenom=?,age=? where id=?");
        ps.setString(1,e.getNom());
        ps.setString(2,e.getPrenom());
        ps.setInt(3,e.getAge());
        ps.setInt(4, e.getId());
        ps.executeUpdate();
        ps.close();
        conn.close();
	}
	
	public void delete(Integer id) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/SQLFormation", "root", "" );		
		
        PreparedStatement ps=conn.prepareStatement("DELETE FROM enfant where id=?");
        ps.setInt(1,id);
        ps.executeUpdate();
        ps.close();
        conn.close();
	}

	
}
