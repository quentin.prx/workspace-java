package connection;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connexion {
	  private static String url = "jdbc:mysql://localhost:3306/SQLFormation";
	  private static String user = "root";
	  private static String passwd = "";
	  private static Connection connect;
	  
	   public static Connection getInstance(){
	    if(connect == null){
	        try {
	        	Class.forName("com.mysql.jdbc.Driver");
	            connect = DriverManager.getConnection(url, user, passwd);
	          } catch (SQLException e) {
	            e.printStackTrace();
	          } catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
	    }
	    return connect;   
	  }   
}
