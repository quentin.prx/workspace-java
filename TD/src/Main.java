import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;


public class Main {

	static Scanner scInt = new Scanner(System.in);
	static Scanner scString = new Scanner(System.in);
	static Scanner scDouble = new Scanner(System.in);
	
	public static void tp1_1(){
		int max = 10, min = 30;
		System.out.println("Min vaut " + min +",  Max vaut " + max);
		
		int temp;
		temp = max;
		max = min;
		min = temp;
		
		System.out.println("Min vaut " + min +",  Max vaut " + max);
	}
	
	public static void tp1_2() {
	int max = 10,med = 5, min = 30;
	System.out.println("Min vaut " + min +",  Max vaut " + max + ", Med vaut "+med);
	
	int temp;
	temp = min;
	min = med;
	med = temp;
	temp = max;
	max = min;
	min = temp;
	
	System.out.println("Min vaut " + min +",  Max vaut " + max + ", Med vaut "+med);
	
	}
	
	public static void tp2_1() {
		System.out.println("Entrer un nombre : ");
		Scanner sc = new Scanner(System.in);
		int carre = sc.nextInt();
		System.out.println("carr� vaut " + carre + ", carre^2 vaut " + carre*carre);
	}
	
	public static void tp2_2() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrer un prix HT : ");
		double prix = sc.nextDouble();
		System.out.println("Entrer un nombre d'article : ");
		int nbArticle = sc.nextInt();
		System.out.println("Entrer la TVA : ");
		double tva = sc.nextDouble();
		
		double tauxTVA = 1 + (tva/100);
		double prixTVA = (tva/100)*prix*nbArticle;
		
		System.out.println("Article 1 : " + prix + "� HT * " + nbArticle);
		System.out.println("Prix total : " + prix*(tauxTVA)*nbArticle + "� TTC");
		System.out.println("dont : " + prixTVA + "� de TVA");
	}
	
	public static double add(double a, double b) {
		return a + b;
	}
	
	public static double minus(double a, double b) {
		return a - b;
	}
	
	public static double mul(double a, double b) {
		return a * b;
	}
	
	public static double div(double a, double b) {
		return a / b;
	}
	
	public static void produit() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrer un nombre : ");
		double nb1 = sc.nextDouble();
		if(nb1<0)
			System.out.println("N�gatif");
		else if(nb1>0)
			System.out.println("Positif");
		else
			System.out.println("Nul");
		
		System.out.println("Entrer un deuxi�me nombre : ");
		double nb2 = sc.nextDouble();
		
		if((nb1<0 && nb2<0) || (nb1>0 && nb2>0))
			System.out.println("Positif");
		else if((nb1 < 0 && nb2 > 0) || (nb1>0 && nb2 <0))
			System.out.println("N�gatif");
		else
			System.out.println("Nul");
	}
	
	public static void categorie() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrer votre �ge : ");
		int age = sc.nextInt();
		
		if(age <= 5)
			System.out.println("Autre");
		else if(age <= 7)
			System.out.println("Poussin");
		else if(age <= 8)
			System.out.println("Pupille");
		else if(age <= 11)
			System.out.println("Minime");
		else
			System.out.println("Cadet");
	}
	
	public static void plusUneMinute() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez l'heure");
		int heure = sc .nextInt();
		System.out.println("Entrez la minute");
		int minute = sc.nextInt();
		minute++;
		if(minute >= 60) {
			heure++;
			if(heure >= 24)
				System.out.println("Il est 24:00");
			else
				System.out.println("Il est "+heure+":00");
		}else
			System.out.println("Il est "+heure+":"+minute);
	}
	
	public static void plusUneSeconde() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez l'heure");
		int heure = sc .nextInt();
		System.out.println("Entrez la minute");
		int minute = sc.nextInt();
		System.out.println("Entrez la seconde");
		int seconde = sc.nextInt();
		seconde++;
		if(seconde >= 60) {
			minute++;
			if(minute >= 60) {
				heure++;
				if(heure >= 24)
					System.out.println("Il est "+"00:00:00");
				else
					System.out.println("Il est "+heure+"00:00");
			}else {
				System.out.println("Il est "+heure+":"+minute+":00");
			}
		}else {
			System.out.println("Il est "+heure+":"+minute+":"+seconde);
		}
	}
	
	public static void prixPhtocopie() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez un nombre de photocopie");
		int nbPhoto = sc.nextInt();
		double prix = 0.0;
		
		if(nbPhoto <= 10) {
			prix = nbPhoto*0.10;
		}else if(nbPhoto <= 20 && nbPhoto >= 10)
			prix = 10*0.10 + (nbPhoto-10)*0.09;
		else
			prix = 10*0.10 + 20*0.09 + (nbPhoto-30)*0.08;
		
		System.out.println("Pour "+nbPhoto+" photocopies il faudra d�bourser "+prix+"�");
	}
	
	public static void imposable() {
		Scanner scInt = new Scanner(System.in);
		Scanner scString = new Scanner(System.in);
		
		System.out.println("Entrez votre age");
		int age = scInt.nextInt();
		System.out.println("Entrez votre sexe");
		String sexe = scString.nextLine();
		sexe.toUpperCase();
		if((age >= 20 && sexe.equals("homme")) || (age >= 18 && age <= 35 && sexe.equals("femme")))
			System.out.println("Imposable");
		else
			System.out.println("Pas imposable");
	}
	
	public static void election() {
		ArrayList<Integer> candidats = new ArrayList<>();
		Scanner sc = new Scanner(System.in);
		int nbVoix = 0;
		
		for(int i=0;i<4;i++) {
			System.out.println("Nombre de voix candidat n�"+(i+1));
			int nb = sc.nextInt();
			nbVoix += nb;
			candidats.add(nb);
		}
		
		double pc1 = ((double)candidats.get(0) / (double)nbVoix)*100.0;
		System.out.println(pc1);
		int pos = 1;
		if(pc1 >= 50.0) {
			System.out.println("Elu au premier tour");
		}else if (pc1 > 12.5) {
			boolean elimine = false;
			for(int i=1;i<candidats.size();i++) {
				System.out.println("voix "+(double)candidats.get(i)/2.0);
				if((double)candidats.get(i)>=(double)nbVoix/2.0)
					elimine = true;
			}
			
			if(!elimine) {
				System.out.println("Est au deuxi�me tour");
				for(int i=1;i<candidats.size();i++) {
					if(pc1 < ((double)candidats.get(i)/(double)nbVoix)*100.0)
						pos++;
				}
				if(pos != 1) {
					System.out.println("Est au deuxi�me tour en position d�favorable ("+pos+")");
				}else {
					System.out.println("Est au deuxi�me tour en position favorable");
				}
			}else {
				System.out.println("Elimine un candidat a plus de 50% des voix");
			}
		}else {
			System.out.println("Est �l�min�");
		}
	}
	
	public static void assurance() {
		Scanner scInt = new Scanner(System.in);
		System.out.println("Entrez votre age");
		int age  = scInt.nextInt();
		System.out.println("Entrez la dur�e du permis");
		int permis = scInt.nextInt();
		System.out.println("Entrez le nombre d'accidnets");
		int nbAccident = scInt.nextInt();
		System.out.println("Entrez la dur�e d'adh�rence");
		int adh = scInt.nextInt();
		
		String couleur = "";
		if(age>=25 && permis > 2 && nbAccident == 0) {
			couleur = "vert";
		}else if((((age<25  && permis > 2) || (age>=25 && permis<2)) && nbAccident == 0) || (age>=25 && permis > 2 && nbAccident == 1)) {
			couleur = "orange";
		}else if((age<25 && permis<2 && nbAccident == 0) && ((((age<25  && permis > 2) || (age>=25 && permis<2)) && nbAccident == 1) || (age>=25 && permis > 2 && nbAccident == 2)) ){
			couleur = "rouge";
		}else {
			couleur = "recal�";
		}
		
		if(adh>5) {
			switch(couleur) {
				case "vert":
					couleur = "bleu";
					break;
				case "orange":
					couleur = "vert";
					break;
				case "rouge":
					couleur = "orange";
					break;
				default:
					couleur ="recal�";
			}
		}
		System.out.println(couleur);
	}
	

	public static void assurance2() {
		Scanner scInt = new Scanner(System.in);
		System.out.println("Entrez votre age");
		int age  = scInt.nextInt();
		System.out.println("Entrez la dur�e du permis");
		int permis = scInt.nextInt();
		System.out.println("Entrez le nombre d'accidnets");
		int nbAccident = scInt.nextInt();
		System.out.println("Entrez la dur�e d'adh�rence");
		int adh = scInt.nextInt();
		
		String couleur = "";
		int score;
		
		if(age>=25 && permis > 2) {
			score = 3 - nbAccident;
		}else if((age<25  && permis > 2) || (age>=25 && permis<2)) {
			score = 2 - nbAccident;
		}else if((age<25 && permis<2 && nbAccident == 0) && ((((age<25  && permis > 2) || (age>=25 && permis<2)) && nbAccident == 1) || (age>=25 && permis > 2 && nbAccident == 2)) ){
			score = 1 - nbAccident;
		}else {
			score = 0;
		}
		
		if(adh>5 && score >0) {
			score++;
		}
		
		switch(score) {
		case 4:
			couleur = "bleu";
			break;
		case 3:
			couleur = "vert";
			break;
		case 2:
			couleur = "orange";
			break;
		case 1:
			couleur = "rouge";
			break;
		default:
			couleur ="recal�";
		}
		System.out.println(couleur);
	}
	
	public static boolean dateValide() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Jour");
		int jour = sc.nextInt();
		System.out.println("Mois");
		int mois = sc.nextInt();
		System.out.println("annee");
		int annee = sc.nextInt();
		
		boolean valide = true;
		
		if(jour > 0 && jour<=31 && mois > 0 && annee > 0) {
			
			 if(mois == 5 || mois == 6 || mois == 9 || mois == 11){
				 if(jour > 30)
					 valide = false;
			}else if(mois == 2) {
				if(annee%400==0 || (annee%100!=0 && annee%4==0)) {
					if(jour > 29)
						valide = false;
				}else {
					if(jour > 28)
						valide = false;
				}
			}else {
				valide = true;
			}
		}else {
			valide = false;
		}
		
		return valide;
	}
	
	public static void rand() {
		Random r = new Random();
		int x = r.nextInt(3) +1;
		
	}
	
	public static void boucle1a3() {
		Scanner sc = new Scanner(System.in);
		boolean notOk = true;
		int nb = 0;
		do {
			System.out.println("Entrez un nombre entre 1 et 3");
			nb = sc.nextInt();
			if(nb >=1 && nb<=3)
				notOk = false;
			else
				System.out.println("Le nombre saisi n'est pas compris entre 1 et 3");
		}while(notOk);
	}
	
	public static void boucle10a20() {
		Scanner sc = new Scanner(System.in);
		boolean notOk = true;
		int nb = 0;
		do {
			System.out.println("Entrez un nombre entre 10 et 20");
			nb = sc.nextInt();
			if(nb < 10) {
				System.out.println("Plus grand");
			}else if(nb > 20) {
				System.out.println("Plus petit");
			}else {
				notOk = false;
			}
		}while(notOk);
	}
	
	public static void les10Prochains() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez un nombre");
		double n = sc.nextDouble();
		System.out.println("Les 10 prochaines nombre apr�s : "+n+" sont : ");
		int i = 1;
		while(i<=10) {
			n++;
			System.out.println(n);
			i++;
		}
		
		System.out.println("Avec do while");
		int j = 1;
		do {
			n++;
			System.out.println(n);
			j++;
		}while(j<=10);
		
		System.out.println("Avec la boucle For");
		for(int k=0;k<10;k++) {
			n++;
			System.out.println(n);
		}
	}
	
	public static void tableMultiplicaion() {
		System.out.println("Entrez un nombre");
		int nb = scInt.nextInt();
		for(int i=0;i<=10;i++)
			System.out.println(nb+"*"+i+"="+(nb*i));
	}
	
	public static void nombrePlusGrand() {
		int nbMax = 0;
		int numMax = 0;
		int nb;
		for(int i=1;i<=200;i++) {
			System.out.println("Entrer le nombre n�"+i);
			Random r = new Random();
			//nb = scInt.nextInt();
			nb = r.nextInt(2000)+1;
			System.out.println("Le nombre est "+nb);
			if(nb>=nbMax) {
				nbMax=nb;
				numMax = i;
			}
		}
		System.out.println("Le plus grand nombre est "+nbMax);
		System.out.println("C'est le nombre "+numMax);
	}
	
	public static void courseChevaux() {
		System.out.println("Entrez le nombre de chevaux en lice");
		int n = scInt.nextInt();
		System.out.println("Entrez le nombre de chevaux jou�");
		int p = scInt.nextInt();
		
		
		int facN=1, facP=1, facNmoinsP=1;
		
		for(int i=1;i<=n;i++) {
			facN *= i;
		}
		for(int i=1;i<=p;i++) {
			facP *= i;
		}
		for(int i=1;i<=(n-p);i++) {
			facNmoinsP *= i;
		}
		
		double X = (double)facN/(double)facNmoinsP;
		double Y = (double)facN/(double)((double)facP*facNmoinsP);
		System.out.println("Dans l'ordre : une chance sur "+X+" de gagner");
		System.out.println("Dans le d�sordre : une chance sur "+Y+" de gagner");
	}
	
	public static void remplirTabA0() {
		ArrayList<Integer> tab = new ArrayList<>();
		int i=0;
		while(i<6) {
			tab.add(0);
			i++;
		}
		System.out.println(tab);
		
		System.out.println("Avec un tableau : ");
		int tabBis[] = new int[6];
		for(int j=0;j<tabBis.length;j++)
			tabBis[j] = 0;
		
		for(int j=0;j<tabBis.length;j++)
			System.out.println(tabBis[j]);
	}
	
	public static ArrayList<Double> note() {
		ArrayList<Double> notes = new ArrayList<>();
		int i = 0;
		while(i<5) {
			System.out.println("Entrez une note :");
			notes.add(scDouble.nextDouble());
			i++;
		}
		System.out.println(notes);
		
		System.out.println("Avec un tableau");
		double notesBis[] = new double[5];
		i = 0;
		while(i<5) {
			System.out.println("Entrez une note :");
			notesBis[i] = scDouble.nextDouble();
			i++;
		}
		
		for(int j=0;j<notesBis.length;j++)
			System.out.println(notesBis[j]);
		
		return notes;
	}
	
	public static double[] noteBis() {
		
		System.out.println("Avec un tableau");
		double notesBis[] = new double[5];
		for(int j=0;j<notesBis.length;j++)
			notesBis[j] = scDouble.nextDouble();
		for(int j=0;j<notesBis.length;j++)
			System.out.println(notesBis[j]);
		
		return notesBis;
	}
	
	public static void somme2Tab() {
		System.out.println("Entrer la taille des tableaux");
		int taille = scInt.nextInt();
		ArrayList<Integer> tab1 = new ArrayList<>();
		ArrayList<Integer> tab2 = new ArrayList<>();
		ArrayList<Integer> tabRes = new ArrayList<>();
		Random r = new Random();
		
		int i = 0;
		while(i<taille) {
			tab1.add(r.nextInt(20));
			tab2.add(r.nextInt(20));
			i++;
		}
		
		for(int j=0;j<tab1.size();j++) {
			tabRes.add(tab1.get(j)+tab2.get(j));
		}
		
		System.out.println(tab1);
		System.out.println("+");
		System.out.println(tab2);
		System.out.println("=");
		System.out.println(tabRes);
	}
	
	public static void somme2TabBis() {
		System.out.println("Entrer la taille des tableaux");
		int taille = scInt.nextInt();
		int tab1[] = new int[taille];
		int tab2[] = new int[taille];
		int tabRes[] = new int[taille];
		Random r = new Random();
		
		int i = 0;
		while(i<taille) {
			tab1[i] = r.nextInt(10);
			tab2[i]= r.nextInt(10);
			i++;
		}
		
		for(int j=0;j<taille;j++) {
			tabRes[j] = tab1[j]+tab2[j];
		}
		
		String ch = "";
		for(int j=0;j<taille;j++) {
			ch += (ch.equals("") ? "[":",") + tab1[j] + ((j+1==taille)?"]":"");
		}
		

		String ch2 = "";
		for(int j=0;j<taille;j++)
			ch2 += (ch2.equals("") ? "[":",") + tab2[j] + ((j+1==taille)?"]":"");
		
		String ch3 = "";
		for(int j=0;j<taille;j++)
			ch3 += (ch3.equals("") ? "[":",") + tabRes[j] + ((j+1==taille)?"]":"");

		
		System.out.println(ch);
		System.out.println("+");
		System.out.println(ch2);
		System.out.println("=");
		System.out.println(ch3);
	}
	
	public static void sommeTab(ArrayList<Double> notes) {
		double somme = 0;
		for(int i=0;i<notes.size();i++) {
			somme += notes.get(i);
		}
		
		System.out.println("La somme des nombres du tableau est "+somme);
	}
	
	public static void sommeTabBis(double[] notes) {
		double somme = 0;
		for(int i=0;i<notes.length;i++) {
			somme += notes[i];
		}
		
		System.out.println("La somme des nombres du tableau est "+somme);
	}
	
	public static void afficherTab(int tab[]) {
		String ch = "";
		for(int n : tab)
			ch  += n + " ";
		System.out.println(ch);
	}
	
	public static void trierTableau1() {
		int tab[] = new int[5];
		
		Random r = new Random();
		
		for(int i =0;i<tab.length;i++)
			tab[i] = r.nextInt(21);
		
		System.out.println("Tab avant tri");
		afficherTab(tab);
		
		//Tri tableau par bulle
		int max = 0;
		int cpt = 0;
		for(int i=0;i<tab.length;i++) {
			for(int j=i+1;j<tab.length;j++) {
				if(tab[i]>tab[j]) {
					max = tab[j];
					tab[j] = tab[i];
					tab[i] = max;
				}
				System.out.println("Tour "+ ++cpt);
				afficherTab(tab);				
			}
		}		
		
		System.out.println("Tab apr�s tri");
		afficherTab(tab);
	}
	
	public static void trierTableau2() {
		int tab[] = new int[20];
		
		Random r = new Random();
		
		for(int i =0;i<tab.length;i++)
			tab[i] = r.nextInt(21);
		
		String ch = "";
		System.out.println("Tab avant tri");
		for(int n : tab)
			ch += n + " ";
		System.out.println(ch);
		
		
		//tri par selection
		int min;
		int tmp;
		for(int i=0;i<tab.length;i++) {
			min = i;
			for(int j=i+1;j<tab.length;j++) {
				if(tab[min]>tab[j]) {
					min = j;
				}
			}
			if(min != i) {
				tmp = tab[i];
				tab[i] = tab[min];
				tab[min] = tmp;
				
			}
		}
		
		ch = "";
		System.out.println("Tab apr�s tri");
		for(int n : tab)
			ch += n + " ";
		System.out.println(ch);
	}
	
	public static void justePrix() {
		System.out.println("Bienvenue au Juste Prix");
		String regles = "";
		regles += "Tapez le num�ro du mode de jeu que vous voulez jouer : ";
		regles += "\n1 - Facile (nombre entre 1 et 100) avec 10 vies";
		regles += "\n2 - Moyenne (nombre entre 1 et 1 000) avec 15 vies";
		regles += "\n3 - Difficile (nombre entre 1 et 100 000) avec 20 vies";
		regles += "\n4 - Diabolique (les bornes sont al�atoires) avec 20 vies";
		
		System.out.println(regles);
		int niveau = scInt.nextInt();
		while(niveau <=0 || niveau > 4) {
			System.out.println("Le niveau n'existe pas ! ");
			System.out.println(regles);
			niveau = scInt.nextInt();
		}
		
		int bInf=0, bSup=0, nbVie=0, nb=0;
		
		switch(niveau) {
			case 1:
				bInf = 1;
				bSup = 100;
				nbVie = 10;
				break;
			case 2:
				bInf = 1;
				bSup = 1000;
				nbVie = 15;
				break;
			case 3:
				bInf = 1;
				bSup = 100000;
				nbVie = 20;
				break;
			case 4:
				Random r = new Random();
				bInf = r.nextInt(1000000);
				do {
					bSup = r.nextInt(1000000);
				}while(bSup<=bInf);
				nbVie = 20;
				break;
			default:
					System.out.println("Erreur !!");
		}
		
		Random r = new Random();
		int al = r.nextInt(bSup-bInf)+bInf;
		boolean fin = false;
		while(!fin) {
			System.out.println("Taper un nombre ("+ nbVie +" restantes) : ");
			nb = scInt.nextInt();
			if(nb < al) {
				System.out.println("Plus grand");
			}else if(nb > al) {
				System.out.println("Plus petit");
			}else {
				fin=true;
			}
			
			nbVie--;
			if(nbVie<0) {
				fin = true;
				System.out.println("Le nombre �tait : "+al);
			}
		}
		
		if(nbVie<0)
			System.out.println("Perdu");
		else
			System.out.println("Gagn�");
		
	}
	
	public static int[][] matrice(){
		System.out.println("Entrez un nombre de ligne :");
		int l = scInt.nextInt();
		System.out.println("Entrez un nombre de colonne : ");
		int c = scInt.nextInt();
		
		int mat[][] = new int[l][c];
		int cpt = 0;
		
		for(int i=0;i<mat.length;i++) {
			for(int j=0;j<mat[i].length;j++) {
				//System.out.println("Entrez un chiffre pour l'indice ("+i+","+j+")");
				//mat[i][j] = scInt.nextInt();
				//mat[i][j] = i+j;
				mat[i][j] = cpt++;
			}
		}
		
		return mat;
 	}
	
	public static void afficherMatriceL(int mat[][]) {
		String ch = "";
		for(int l[] : mat) {
			for(int n : l) {
				ch += n + " ";
			}
			ch += "\n";
		}
		System.out.println(ch);
	}
	
	public static void afficherMatriceC(int mat[][]) {
		String ch="";
		
		for(int i=0;i<mat.length;i++) {
			for(int j=0;j<mat[i].length;j++) {
				ch += mat[j][i] + " ";
			}
			ch += "\n";
		}
		
		System.out.println(ch);
	}
	
	public static void choixExo() {
	
		boolean restart = false;
		
		do {
			Scanner sc = new Scanner(System.in);
			System.out.println("Quelle fonction est la fonction que vous voulez ex�cuter ? ");
			System.out.println("1 - tp1_1");
			System.out.println("2 - tp1_2");
			System.out.println("3 - tp2_1");
			System.out.println("4 - tp2_2");
			System.out.println("5 - produit");
			System.out.println("6 - cat�gorie");
			System.out.println("7 - plus une minute");
			System.out.println("8 - plus une seconde");
			System.out.println("9 - prix photocopie");
			System.out.println("10 - impossable");
			System.out.println("11 - election");
			System.out.println("12 - assurance");
			System.out.println("13 - assurance 2");
			System.out.println("14 - date valide");
			System.out.println("15 - boucle 1 a 3");
			System.out.println("16 - boucle 10 a 20");
			System.out.println("17 - les10Prochaines");
			System.out.println("18 - table multiplication");
			System.out.println("19 - nombre plus grand");
			System.out.println("20 - course de chevaux");
			System.out.println("21 - remplir tab 0");
			System.out.println("22 - note");
			System.out.println("23 - sommeTab");
			System.out.println("24 - somme 2 tableaux");
			System.out.println("25 - noteBis");
			System.out.println("26 - sommeTab bis");
			System.out.println("27 - somme 2 tableaux bis");
			System.out.println("Autre - Arr�ter le programme");
			
			String choix = sc.nextLine();
			
			switch(choix) {
				case "1":
					tp1_1();
					break;
				case "2":
					tp1_2();
					break;
				case "3":
					tp2_1();
					break;
				case "4":
					tp2_2();
					break;
				case "5":
					produit();
					break;
				case "6":
					categorie();
					break;
				case "7":
					plusUneMinute();
					break;
				case "8":
					plusUneSeconde();
				case "9":
					prixPhtocopie();
					break;
				case "10":
					imposable();
					break;
				case "11":
					election();
					break;
				case "12":
					assurance();
					break;
				case "13":
					assurance2();
					break;
				case "14":
					System.out.println(dateValide());
					break;
				case "15":
					boucle1a3();
					break;
				case "16":
					boucle10a20();
					break;
				case "17":
					les10Prochains();
					break;
				case "18":
					tableMultiplicaion();
					break;
				case "19":
					nombrePlusGrand();
					break;
				case "20":
					courseChevaux();
					break;
				case "21":
					remplirTabA0();
					break;
				case "22":
					note();
					break;
				case "23":
					sommeTab(note());
					break;
				case "24":
					somme2Tab();
					break;
				case "25":
					noteBis();
					break;
				case "26":
					sommeTabBis(noteBis());
					break;
				case "27":
					somme2TabBis();
					break;
				default:
					restart = false;
					
			}
			
			/*if(choix.equals("1"))
				tp1_1();
			else if(choix.equals("2"))
				tp1_2();
			else if(choix.equals("3"))
				tp2_1();
			else if(choix.equals("4"))
				tp2_2();
			else
				restart = false;*/	
		}while(restart);
	}
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello word!");

		//choixExo();
		
		/*System.out.println("Afficher par ligne");
		afficherMatriceL(matrice());
		
		System.out.println("Afficher par colonne");
		afficherMatriceC(matrice());*/
		
		justePrix();
	}

}
