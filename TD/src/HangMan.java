import java.util.Scanner;

public class HangMan {

	static Scanner scStr = new Scanner(System.in);

	// D�finition des variables statiques
	static int curRound = 1;
	static int maxRound = 11;
	static int hntRound = 8;
	static int missing  = 0;
	static String letters = "";
	static String hang = "";
	static String secrets[]  = {"", ""};
	static String alphabet[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

	
	// [OK] Demande du mot � trouver et de l'indice
	public static void secretWord() {

		// Choix du mot � trouver
		while(secrets[0].equals("")) {
			System.out.print("Choisissez un mot : ");
			secrets[0] = scStr.nextLine().toUpperCase();
			missing = missingLetters();
		}

		// Choix de l'indice pour aider
		while(secrets[1].equals("")) {
			System.out.print("D�finissez un indice : ");
			secrets[1] = scStr.nextLine();
		}
		
		clearScreen();
		
	}


	// [OK] On traite la lettre envoy�e par l'utilisateur
	public static String askLetter() {

		// D�fintion des variables
		String letter = "";
		
		// Demande de la letter � l'utilisateur
		System.out.println("Quelle lettre proposez vous ? ");
		letter = scStr.nextLine().toUpperCase();
		
		if (letter.length() == 0) {
			System.out.println("Vous ne devez renter au moins une lettre.");
			letter = "";
		} else if (letter.length() != 1) {
			System.out.println("Vous ne devez renter qu'une seule lettre.");
			letter = "";
		} else if (inArray(alphabet, letter) == false) {
			System.out.println("Lettre inconnue dans l'alphabet.");
			letter = "";
		} else if (inArray(string2Array(letters), letter) == true) {
			System.out.println("Vous avez d�j� propos� cette lettre.");
			letter = "";
		}

		letters = letters + letter;
		
		return letter;
		
	}
	

	// [OK] On affiche ce qu'on doit faire deviner
	public static void maskedSecret() {

		// D�finition des variables
		String str = "";
		String[] secret = string2Array(secrets[0]);

		for (int i = 0; i < secret.length; i++)
			str += (!str.equals("") ? " " : "") + (secret[i].equals(" ") ? " " : (inArray(string2Array(letters), secret[i]) == true) ? secret[i] : "_");

		System.out.print(str + "\r\n");
		
	}
	

	// [OK] On compte le nombre de lettres qu'on n'a pas encore d�couvert
	public static int missingLetters() {

		// D�finition des variables
		String tmp = "";
		String[] secret = string2Array(secrets[0]);

		for (int i = 0; i < secret.length; i++) {
			if (inArray(string2Array(letters), secret[i]) == true) continue;
			else tmp += secret[i];
		}

		return tmp.length();
		
	}
	

	// [OK] Listing des lettres jou�es
	public static String listLetters() {

		String str = "";
		String[] liste = string2Array(letters);
		
		if (liste.length == 0) str = "Aucune !";
		else
			for (int i = 0; i < liste.length; i++) str += (!str.equals("") ? ", " : "") + liste[i];

		return str;		

	}

	
	// [OK] Afficher le message de retour
	public static void callResults(boolean victory) {
		
		if (victory == true) {
			System.out.println("F�licitations ! Tu as trouv� la r�ponse : " + secrets[0]);
		} else {
			hang += "  ______  \r\n"
				 +  "  |/   |  \r\n"
				 +  "  |    o  \r\n"
				 +  "  |   /|\\ \r\n"
				 +  "  |   / \\ \r\n"
				 +  "__|__     \r\n";
			System.out.println("You lose!\r\n" + hang);
			System.out.println("La r�ponse �tait " + secrets[0]);
		}
		
	}
	
	
	// [OK] Phase de jeu
	public static void letsPlay() {
		
		// D�finition des variables
		boolean victoire = false;
		
		// Action pour chaque round 
		while (curRound < maxRound) {
			
			String letter = "";
			System.out.print("Round " + curRound + ". Plus que " + (maxRound - curRound) + ".");
			System.out.println(" Lettre d�j� propos�es : " + listLetters());
			if (curRound == hntRound) System.out.println("\t\tPetit indice : " + secrets[1]);
			maskedSecret();

			// On demande quelle lettre au joueur
			while(letter.length() != 1)	letter = askLetter();

			// On v�rifie que la lettre fait partie de la r�ponse
			if (inArray(string2Array(secrets[0]), letter) == true) {
				System.out.println("Bravo, tu as trouv� le " + letter + ".");
				if (missingLetters() == 0) {
					victoire = true;
					break;
				}
			} else {
				System.out.println("D�sol�, le " + letter + " ne fait pas partie de la solution !");
				curRound++;
			}

		}

		callResults(victoire);
		
	}
	
	
	// ####################################################################
	// ##### MAIN #########################################################
	// ####################################################################
	public static void main(String[] args) {

		// D�finition du mot � trouver et de son indice
		secretWord();
		
		// Jouons !
		letsPlay();
			
	}

	// string2Array
	public static String[] string2Array(String str) {
		String[] array = new String[str.length()];
		array = str.split("");
		return array;
	}
	
	// inArray
	public static boolean inArray(String[] haystack, String needle) {
		boolean inArray = false;
		for (int i = 0; i < haystack.length; i++) {
			if (haystack[i].equals(needle)) {
				inArray = true;
				break;
			}
		}
		return inArray;
	}

	// clearScreen
	public static void clearScreen() {  
		for(int clear = 0; clear < 1000; clear++) {
			System.out.println("\r\n") ;
		}  
	} 
	
}
