import java.util.Random;
import java.util.Scanner;

public class MasterMind {

	static Scanner scInt = new Scanner(System.in);
	static Scanner scStr = new Scanner(System.in);
	static Scanner scDbl = new Scanner(System.in);
	
	// [OK] R�cup�ration des diff�rentes couleurs
	public static String[] getColors(String lvl) {
		
		// D�finition des variables
		String[] colors = new String[0];
		
		switch (lvl) {
			case "N":
				colors = new String[]{"Jaune", "Bleu", "Rouge", "Vert", "Blanc", "Noir"};
				break;

			case "M": 
				colors = new String[]{"Jaune", "Bleu", "Rouge", "Vert", "Blanc", "Rose", "Violet", "Orange"};
				break;
		}
		
		return colors;
	}

	// [OK] Demande du mode de jeu
	public static String[] gameMode() {
		
		// D�finition des variables
		String[] config = new String[2];
		
		// Demande du niveau de jeu	
		System.out.println("Quel niveau de jeu voulez-vous ? Normal (N) ou Master (M) ?");
		config[0] = scStr.nextLine().toUpperCase();
		
		// Demande contre qui on joue
		System.out.println("Contre qui jouez-vous ? Humain (H) ou Ordinater (C) ?");
		config[1] = scStr.nextLine().toUpperCase();

		return config;
		
	}
	
	// [OK] Cr�ation de la phrase secr�te en fonction du niveau de jeu
	public static String[] getSecret(String[] col, String vrs, int max) {

		// D�finition des variables
		String[] secret = new String[max];
		String   colors = listColors(col), values;
		
		if (vrs.equals("H")) {
			
			for (int i = 0; i < max; i++) {
				System.out.println("Couleur " + (i + 1) + "/" + max + " : (Pour rappel, couleurs disponibles : " + colors + ")");
				values = scStr.nextLine();					
				if (inArray(col, values) == false) {
					System.out.println("Erreur : La couleur `" + values + "` n'est pas disponible dans cette version.");
					i--;
				} else {
					secret[i] = values;
					values = "";
				}
			}
			
			clearScreen();
			
		} else secret = makeSecret(col, max);
		
		return secret;
		
	}

	// [OK] Cr�ation de la phrase secr�te al�atoirement
	public static String[] makeSecret(String[] colors, int max) {

		// D�finition des variables
		String[] secret = new String[max];
		
		for (int i = 0; i < secret.length; i++)
			secret[i] = colors[getRandom(0, max)];

		return secret;
		
	}
	
	// [OK] Listing des couleurs disponibles
	public static String listColors(String[] array) {
		String str = "";
		for (int i = 0; i < array.length; i++) str += (!str.equals("") ? ", " : "") + array[i];
		return str;		
	}
	
	// [OK] Afficher la phrase secr�te en clair
	public static String showSecret(String[] array) {
		String str = "";
		for (int i = 0; i < array.length; i++) str += (!str.equals("") ? ", " : "") + array[i];
		return str;
	}
	
	// [OK] V�rification du nombre de couleurs envoy�es
	public static boolean checkNumColors(String colors, int max) {
		
		// D�finition des variables
		String[] propal = new String[0];
		boolean valid = false;

		// Traitement des donn�es
		propal = colors.split(";");
		if (propal.length == max) valid = true; 
		
		return valid;

	}
	
	// [OK] V�rification que les couleurs envoy�es sont disponibles
	public static boolean checkNameColors(String given, String[] colors) {
		
		// D�finition des variables
		boolean valid = true;
		String[] propal = new String[0];
		propal = given.split(";");
		
		for (int i = 0; i < propal.length; i++) {
			if (!inArray(colors, propal[i])) {
				valid = false;
				break;
			}
		}
		
		return valid;
	}
	
	// [OK] V�rification d'une couleur bien plac�e dans la phrase secr�te 
	public static int checkPlaced(String given, String[] colors) {

		// D�finition des variables
		int num = 0;
		String[] propal = new String[0];
		propal = given.split(";");
		
		for (int i = 0; i < colors.length; i++)
			if (colors[i].equals(propal[i])) num++;

		return num;

	}
	
	// [OK] V�rification de la pr�sence d'une couleur dans la phrase secr�te
	public static int checkPresent(String given, String[] colors) {

		// D�finition des variables
		int num = 0;
		String[] propal = new String[0];
		propal = given.split(";");
		
		for (int i = 0; i < propal.length; i++)
			if (inArray(colors, propal[i])) num++;
		
		return num;

	} 
	
	// [OK] Jouons !
	public static int[] letsPlay(String[] secret, String[] colors, int max) {

		// D�finition des variables
		int roundMax = 12, roundCur;
		int[] finJeu = new int[2];
		
		// Parcours sur les 12 rounds
		for (int i = 0; i < roundMax; i++) {
			
			roundCur = (i + 1);
			System.out.println("Round " + roundCur + " : Saisissez " + (max > 1 ? "vos" : "votre") + " couleur" + (max > 1 ? "s" : "") + " s�par�es par des ';'. (Tapez 'rappel' pour un rappel des couleurs disponibles.)");
			
			String given = scStr.nextLine();
			
			// Si on demande un rappel des couleurs
			if (given.equals("rappel")) {
				System.out.println(listColors(colors));
				given = scStr.nextLine();
			}
			
			// Si on demande un 'fliptable/ragequit'
			if (given.equals("fliptable") || given.equals("ragequit")) {
				System.out.println("Fuck this shit up!");
				finJeu[0] = -1;			// Forfait
				finJeu[1] = roundCur;	// � quel round
				break;
			}

			// On v�rifie que le nombre de couleurs donn�es correspond au nombre de couleurs attendues
			if (checkNumColors(given, max) == false) {
				System.out.println("Vous devez entrer " + max + " couleur" + (max > 1 ? "s" : ""));
				given = scStr.nextLine();
			}
			
			// On v�rifie que les couleurs sont disponibles dans le jeu, et le mode de jeu
			if (checkNameColors(given, colors) == false) {
				System.out.println("Au moins une couleur que vous utilisez n'est pas disponible.");
				System.out.println(listColors(colors));
				given = scStr.nextLine();
			}
			
			// A priori, pas d'erreur : On compare maintenant les phrases secr�tes
			int set = checkPlaced(given, secret);
			int ins = checkPresent(given, secret);

			if (set == 4) {	
				
				finJeu[0] = 1;			// Victoire
				finJeu[1] = roundCur;	// � quel round
				break;
				
			} else {

				String pluralSet = (ins > 1 ? "s" : "");
				String pluralIns = (set > 1 ? "s" : "");
				System.out.println("Vous avez " + ins + " couleur" + pluralIns + " pr�sente" + pluralIns + " dans la phrase secr�te.");
				System.out.println("Vous avez " + set + " couleur" + pluralSet + " bien plac�e" + pluralSet + " dans la phrase secr�te.");

				finJeu[0] = 0;			// D�faite
				finJeu[1] = roundCur;	// � quel round
				
			}

			System.out.println(given);
			
		}
		
		return finJeu;
		
	}
	
	
	public static void main(String[] args) {

		// D�finition des variables
		int max = 4;
		int[] game 		= new int[2];
		String[] gamMod = new String[0];
		String[] colors = new String[0];
		String[] secret = new String[0];
		// String[] propal = new String[0];
		
		// D�finition des diff�rents modes de jeu
		gamMod = gameMode();	// GameMode[0] = Normal ou Master -- GameMode[1] = Human ou Computer

		// D�finition des valeurs disponibles
		if (gamMod[0].equals("M")) max = 5;
		colors = getColors(gamMod[0]);
		
		// D�finition de la phrase secr�te
		secret = getSecret(colors, gamMod[1], max); 
		
		// C'est maintenant qu'on joue !
		game = letsPlay(secret, colors, max);
		
		switch (game[0]) {
			case -1:
				System.out.println("Vous avez d�clar� forfait !");
				break;

			case 0:
				System.out.println("Vous avez perdu. La phrase secr�te �tait  : " + showSecret(secret) + ".");
				break;

			case 1:
				System.out.print("F�licitation ! Vous avez trouv� la phrase secr�te en " + game[1] + " coup" + (game[1] > 1 ? "s" : "") + " !");
				if (game[1] == 1) System.out.println(" Tricheur ?");
				break;
		}
		
	}

	// ##########################################################################################################
	// ##########################################################################################################
	// ##########################################################################################################
	
	// printArray
	public static void printArray(String[] array) {
		String str = "Array(";
		for (int i = 0; i < array.length; i++) str += (!str.equals("Array(") ? ", " : "") + array[i];
		str += ")";
		System.out.println(str);
	}

	// inArray
	public static boolean inArray(String[] haystack, String needle) {
		boolean inArray = false;
		for (int i = 0; i < haystack.length; i++) {
			if (haystack[i].equals(needle)) {
				inArray = true;
				break;
			}
		}
		return inArray;
	}

	// R�cup�ration d'un nombre al�atoire
	public static int getRandom(int min, int max) {
		int rnd;
		Random r = new Random();
		rnd = r.nextInt(max) + min;
		return rnd;
	}
	
	// clearScreen
	public static void clearScreen() {  
		for(int clear = 0; clear < 1000; clear++) {
			System.out.println("\r\n") ;
		}  
	} 
}