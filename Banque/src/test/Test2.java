package test;

import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import metier.Carte;
import metier.Compte;
import metier.ComptePayant;
import metier.ComptePrenium;
import metier.CompteSimple;

public class Test2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*System.out.println("Cr�ation des comptes");
		CompteSimple simple1 = new CompteSimple(3, Carte.MasterCard);
		ComptePayant payant1 = new ComptePayant(1000, Carte.CB, 300);
		ComptePrenium prenium1 = new ComptePrenium(1000, Carte.CB);
		
		System.out.println(simple1);
		System.out.println(payant1);
		System.out.println(prenium1);
		
		System.out.println("\nRetrait d'argent");
		simple1.retrait(150);
		payant1.retrait(150);
		prenium1.retrait(150);
		
		System.out.println(simple1);
		System.out.println(payant1);
		System.out.println(prenium1);
		
		System.out.println("\nVersement d'argent");
		simple1.versement(1);
		payant1.versement(100);
		prenium1.versement(100);
		
		System.out.println(simple1);
		System.out.println(payant1);
		System.out.println(prenium1);
		
		
		System.out.println("\nTransfert de prenium � simple");		
		prenium1.transfert(4, simple1);
		System.out.println(simple1);
		System.out.println(prenium1);*/
		
		
		System.out.println("Bienvenue � la Banque de France !");
		boolean continuer = true;
		//List<Compte> comptes = new ArrayList<Compte>();
		List<Compte> comptes = Compte.getComptes();
		HashMap<Integer,Compte> comptes2 = Compte.getComptes2();
		Scanner scString = new Scanner(System.in);
		Scanner scDouble = new Scanner(System.in);
		Scanner scInt  = new Scanner(System.in);
		do {
			System.out.println("Que voulez-vous faire ?");
			System.out.println("1 - Retrait");
			System.out.println("2 - Versement");
			System.out.println("3 - Transfert ");
			System.out.println("4 - Cr�er un compte");
			System.out.println("5 - Visualiser les comptes");
			System.out.println("6 - Supprimer un compte");
			System.out.println("Autre - Partir");
			String choixFaire = scString.nextLine();
			
			switch(choixFaire) {
				case "1":
					if(comptes2.size()<1) {
						System.out.println("Il n'y a pas de compte cr�� !");
					}else {
						System.out.println("Sur quel compte voulez-vous faire un retrait ?");
						for(Compte compte : comptes2.values()) {
							System.out.println(compte.getCode()+" - "+compte);
						}
						int choixRetrait = scInt.nextInt();
						while(!comptes2.containsKey(choixRetrait)) {
							System.out.println("Le compte n'existe pas");
							System.out.println("Sur quel compte voulez-vous faire un retrait ?");
							for(Compte compte : comptes2.values()) {
								System.out.println(compte.getCode()+" - "+compte);
							}
							choixRetrait = scInt.nextInt();
						}
						
						System.out.println("Quel montant voulez-vous retirer ?");
						double montantRetrait = scDouble.nextDouble();
						while(montantRetrait < 0) {
							System.out.println("Le montant est inf�rieur � 0");
							System.out.println("Quel montant voulez-vous retirer ?");
							montantRetrait = scDouble.nextDouble();
						}
						
						Compte choixCompteRetrait = comptes2.get(choixRetrait);
						choixCompteRetrait.retrait(montantRetrait);
					}
					break;
				case "2":
					if(comptes2.size()<1) {
						System.out.println("Il n'y a pas de compte cr�� !");
					}else {
						System.out.println("Sur quel compte voulez-vous faire un versement ?");
						for(Compte compte : comptes2.values()) {
							System.out.println(compte.getCode()+" - "+compte);
						}
						int choixVersement = scInt.nextInt();
						while(!comptes2.containsKey(choixVersement)) {
							System.out.println("Le compte n'existe pas");
							System.out.println("Sur quel compte voulez-vous faire un versement ?");
							for(Compte compte : comptes2.values()) {
								System.out.println(compte.getCode()+" - "+compte);
							}
							choixVersement = scInt.nextInt();
						}
						
						System.out.println("Quel montant voulez-vous verser ?");
						double montantVersement = scDouble.nextDouble();
						while(montantVersement < 0) {
							System.out.println("Le montant est inf�rieur � 0");
							System.out.println("Quel montant voulez-vous verser ?");
							montantVersement = scDouble.nextDouble();
						}
						
						Compte choixCompteVersement = comptes2.get(choixVersement);
						choixCompteVersement.versement(montantVersement);
					}
					break;
				case "3":
					if(comptes2.size()<2) {
						System.out.println("Il n'y a pas assez de compte cr�� !");
					}else {
						System.out.println("Quel compte va faire le transfert ?");
						for(Compte compte : comptes2.values()) {
							System.out.println(compte.getCode()+" - "+compte);
						}
						int choixTrans = scInt.nextInt();
						while(!comptes2.containsKey(choixTrans)) {
							System.out.println("Le compte n'existe pas");
							System.out.println("Quel compte va faire le transfert ?");
							for(Compte compte : comptes2.values()) {
								System.out.println(compte.getCode()+" - "+compte);
							}
							choixTrans = scInt.nextInt();
						}
						
						
						System.out.println("Quel compte va rececoir l'argent ?");
						for(Compte compte : comptes2.values()) {
							System.out.println(compte.getCode()+" - "+compte);
						}
						int choixRece = scInt.nextInt();
						while(!comptes2.containsKey(choixRece) || choixTrans==choixRece) {
							System.out.println("Le compte n'existe pas ou c'est le m�me que le compte qui transfert");
							System.out.println("Quel compte va faire le transfert ?");
							for(Compte compte : comptes2.values()) {
								System.out.println(compte.getCode()+" - "+compte);
							}
							choixRece = scInt.nextInt();
						}
						
						
						System.out.println("Quel montant voulez-vous transferer ?");
						double montantTransfert = scDouble.nextDouble();
						while(montantTransfert < 0) {
							System.out.println("Le montant est inf�rieur � 0");
							System.out.println("Quel montant voulez-vous transferer ?");
							montantTransfert = scDouble.nextDouble();
						}
						
						Compte choixCompteTrans = comptes2.get(choixTrans);
						Compte choixCompteRece = comptes2.get(choixRece);
						choixCompteTrans.transfert(montantTransfert, choixCompteRece);
					}
					break;
				case "4":
					System.out.println("Quel type de compte voulez-vous cr�er ?");
					System.out.println("1 - Compte Simple");
					System.out.println("2 - Compte Payant");
					System.out.println("3 - Compte Prenium");
					String choixCompte = scString.nextLine();
					
					while(!choixCompte.equals("1") && !choixCompte.equals("2") && !choixCompte.equals("3")) {
						System.out.println("Le compte n'existe pas !");
						System.out.println("Quel type de compte voulez-vous cr�er ?");
						System.out.println("1 - Compte Simple");
						System.out.println("2 - Compte Payant");
						System.out.println("3 - Compte Prenium");
						choixCompte = scString.nextLine();
					}
					
					System.out.println("Quel montant voulez-vous mettre ?");
					double montant = scDouble.nextDouble();
					while(montant < 0) {
						System.out.println("Le Montant est inf�rieur � 0 ! Quel montant voulez-vous mettre ?");
						montant = scDouble.nextDouble();
					}
					
					System.out.println("Quelle type de carte voulez-vous ?");
					int nb = 1;
					for(Carte s : Carte.values()) {
						System.out.println(nb+" - "+s.name());
						nb++;
					}
					int choixCarte = scInt.nextInt();
					
					while(choixCarte<0 || choixCarte>Carte.values().length) {
						System.out.println("Le type de carte n'existe pas !");
						System.out.println("Quelle type de carte voulez-vous ?");
						nb = 1;
						for(Carte s : Carte.values()) {
							System.out.println(nb+" - "+s.name());
							nb++;
						}
						choixCarte = scInt.nextInt();
					}
					
					Carte c = Carte.values()[choixCarte-1];
					
						
					
					switch(choixCompte) {
						case "1":
							CompteSimple compteSimple = new CompteSimple(montant, c);
							System.out.println(compteSimple);
							//comptes.add(compteSimple);
							break;
						case "2":
							System.out.println("Quel d�couvert voulez-vous ? (0 � 500)");
							double decouvert = scDouble.nextDouble();
							while(decouvert < 0 || decouvert > 500) {
								System.out.println("Le d�couvert n'est pas valide !");
								System.out.println("Quel d�couvert voulez-vous ? (0 � 500)");
								decouvert = scDouble.nextDouble();
							}
							ComptePayant comptePayant = new ComptePayant(montant, c, -decouvert);
							System.out.println(comptePayant);
							//comptes.add(comptePayant);
							break;
						case "3":
							ComptePrenium comptePrenium = new ComptePrenium(montant, c);
							System.out.println(comptePrenium);
							//comptes.add(comptePrenium);
							break;
						default:
							System.out.println("Aucun compte ne correspont");
					}
					break;
				case "5":
					if(comptes2.size()==0) {
						System.out.println("Il n'y a pas de comptes");
					}else {
						System.out.println("Voici la liste des comptes : ");
						for(Compte compte : comptes2.values()) {
							System.out.println(compte);
						}
					}
					break;
				case "6":
					if(comptes2.size()==0) {
						System.out.println("Il n'y a pas de comptes");
					}else {
						System.out.println("Quel compte voulez-vous supprimer ?");
						for(Compte compte : comptes2.values()) {
							System.out.println(compte.getCode()+" - "+compte);
						}
						int choixDelete = scInt.nextInt();
						
						while(!comptes2.containsKey(choixDelete)) {
							System.out.println("Le compte n'existe pas");
							System.out.println("Quel compte voulez-vous supprimer ?");
							for(Compte compte : comptes2.values()) {
								System.out.println(compte.getCode()+" - "+compte);
							}
							choixDelete = scInt.nextInt();
						}
						
						comptes2.remove(choixDelete);
					}
					break;
				default:
					continuer = false;
			}
		}while(continuer);
		
		System.out.println("Il y a "+Compte.getComptes2().size() + " comptes");
		System.out.println("Il y a eu "+Compte.getNbTransac()+" transactions");
		System.out.println("On a tax� "+Compte.getNbTaxeTotal()+"�");
		
	}

}
