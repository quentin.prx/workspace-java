package metier;

public final class ComptePayant extends Compte {
	private double decouvert;

	public ComptePayant(double solde, Carte carte, double decouvert) {		
		super(solde-50, 1, carte);
		this.decouvert = decouvert;
		if(this.decouvert < Carte.VISA.getPrix()) {
			System.out.println("Vous n'avez pas les fonds nécessaire ! Pour une "+carte);
			this.carte = Carte.CB;
		}else if(this.decouvert < Carte.MasterCard.getPrix() && carte.equals(Carte.MasterCard)) {
			System.out.println("Vous n'avvez pas les fonds nécessaire ! Pour une "+carte);
			this.carte = Carte.VISA;
		}
	}

	public boolean retraitPossible(double montant) {
		if(solde-montant-taxe < decouvert) {
			return false;
		}else {
			return true;
		}		
	}
	
	public void retrait(double montant) {
		if(solde-montant-taxe < decouvert) {
			System.out.println("Vous n'avez pas les fonds nécessaire !");
		}else {
			solde -= montant+taxe;
			plusTaxe(taxe);
			plusTransac();
		}		
	}

	public double getDecouvert() {
		return decouvert;
	}

	public void setDecouvert(double decouvert) {
		this.decouvert = decouvert;
	}
	
	
	public String toString() {
		return "ComptePayant [decouvert=" + decouvert + ", code=" + code + ", solde=" + solde + ", taxe=" + taxe
				+ ", carte=" + carte + "]";
	}
	
	
}
