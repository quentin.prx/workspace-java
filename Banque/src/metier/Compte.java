package metier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Compte {
	protected int code;
	protected double solde;
	protected int taxe;
	protected Carte carte;
	private static int nombreCompte=0;
	private static int nbTransac = 0;
	private static double nbTaxeTotal = 0.0;
	private static List<Compte> comptes = new ArrayList<Compte>();
	private static HashMap<Integer,Compte> comptes2 = new HashMap<Integer,Compte>();
	
	public Compte(double solde,int taxe,Carte carte) {
		
		/*if(solde < Carte.VISA.getPrix()) {
			System.out.println("Vous n'avez pas les fonds n�cessaire ! Pour une "+carte);
			carte = Carte.CB;
		}else if(solde < Carte.MasterCard.getPrix() && carte.equals(Carte.MasterCard)) {
			System.out.println("Vous n'avvez pas les fonds n�cessaire ! Pour une "+carte);
			carte = Carte.VISA;
		}*/
		
		this.solde = solde - carte.getPrix();
		this.code = ++nombreCompte;
		this.taxe = taxe;
		this.carte = carte;
		comptes.add(this);
		comptes2.put(code, this);
	}
	
	public void versement(double montant) {
		if(montant < this.taxe) {
			System.out.println("Le montant que vous versez est inf�rieur � la taxe");
		}else {
			montant -= taxe;
			solde += montant;
			plusTaxe(taxe);
			plusTransac();
		}
	}
	
	
	public boolean versementPossible(double montant) {
		if(montant < this.taxe) {
			return false;
		}else {
			return true;
		}
	}	
	

	public void retrait(double montant) {
		solde -= montant+taxe;
		plusTransac();
		plusTaxe(taxe);
	}
	
	public boolean retraitPossible(double montant) {
		return true;
	}
	
	public void transfert(double montant,Compte c) {
		if(this.retraitPossible(montant) && c.versementPossible(montant)) {
			this.retrait(montant);
			c.versement(montant);
		}else {
			System.out.println("Impossible de faire le transfert");
		}
	}
	
	protected void plusTransac() {
		Compte.nbTransac++;
	}
	
	protected void plusTaxe(double montant) {
		Compte.nbTaxeTotal += taxe;
	}

	public static int getNbTransac() {
		return nbTransac;
	}

	public static double getNbTaxeTotal() {
		return nbTaxeTotal;
	}
	
	public static List<Compte> getComptes() {
		return comptes;
	}
	
	public static HashMap<Integer, Compte> getComptes2() {
		return comptes2;
	}

	public int getTaxe() {
		return taxe;
	}

	public void setTaxe(int taxe) {
		this.taxe = taxe;
	}

	public int getCode() {
		return code;
	}

	public double getSolde() {
		return solde;
	}

	public Carte getCarte() {
		return carte;
	}

	public static int getNombreCompte() {
		return nombreCompte;
	}
	
	
	
}
