package metier;

public final class ComptePrenium extends Compte{
	
	public ComptePrenium(double solde, Carte carte) {
		super(solde-150, 0, carte);
	}


	public String toString() {
		return "ComptePrenium [code=" + code + ", solde=" + solde + ", taxe=" + taxe + ", carte=" + carte + "]";
	}

	
	
}
