package metier;

public final class CompteSimple extends Compte {
	private final double decouvert = 0;

	public CompteSimple(double solde, Carte carte) {
		super(solde, 5, carte);
		if(this.solde < Carte.VISA.getPrix()) {
			System.out.println("Vous n'avez pas les fonds nécessaire ! Pour une "+carte);
			this.carte = Carte.CB;
		}else if(this.solde < Carte.MasterCard.getPrix() && carte.equals(Carte.MasterCard)) {
			System.out.println("Vous n'avvez pas les fonds nécessaire ! Pour une "+carte);
			this.carte = Carte.VISA;
		}
	}

	public void retrait(double montant) {
		if(solde-montant-taxe < decouvert) {
			System.out.println("Vous n'avez pas les fonds nécessaire !");
		}else {
			solde -= montant+taxe;
			plusTaxe(taxe);
			plusTransac();
		}		
	}
	
	public boolean retraitPossible(double montant) {
		return solde-montant-taxe < decouvert;		
	}


	public double getDecouvert() {
		return decouvert;
	}

	public String toString() {
		return "CompteSimple [decouvert=" + decouvert + ", code=" + code + ", solde=" + solde + ", taxe=" + taxe
				+ ", carte=" + carte + "]";
	}
	
	
}
