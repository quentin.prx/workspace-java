package metier;

public enum Carte {
	
	CB(0),
	VISA(5),
	MasterCard(10);
	
	private int prix;
	
	Carte(int prix) {
		this.prix = prix;
	}
	
	public int getPrix() {
		return this.prix;
	}
	
}
