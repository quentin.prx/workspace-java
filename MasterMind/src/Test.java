import java.util.Random;
import java.util.Scanner;

public class Test {
	
	public static Scanner scInt = new Scanner(System.in);
	public static Random r = new Random();
	
	public static int[] pionCache() {
		int tabPion[] = new int[4];
		
		for(int i=0;i<tabPion.length;i++) {
			tabPion[i] = r.nextInt(6)+1;
		}
		
		return tabPion;
	}
	
	public static boolean estPresent(int couleur,int tabPion[]) {
		for(int n : tabPion) {
			if(n==couleur)
				return true;
		}
		return false;
	}
	
	public static void jouer() {
		int tabPionCache[] = pionCache();
		
		//0 pas pr�sent, 1 mal plac�, 2 bien plac�
		int tabVerifPlacement[] = new int[4];
		
		int testPlacement[] = new int[4];
		
		int nbVie = 10;
		boolean correcte = false;
		
		while(nbVie>=10 || !correcte) {
			System.out.println("Tour n�"+(11-nbVie));
			for(int i=1;i<=4;i++) {
				System.out.println("Entrer la "+i+" couleur (1 � 6)");
				int choix = scInt.nextInt();
				while(choix<0 || choix>7) {
					System.out.println("Erreur ! J'ai dit ENTRER la "+i+" couleur (1 � 6)");
					choix = scInt.nextInt();
				}
				testPlacement[i-1] = choix;
			}
			
			String chTest = "";
			for(int i=0;i<testPlacement.length;i++) {
				chTest += testPlacement[i] + " ";
				if(testPlacement[i] == tabPionCache[i]) {
					//System.out.println("Le pion n�"+(i+1)+" est bien plac�");
					tabVerifPlacement[i] = 2;
				}else {
					if(estPresent(testPlacement[i], tabPionCache)) {
						//System.out.println("Le pion n�"+(i+1)+" est mal plac�");
						tabVerifPlacement[i] = 1;
					}else {
						//System.out.println("Le pion n�"+(i+1)+" est pas pr�sent");
						tabVerifPlacement[i] = 0;
					}
				}
			}
			System.out.println("Votre jeu "+chTest);
			
			int nbCorrecte = 0;
			String ch = "";
			for(int i=0;i<tabVerifPlacement.length;i++) {
				ch += tabVerifPlacement[i] + " ";
				if(tabVerifPlacement[i]==2)
					nbCorrecte++;
			}
			System.out.println("Placement : "+ch);
			if(nbCorrecte == 4)
				correcte = true;
			
			nbVie--;
			
			if(nbVie < 0)
				correcte = true;
		}
		
		if(nbVie <0) {
			System.out.println("Vous avez perdu !");
		}else {
			System.out.println("Vous avez gagn� !");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		jouer();
	}

}
