import java.util.Scanner;

public class Main {
	
	public static double add(double a, double b) {
		return a + b;
	}
	
	public static double minus(double a, double b) {
		return a - b;
	}
	
	public static double mul(double a, double b) {
		return a * b;
	}
	
	public static double div(double a, double b) {
		return a / b;
	}
	
	public static void produit() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrer un nombre : ");
		double nb1 = sc.nextDouble();
		if(nb1<0)
			System.out.println("N�gatif");
		else if(nb1>0)
			System.out.println("Positif");
		else
			System.out.println("Nul");
		
		System.out.println("Entrer un deuxi�me nombre : ");
		double nb2 = sc.nextDouble();
		
		if((nb1<0 && nb2<0) || (nb1>0 && nb2>0))
			System.out.println("Positif");
		else if((nb1 < 0 && nb2 > 0) || (nb1>0 && nb2 <0))
			System.out.println("N�gatif");
		else
			System.out.println("Nul");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println(add(5,5));
		
		Scanner scInt = new Scanner(System.in);
		Scanner scString = new Scanner(System.in);
		System.out.println("ENTREZ UN NOMBRE A");
		double a = scInt.nextDouble();
		System.out.println("ENTRER UN NOMBRE B");
		double b = scInt.nextDouble();
		System.out.println("Entrer un op�rateur");
		String op = scString.nextLine();
		
		
		switch(op) {
			case "*":
				System.out.println(mul(a, b));
				break;
			case "+":
				System.out.println(add(a, b));
				break;
			case "-":
				System.out.println(minus(a, b));
				break;
			case "/":
				System.out.println(div(a, b));
				break;
			default:
				System.out.println("Op�rateur incorrect");
				
		}
		

	}

}
