<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Nouvelle Enfant</title>
</head>
	<body>
		<%@	include file="index.html" %>
		<h1>Nouvelle enfant</h1>
		<form action="" method="get">
			<table>
				<tr><td><label for="idNom">NOM</label></td><td><input type="text" id="idNom" name="nom" required></td></tr>
				<tr><td><label for="idPrenom">PRENOM</label></td><td><input type="text" id="idPrenom" name="prenom" required></td></tr>
				<tr><td><label for="idAge">Age</label></td><td><input type="number" id="idAge" name="age" required></td></tr>
				<tr><td rowspan="1"><input type="submit" id="idValider" name="valider" value="Valider"></td></tr>
			</table>
			
			
			${enfant}
		</form>
	</body>
</html>