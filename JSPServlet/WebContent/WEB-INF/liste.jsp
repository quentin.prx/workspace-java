<%@page import="java.util.List"%>
<%@page import="java.sql.*"%>
<%@page import="dao.DAO"%>
<%@page import="dao.DAOEnfant"%>
<%@page import="modele.Enfant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Liste des enfants</title>
</head>
	<body>
		<%@	include file="index.html" %>
		<%	String prenom="Norman"; %>
		<%	out.println("<h1>"+prenom+"</h1>"); %>
		
		
		<h1>${enfant.prenom} - ${enfant.nom}</h1>
		<h2>ECHO / ${testo}</h2>
		
		<table>
			<tr><th>Nom</th><th>Prénom</th><th>Age</th></tr>
			<% 
				DAOEnfant daoEnfant = new DAOEnfant();
				
				for(Enfant e : daoEnfant.findAll()){
					out.println("<tr><td>"+e.getNom()+"</td><td>"+e.getPrenom()+"</td><td>"+e.getAge()+"</td></tr>");
				}
			
			%>
		</table>
		
		<h3>La moyenne d'âge est ${AVG} ans</h3>
	</body>
</html>