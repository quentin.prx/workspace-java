package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOEnfant;
import modele.Enfant;

@WebServlet("/liste")
public class ListeEnfants extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		DAOEnfant daoEnfant = new DAOEnfant();
		
		Enfant e = new Enfant("Le Sapin", "Norman", 85);
		request.setAttribute("enfant", e);
		
		String test = request.getParameter("nom");
		int id = Integer.parseInt(request.getParameter("id"));
		
		
		try {
			Enfant e2 = daoEnfant.findByName(test);
			//request.setAttribute("testo", e2);
			
			Enfant e3 = daoEnfant.findById(id);
			request.setAttribute("testo", e3);
			double avg = daoEnfant.avgAge();
			request.setAttribute("AVG", avg);
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/liste.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
