package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOEnfant;
import modele.Enfant;


@WebServlet("/newEnfant")
public class newEnfant extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		DAOEnfant daoEnfant = new DAOEnfant();
		
		
		
		Enfant e = null;
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String ageS = request.getParameter("age");
		if(nom != null && prenom != null && ageS != null) {
			int age = Integer.parseInt(ageS);
			e = new Enfant(nom, prenom, age);
			try {
				daoEnfant.create(e);
			} catch (ClassNotFoundException | SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		request.setAttribute("enfant", e);
		
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/newEnfant.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
