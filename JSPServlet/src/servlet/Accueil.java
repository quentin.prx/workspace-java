package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/index")
public class Accueil extends HttpServlet{
	
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/index.html").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request,HttpServletResponse response) {
		
	}

}
