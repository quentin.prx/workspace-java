import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Personne> listP = Arrays.asList(
				new Personne(1.80, 70, "A", "Nicolas", Couleur.BLEU),
				new Personne(1.56, 50, "B", "Nicole", Couleur.VERRON),
				new Personne(1.75, 65, "C", "Germain", Couleur.VERT),
				new Personne(1.68, 50, "D", "Michel", Couleur.ROUGE),
				new Personne(1.96, 65, "E", "Cyrille", Couleur.BLEU),
				new Personne(2.10, 120, "F", "Denis", Couleur.ROUGE),
				new Personne(1.90, 90, "G", "Olivier", Couleur.VERRON)
		);
		
		//Affiche toutes les personnes de la liste
		Stream<Personne> sp = listP.stream();
		sp.forEach(System.out::println);
		
		//Stream avec une iteration infini
		//Stream.iterate(1, (x)->x+1).forEach(System.out::println);
		
		//Stream avec une iteration limit� � 100
		Stream.iterate(1, (x)->x+2).limit(100).forEach(System.out::println);
		
		//Filtrer des donn�es
		//Affiche que les personnes dont le poids est superieur � 50kg
		sp = listP.stream();
		sp.filter(x -> x.getPoids() > 50).forEach(System.out::println);
		
		//R�cup�rrer les informations filtrer
		//On r�cup�re que le poids
		sp = listP.stream();
		sp.filter(x -> x.getPoids() > 50).map(x -> x.getPoids()).forEach(System.out::println);
		
		//Agr�er un contenu
		//On fait la somme des poids r�cup�rrer
		sp = listP.stream();
		Double sum = sp.filter(x -> x.getPoids() > 50).map(x -> x.getPoids()).reduce(0.0d, (x,y) -> x+y);
		System.out.println(sum);
		
		//Optinonal
		sp = listP.stream();

		Optional<Double> sumi = sp.filter(x -> x.getPoids() > 250).map(x -> x.getPoids()).reduce((x,y) -> x+y);
		//System.out.println(sumi.get()); //erreur si aucun agregat
		System.out.println(sumi.orElse(0.0));
		if(sumi.isPresent())
			System.out.println(sumi.get());
		else
			System.out.println("Aucun aggr�gat de poids...");
		
		
		//Count
		sp = listP.stream();
		long count = sp	.filter(x -> x.getPoids() > 50).map(x -> x.getPoids()).count();
		System.out.println("Nombre d'�l�ments : " + count);
		
		//Collect
		//on r�cup�re les donn�es sous la forme d'un tableau
		sp = listP.stream();
		List<Double> ld = sp.filter(x -> x.getPoids() > 50).map(x -> x.getPoids()).collect(Collectors.toList());
		System.out.println(ld);
		
	}

}
