package metier;

import java.io.Serializable;

public abstract class Animal implements Serializable {
	protected double poids;
	protected transient Gender sexe;
	
	public Animal(double poids, Gender sexe) {
		this.poids = poids;
		this.sexe = sexe;
	}

	public double getPoids() {
		return poids;
	}

	public void setPoids(double poids) {
		this.poids = poids;
	}

	public Gender getSexe() {
		return sexe;
	}

	public void setSexe(Gender sexe) {
		this.sexe = sexe;
	}

	public abstract void parler();

	public final void jeSuisUnAnimal() {
		System.out.println("Je suis un animal !");
	}

	@Override
	public String toString() {
		return "Animal [poids=" + poids + ", sexe=" + sexe + "]";
	}
}
