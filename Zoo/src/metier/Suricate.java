package metier;

public final class Suricate extends Animal implements Hero {
	private double taille;
	private int nbPatte;

	public Suricate(double poids, Gender sexe,double taille,int nbPatte) {
		super(poids, sexe);
		this.taille = taille;
		this.nbPatte = nbPatte;
	}
	
	

	public double getTaille() {
		return taille;
	}

	public void setTaille(double taille) {
		this.taille = taille;
	}

	public int getNbPatte() {
		return nbPatte;
	}


	public void setNbPatte(int nbPatte) {
		this.nbPatte = nbPatte;
	}

	
	public void parler() {
		System.out.println("Hakuna Matata");
	}


	public String toString() {
		return "Suricate [taille=" + taille + ", nbPatte=" + nbPatte + ", poids=" + poids + ", sexe=" + sexe + "]";
	}


	public void voler() {
		// TODO Auto-generated method stub
		System.out.println("Je vole tr�s bas");
	}


	public String sePresenter() {
		// TODO Auto-generated method stub
		return "Je suis super Suricate";
	}
	
	
		
	
	
}
