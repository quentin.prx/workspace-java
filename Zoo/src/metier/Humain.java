package metier;

public final class Humain implements Hero {
	private String prenom, nom;

	public Humain(String prenom, String nom) {
		this.prenom = prenom;
		this.nom = nom;
	}

	
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String toString() {
		return "Humain [prenom=" + prenom + ", nom=" + nom + "]";
	}

	public void voler() {
		// TODO Auto-generated method stub
		System.out.println("Je vole Jack ! Je vole !");		
	}

	public String sePresenter() {
		// TODO Auto-generated method stub
		return "Je suis "+prenom+" "+nom;
	}
	
	
}
