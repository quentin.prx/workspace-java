package metier;

public enum Gender {
	MALE("M�le"),
	FEMELLE("Femelle"),
	HERMAPHRODITE("Hermaphrodite");
	
	private String sexe;
	
	Gender(String sexe){
		this.sexe = sexe;
	}
	
	public String getSexe() {
		return sexe;
	}
}
