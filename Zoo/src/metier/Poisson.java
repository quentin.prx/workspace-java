package metier;

public class Poisson extends Animal {
	protected String couleurNagoire;

	public Poisson(double poids, Gender sexe, String couleurNagoire) {
		super(poids, sexe);
		this.couleurNagoire = couleurNagoire;
	}

	public String getCouleurNagoire() {
		return couleurNagoire;
	}

	public void setCouleurNagoire(String couleurNagoire) {
		this.couleurNagoire = couleurNagoire;
	}

	public void parler() {
		System.out.println("Je bulle");
	}

	public String toString() {
		
		return super.toString() + " Poisson [couleurNagoire=" + couleurNagoire + "]";
	}

	
}
