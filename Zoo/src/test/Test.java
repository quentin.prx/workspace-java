package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import metier.Animal;
import metier.Chien;
import metier.Gender;
import metier.Poisson;
import metier.Suricate;

public class Test {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		/*Suricate timon = new Suricate(6.04, Gender.MALE, 50.0, 4);
		System.out.println(timon);
		Poisson nemo = new Poisson(3.0, Gender.FEMELLE, "Orange");
		System.out.println(nemo);
		
		List<Animal> zoo = new ArrayList<Animal>();
		zoo.add(timon);
		zoo.add(nemo);
		
		for(Animal a : zoo)
			a.parler();
		
		HashMap<Integer,Animal> listeAnimal = new HashMap<>();
		listeAnimal.put(1, timon);
		listeAnimal.put(2,nemo);
		Iterator<Integer> it = listeAnimal.keySet().iterator();
		
		while(it.hasNext()) {
			int key = it.next();
			Animal  a = listeAnimal.get(key);
			System.out.println(key);
			a.parler();
		}
		
		Humain h = new Humain("BatMan","Robin");
		List<Hero> heros = new ArrayList<Hero>();
		heros.add(h);
		heros.add(timon);
		
		for(Hero he : heros) {
			he.voler();
			System.out.println(he.sePresenter());
		}*/
		
		/*Chien medore = new Chien(50.5, Gender.MALE);
		Chien choupette = new Chien(5.5,Gender.FEMELLE);
		Poisson dory = new Poisson(2.0, Gender.FEMELLE, "Rouge");
		Chat garfield = new Chat(80, Gender.MALE);
		Chat laine = new Chat(10,Gender.FEMELLE);
		Lapin bunny = new Lapin(25, Gender.MALE);
		
		List<Animal> zoo = new ArrayList<Animal>();
		zoo.add(medore);
		zoo.add(dory);
		zoo.add(choupette);
		zoo.add(garfield);
		zoo.add(laine);
		zoo.add(bunny);
		
		for(Animal a : zoo) {
			if(a instanceof Crie) {
				((Crie) a).crier();
			}else {
				System.out.println("Le "+a.getClass().getSimpleName()+" ne crie pas");
			}
		}*/
		
		
		String chemin = "C:\\Users\\Informatique\\Desktop\\Cours_JAVA\\TestFichier\\test.txt";
		File f = new File(chemin);
		/*FileWriter fw = new FileWriter(f);
		
		fw.write("Toto");
		
		fw.close();
		
		FileReader fr = new FileReader(chemin);
		
		int c;
		while((c = fr.read()) != -1) {
			System.out.print((char)c);
		}
		
		fr.close();*/
		
		Chien c = new Chien(50.2, Gender.FEMELLE);
		Suricate s = new Suricate(20, Gender.MALE, 20, 4);
		Poisson p = new Poisson(12, Gender.FEMELLE, "Vert");
		
		List<Animal> zoo = new ArrayList<Animal>();
		
		zoo.add(c);
		zoo.add(s);
		zoo.add(p);
		
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(zoo);
		oos.close();
		fos.close();
		
		FileInputStream fis = new FileInputStream(f);
		ObjectInputStream ois = new ObjectInputStream(fis);
		List<Animal> zoo2 = (List<Animal>) ois.readObject();
		for(Animal a : zoo2) {
			System.out.println(a.getClass().getSimpleName());
		}
		ois.close();
		fis.close();
		
		
	}

}
