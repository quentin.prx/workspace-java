package metier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import metier.combat.BattleRoyal;
import metier.combat.Combat;
import metier.combat.Duel;
import metier.combat.GuerreDesGangs;
import metier.gang.Gang;
import metier.personne.Directeur;
import metier.personne.Prisonnier;

public class Goulag implements Serializable{
	private String nom;
	private Directeur directeur;
	private List<Prisonnier> prisonniersVivants;
	private List<Gang> gangs;
	private List<Combat> combatsHistorique;
	private List<Prisonnier> prisonniersMorts;
	private static Goulag goulag = null;
	
	private Goulag(String nom, Directeur directeur) {
		this.nom = nom;
		this.directeur = directeur;
		this.prisonniersVivants = new ArrayList<Prisonnier>();
		this.gangs = new ArrayList<Gang>();
		this.combatsHistorique = new ArrayList<Combat>();
		this.prisonniersMorts = new ArrayList<Prisonnier>();
	}
	
	public static Goulag getInstance(String nom,Directeur directeur) {
		if(goulag == null) {
			goulag = new Goulag(nom, directeur);
		}
		return goulag;
	}
	
	public static Goulag getGoulag() {
		return goulag;
	}

	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}

	public Directeur getDirecteur() {
		return directeur;
	}

	public List<Prisonnier> getPrisonniersVivants() {
		return prisonniersVivants;
	}
	
	public List<Gang> getGang() {
		return gangs;
	}

	public List<Combat> getCombatsHistorique() {
		return combatsHistorique;
	}

	public List<Prisonnier> getPrisonniersMorts() {
		return prisonniersMorts;
	}
	
	public void ajouterPrisonnier(Prisonnier p) {
		prisonniersVivants.add(p);
	}
	
	public void tuerPrisonnier(Prisonnier p) {
		prisonniersVivants.remove(p);
		prisonniersMorts.add(p);
	}
	
	public void listerPrisonniersVivants() {
		if(prisonniersVivants.size()<1) {
			System.out.println("\tIl n'y a pas de prisonniers vivants");
		}else {
			for(Prisonnier p : prisonniersVivants) {
				System.out.println("\t- "+p.getPrenomNom());
			}
		}
	}
	
	public void listerPrisonniersMorts() {
		if(prisonniersMorts.size()<1) {
			System.out.println("\tIl n'y a pas de prisonniers morts");
		}else {
			for(Prisonnier p : prisonniersMorts) {
				System.out.println("\t- "+p.getPrenomNom());
			}
		}
	}
	
	public void archiverCombat(Combat c) {
		combatsHistorique.add(c);
	}
	
	
	public void listerCombatsHistorique() {
		if(combatsHistorique.size()<1) {
			System.out.println("\tIl n'y a pas de combats");
		}else {
			for(Combat c : combatsHistorique) {
				System.out.println(c);
			}
		}
	}
	
	public void ajouterGang(Gang g) {
		gangs.add(g);
	}
	
	public void retirerGang(Gang g) {
		gangs.remove(g);
	}
	
	public void listerMembreGang() {
		if(gangs.size()<1) {
			System.out.println("\tIl n'y a pas de gang !");
		}else {
			for(Gang g : gangs) {
				System.out.println("\t"+g);
			}
		}
	}

	@Override
	public String toString() {
		String ch = "";
		ch += nom + " :\n";
		ch += "Directeur : "+ directeur.getPrenomNom() + "\n";
		ch += "Il y a actuellement " + prisonniersVivants.size() + " pensionnaire"+ ((prisonniersVivants.size()<=1)?"":"s")+"\n";
		for(Prisonnier p : prisonniersVivants)
			ch += "\t- "+ p.getPrenomNom() + "\n";
		ch += "\n";
		
		ch += "Il y a actuellement " + gangs.size() + " gang"+ ((gangs.size()<=0)?"":"s")+"\n";
		for(Gang g : gangs)
			ch += "\t- "+ g + "\n";
		ch += "\n";
		
		ch += "Il y eu actuellement " + combatsHistorique.size() + " combat"+ ((combatsHistorique.size()<=1)?"":"s")+"\n";
		for(Combat c : combatsHistorique) {
			ch += "\t- "+ c + " victoire de ";
			if(c instanceof Duel)
				ch += ((Duel) c).getGagnant().getPrenomNom() +"\n";
			else if(c instanceof BattleRoyal)
				ch += ((BattleRoyal) c).getGagnant().getPrenomNom() +"\n";
			else if(c instanceof GuerreDesGangs)
				ch += ((GuerreDesGangs) c).getGagnant().getNom() +"\n";
		}
		ch += "\n";
		
		ch += "Il y a actuellement " + prisonniersMorts.size() + " mort"+ ((prisonniersMorts.size()<=1)?"":"s")+"\n";
		for(Prisonnier p : prisonniersMorts)
			ch += "\t- "+ p.getPrenomNom() + "\n";
		ch += "\n";
		
		return ch;
	}
	
	

}
