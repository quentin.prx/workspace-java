package metier.personne;

public final class Directeur extends Personnage {
	private static int cptDirecteur = 1;
	private static Directeur directeur = null;
	
	private Directeur(String login,String password,String prenom, String nom, int age) {
		super(login,password,prenom, nom, age);
		this.id = cptDirecteur++;
	}
	
	public static Directeur getInstance(String login,String password,String prenom,String nom, int age) {
		if(directeur == null) {
			directeur = new Directeur(login,password,prenom, nom, age);
			return directeur;
		}
		return directeur;
	}
	
	public static Directeur getDirecteur() {
		return directeur;
	}
	
	
	public String toString() {
		return super.toString();
	}
	
}
