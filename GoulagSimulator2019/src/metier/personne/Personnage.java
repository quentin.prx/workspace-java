package metier.personne;

import java.io.Serializable;

public abstract class Personnage implements Serializable{
	protected int id;
	protected String login;
	protected String password;
	protected String prenom;
	protected String nom;
	protected int age;
	
		public Personnage(String login, String password, String prenom, String nom, int age) {
		this.login = login;
		this.password = password;
		this.prenom = prenom;
		this.nom = nom;
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getNom() {
		return nom;
	}
	
	public String getPrenomNom() {
		return prenom + " " + nom;
	}
	
	public int getId() {
		return id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	@Override
	public String toString() {
		return prenom + " " + nom + " - " + age + " ans";
	}
	
	
	
}
