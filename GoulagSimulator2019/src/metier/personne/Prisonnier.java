package metier.personne;

import java.util.ArrayList;
import java.util.List;

import metier.gang.Gang;
import metier.gang.TypeGang;
import metier.stuff.Stuff;
import metier.triche.Triche;

public final class Prisonnier extends Personnage {
	private int force;
	private int agilite;
	private int endurance;
	private int intelligence;
	private int esprit;
	private Gang gang;
	private int nbVictoire;
	private int nbPain;
	private List<Stuff> stuffs;
	private static int cptPrisonnier = 1;
	
	public Prisonnier(String login,String password,String prenom, String nom, int age, int force, int agilite, int endurance, int intelligence,
			int esprit) {
		super(login,password,prenom, nom, age);
		this.force = force;
		this.agilite = agilite;
		this.endurance = endurance;
		this.intelligence = intelligence;
		this.esprit = esprit;
		this.gang = null;
		this.nbVictoire = 0;
		this.nbPain = 0;
		stuffs = new ArrayList<Stuff>();
		this.id = cptPrisonnier++;
	}

	public int getForce() {
		return force;
	}

	public void setForce(int force) {
		this.force = force;
	}

	public int getAgilite() {
		return agilite;
	}

	public void setAgilite(int agilite) {
		this.agilite = agilite;
	}

	public int getEndurance() {
		return endurance;
	}

	public void setEndurance(int endurance) {
		this.endurance = endurance;
	}

	public int getIntelligence() {
		return intelligence;
	}

	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	}

	public int getEsprit() {
		return esprit;
	}

	public void setEsprit(int esprit) {
		this.esprit = esprit;
	}

	public Gang getGang() {
		return gang;
	}

	public void setGang(Gang gang) {
		this.gang = gang;
	}
	
	public int getNbVictoire() {
		return nbVictoire;
	}
	
	public void setVictoire() {
		nbVictoire++;
	}
	
	public int getNbPain() {
		return nbPain;
	}
	
	public void setNbPain(int nbPain) {
		this.nbPain += nbPain*nbVictoire;
	}
	
	public List<Stuff> getStuffs(){
		return stuffs;
	}
	

	public static int getCptPrisonnier() {
		return cptPrisonnier;
	}
	
	
	/********************************************************************************
	 * ********************Actions du prisonnier************************************
	******************************************************************************* */
	
	public void acheterDuStuff(Stuff stuff) {
		if(stuffs.contains(stuff)) {
			System.out.println("Vous poss�der d�j� cette article");
		}else {
			if(nbPain<stuff.getCout()) {
				System.out.println("Vous n'avez pas les fonds n�cessaires");
			}else {
				nbPain -= stuff.getCout();
				stuffs.add(stuff);
				switch(stuff.getCategegorie()) {
					case 1:
						this.agilite += stuff.getGain();
						break;
					case 2:
						this.endurance += stuff.getGain();
						break;
					case 3:
						this.esprit += stuff.getGain();
						break;
					case 4:
						this.force += stuff.getGain();
						break;
					case 5:
						this.intelligence += stuff.getGain();
						break;
					default:
						System.out.println("Cette article n'apporte rien de sp�cial");
				}
			}
		}
	}
	
	public void creerUnGang(String nom,TypeGang type) {
		if(nbPain < 100) {
			System.out.println("Vous n'avez pas assez de Pains pour cr�er un Gang");
		}else {
			if(gang != null) {
				System.out.println("Vous avez �tes d�j� dans un gang");
			}else {
				nbPain -= 100;
				Gang g = new Gang(nom, type);
				g.ajouterMembre(this);
				gang = g;
			}
		}
	}
	
	public void entrerDansUnGang(Gang g) {
		if(nbPain<50) {
			System.out.println("Vous n'avez pas assez de Pains pour entrer dans un gang");
		}else {
			if(gang != null) {
				System.out.println("Vous �tes d�j� dans un gang");
			}else {
				nbPain -= 50;
				gang = g;
				g.ajouterMembre(this);
			}
		}
	}
	
	public void quitterLeGang() {
		if(nbPain<20) {
			System.out.println("Vous n'avez pas assez de Pains pour quitter le gang");
		}else {
			nbPain -= 20;
			gang.retirerMembre(this);
			gang = null;
		}
	}
	
	public void tricher(String code) {
		int gain = 0;
		for(Triche t : Triche.values()) {
			if(t.getCode().equals(code)) {
				gain = t.getNombre();
				switch(t.getCategorie()) {
					case 0:
						nbPain += gain;
						break;
					default:
						System.out.println("Rien de sp�cial");
				}
			}
		}
	}

	@Override
	public String toString() {
		String ch = super.toString() + " avec " + nbVictoire + " victoire" + ((nbVictoire<2)?"":"s");
		if(gang != null) {
			ch += " membre des " + gang.getNom() + " : \n";
		}else {
			ch += " : \n";
		}
		ch += "\t- " + agilite + " d'agilite\n";
		ch += "\t- " + endurance + " d'endurence\n";
		ch += "\t- " + esprit + " d'esprit\n";
		ch += "\t- " + force + " de force\n";
		ch += "\t- " + intelligence + " d'intelligence\n";
		ch += "Solde de pain : " + nbPain + "P\n";
		return ch;
	}
	
	
	
	
}
