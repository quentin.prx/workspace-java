package metier.gang;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import metier.personne.Prisonnier;

public class Gang implements Serializable{
	private int id;
	private String nom;
	private TypeGang type;
	private List<Prisonnier> prisonniers;
	private static int cptGang = 1;
	
	public Gang(String nom,TypeGang type) {
		this.nom = nom;
		this.type = type;
		this.id = cptGang++;
		prisonniers = new ArrayList<Prisonnier>();
	}
	
	public String getNom() {
		return nom;
	}

	public List<Prisonnier> getPrisonniers() {
		return prisonniers;
	}

	public void setPrisonniers(List<Prisonnier> prisonniers) {
		this.prisonniers = prisonniers;
	}

	public int getId() {
		return id;
	}

	public TypeGang getType() {
		return type;
	}

	public static int getCptGang() {
		return cptGang;
	}
	
	
	public void ajouterMembre(Prisonnier p) {
		boolean present = false;
		for(Prisonnier pp : prisonniers) {
			if(pp.getId() == p.getId())
				present = true;
		}
		if(!present)
			prisonniers.add(p);
	}
	
	public void retirerMembre(Prisonnier p) {
		prisonniers.remove(p);
	}
	

	@Override
	public String toString() {
		String ch = "";
		ch += type.getNom() + " " + nom;
		if(prisonniers.size()<=0) {
			ch += " n'a aucun membre dans ses rangs";
		}else {
			ch +=  " a dans ses rang "+ ((prisonniers.size()<=1)?"le":"les") +" prisonnier"+ ((prisonniers.size()<=1)?"":"s")  +" : \n";
		}
		for(Prisonnier p : prisonniers) {
			ch += "\t\t- " + p.getPrenomNom()+ "\n";
		}
		return ch;
	}
	
	
}
