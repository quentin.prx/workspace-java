package metier.gang;

public enum TypeGang {

	StringRouge(1,"String Rouge"),
	GiletJaune(2,"Gilet Jaune"),
	CasqueBleu(3,"Casque Bleu"),
	ChaussetteBlanche(4,"Chaussette Blanche");
	
	private int id;
	private String nom;
	
	private TypeGang(int id, String nom) {
		this.id = id;
		this.nom = nom;
	}
	
	public int getId() {
		return id;
	}
	
	public String getNom() {
		return nom;
	}
}
