package metier.triche;

public enum Triche {
	AjouterPain(1,"Ajouter 200 Pains","motherlode",200,0);
	
	private int id;
	private String nom;
	private String code;
	private int nombre;
	private int categorie;
	
	private Triche(int id, String nom,String code, int nombre,int categorie) {
		this.id = id;
		this.nom = nom;
		this.code = code;
		this.nombre = nombre;
		this.categorie = categorie;
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}
	
	public String getCode() {
		return code;
	}

	public int getNombre() {
		return nombre;
	}
	
	public int getCategorie() {
		return categorie;
	}
	
	
}
