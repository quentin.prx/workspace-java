package metier.combat;

import java.util.Random;

import metier.gang.Gang;
import metier.personne.Prisonnier;

public class GuerreDesGangs extends Combat{
	private static int cptGuerreDesGangs = 0;
	private Gang gang1;
	private Gang gang2;
	private Gang gagnant;
	
	public GuerreDesGangs(String nom, TypeLieu lieu,int painAGagner, Gang gang1, Gang gang2) {
		super(nom, lieu,painAGagner);
		this.gang1 = gang1;
		this.gang2 = gang2;
		this.gagnant = null;
		this.id = cptGuerreDesGangs++;
	}

	public static int getCptGuerreDesGangs() {
		return cptGuerreDesGangs;
	}

	public Gang getGang1() {
		return gang1;
	}

	public Gang getGang2() {
		return gang2;
	}

	public Gang getGagnant() {
		return gagnant;
	}

	public void setGagnant(Gang gagnant) {
		this.gagnant = gagnant;
	}
	
	
	public void lancerCombat() {
		Random r = new Random();
		int al;
		int match = 1;
		
		System.out.println("C'est parti pour la Guerre des Gang entre "+gang1.getNom()+ " VS "+ gang2.getNom());
		System.out.println("____________________________________");
		System.out.println(gang1.getNom()+" il y a "+gang1.getPrisonniers().size()+" prisonnier"+(((gang1.getPrisonniers().size()<0)?"":"s")));
		System.out.println(gang2.getNom()+" il y a "+gang2.getPrisonniers().size()+" prisonnier"+(((gang2.getPrisonniers().size()<0)?"":"s")));
		
		while(gang1.getPrisonniers().size()>=1 && gang2.getPrisonniers().size()>=1) {
			al = r.nextInt(gang1.getPrisonniers().size());
			Prisonnier p1 = gang1.getPrisonniers().get(al);
			
			al = r.nextInt(gang2.getPrisonniers().size());
			Prisonnier p2 = gang2.getPrisonniers().get(al);
			
			System.out.println("Match n�"+(match++)+" entre "+p1.getPrenomNom()+" et "+p2.getPrenomNom());
			Duel d = new Duel(nom, lieu, painAGagner, p1, p2);
			d.lancerCombat();
			Prisonnier gag = d.getGagnant();
			
			if(gag == p1) {
				gang2.retirerMembre(p2);
			}else {
				gang1.retirerMembre(p1);
			}
			System.out.println(gang1.getNom()+" il reste "+gang1.getPrisonniers().size()+" prisonnier"+(((gang1.getPrisonniers().size()<0)?"":"s")));
			System.out.println(gang2.getNom()+" il reste "+gang2.getPrisonniers().size()+" prisonnier"+(((gang2.getPrisonniers().size()<0)?"":"s")));
			System.out.println("____________________________________");
		}
		this.gagnant = ((gang1.getPrisonniers().size()<1)?gang2:gang1);
		System.out.println("Victoire finale de "+gagnant.getNom());
		System.out.println("Liste des membres restant : ");
		for(Prisonnier p: gagnant.getPrisonniers()) {
			System.out.println("\t-"+p.getPrenomNom());
		}
	}
	
	public String toString() {
		String ch = super.toString();
		ch += " opposant : \n";
		ch += "\t\t-" + gang1.getType().getNom() + " " + gang1.getNom();
		ch += " � " + gang2.getType().getNom() + " " + gang2.getNom();
		return ch;
	}
		
}
