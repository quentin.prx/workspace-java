package metier.combat;

import java.util.ArrayList;

public enum TypeLieu {
	
	BOUE(1,"Boue",new ArrayList<String>()),
	NEIGE(2,"Neige",new ArrayList<String>()),
	SABLE(3,"Sable",new ArrayList<String>()),
	MUR(4,"Mur",new ArrayList<String>()),
	EAU(5,"Eau",new ArrayList<String>()),
	CUISINE(6,"Cuisine",new ArrayList<String>()),
	DORTOIRE(7,"Dortoire",new ArrayList<String>());
	
	
	private int id;
	private String nom;
	private ArrayList<String> morts;
	private TypeLieu(int id,String nom,ArrayList<String> morts) {
		this.id = id;
		this.nom = nom;
		this.morts = morts;
		
		switch(this.id) {
			case 1:
				morts.add(" tu� par une coul�e de boue");
				morts.add(" tu� �touff� par la boue");
				break;
			case 2:
				morts.add(" tu� par une boule de neige explosive");
				morts.add(" �cras� par un bonhomme de neige");
				morts.add(" tu� apr�s avoir gliss� sur une �charppe rose");
				break;
			case 3:
				morts.add(" ensevlie par 3 tonnes de sables");
				morts.add(" mort apr�s avoir regard� un bikini rouge de trop pr�s");
				break;
			case 4:
				morts.add(" mort apr�s avoir tribuch� sur le mur");
				morts.add(" tu� par un lanc� franc de brique");
				break;
			case 5:
				morts.add(" mort noy�");
				morts.add(" tu� par un requin myope");
				morts.add(" tu� par une m�duse naine");
				morts.add(" mort �lectrocut� par un fer mal repass�");
				break;
			case 6:
				morts.add(" mort dans l'huile de friture");
				morts.add(" mort par un lanc� de couteaux mal aiguis�s");
				morts.add(" tu� par un coup de poele un peu trop violent");
				morts.add(" tu� d'une fourchette dans l'oeil droit");
				morts.add(" tu� d'une cuilli�re dans le nez");
				break;
			case 7:
				morts.add(" tu� �touff� par un oreill� pleins de rat");
				morts.add(" tu� par le barreau d'un lit rouill�");
				morts.add(" mort �trangl� par un drap trou�");
				break;
		default:
			morts.add(" tu� d'une fa�on encore inexpliqu�e!");
		}
	}
	
	public int getId() {
		return id;
	}
	
	public String getNom() {
		return nom;
	}
	
	public ArrayList<String> getMorts() {
		return morts;
	}

}
