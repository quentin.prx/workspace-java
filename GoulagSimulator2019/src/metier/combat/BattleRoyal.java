package metier.combat;

import java.util.List;
import java.util.Random;

import metier.personne.Prisonnier;

public class BattleRoyal extends Combat {
	private static int cptBattleRoyale = 1;
	private Prisonnier gagnant;
	private List<Prisonnier> prisonniers;
	
	public BattleRoyal(String nom, TypeLieu lieu,int painAGagner, List<Prisonnier> prisonniers) {
		super(nom, lieu,painAGagner);
		this.prisonniers = prisonniers;
		this.gagnant = null;
		this.id = cptBattleRoyale++;
	}
	
	
	public static int getCptBattleRoyale() {
		return cptBattleRoyale;
	}

	public List<Prisonnier> getPrisonniers() {
		return prisonniers;
	}

	public Prisonnier getGagnant() {
		return gagnant;
	}
	public void setGagnant(Prisonnier gagnant) {
		this.gagnant = gagnant;
	}
	
	
	public void lancerCombat() {
		Random r = new Random();
		int al;
		int match = 1;
		
		System.out.println("C'est parti pour le Battle Royale");
		System.out.println("____________________________________");
		System.out.println("Il y a "+prisonniers.size()+" prisonnier"+(((prisonniers.size()<0)?"":"s")));
		while(prisonniers.size()>1) {
			al = r.nextInt(prisonniers.size());
			Prisonnier p1 = prisonniers.get(al);
			al = r.nextInt(prisonniers.size());
			Prisonnier p2 = prisonniers.get(al);
			while(p1==p2) {
				al = r.nextInt(prisonniers.size());
				p2 = prisonniers.get(al);
			}
			System.out.println("Match n�"+(match++)+" entre "+p1.getPrenomNom()+" et "+p2.getPrenomNom());
			Duel d = new Duel(nom, lieu, painAGagner, p1, p2);
			d.lancerCombat();
			
			Prisonnier gag = d.getGagnant();
			
			if(gag == p1) {
				prisonniers.remove(p2);
			}else {
				prisonniers.remove(p1);
			}
			System.out.println("Il reste "+prisonniers.size()+" prisonnier"+(((prisonniers.size()<0)?"":"s")));
			System.out.println("_________________________________");
		}
		this.gagnant = prisonniers.get(0);
		System.out.println("Victoire finale de "+gagnant.getPrenomNom());
	}
	
	public String toString() {
		String ch = super.toString();
		ch += " opposant tous les prisonniers entre eux";
		return ch;
	}
	
}
