package metier.combat;

import java.util.Random;

import metier.Goulag;
import metier.personne.Prisonnier;

public final class Duel extends Combat{
	private static int cptDuel = 1;
	private Prisonnier p1;
	private Prisonnier p2;
	private Prisonnier gagnant;
	
	public Duel(String nom, TypeLieu lieu,int painAGagner, Prisonnier p1, Prisonnier p2) {
		super(nom, lieu,painAGagner);
		this.p1 = p1;
		this.p2 = p2;
		this.gagnant = null;
		this.id = cptDuel++;
	}

	public static int getCptDuel() {
		return cptDuel;
	}

	public Prisonnier getP1() {
		return p1;
	}

	public Prisonnier getP2() {
		return p2;
	}

	public Prisonnier getGagnant() {
		return gagnant;
	}

	public void setGagnant(Prisonnier gagnant) {
		this.gagnant = gagnant;
	}
	

	public void lancerCombat() {
			Random r = new Random();
			int al = 0;
			int nbVictoireP1 = 0;
			int nbVictoireP2 = 0;
			
			System.out.println("C'est parti pour le Duel");
			System.out.println("____________________________________");
			System.out.println("Manche 1 - Agilite : ");
			al = r.nextInt(p1.getAgilite()+p2.getAgilite()) + 1;
			if(al<=p1.getAgilite()) {
				System.out.println("Victoire de "+ p1.getPrenomNom());
				nbVictoireP1++;
			}else {
				System.out.println("Victoire de "+ p2.getPrenomNom());
				nbVictoireP2++;
			}
			
			System.out.println("Manche 2 - Endurance : ");
			al = r.nextInt(p1.getEndurance()+p2.getEndurance()) + 1;
			if(al<=p1.getEndurance()) {
				System.out.println("Victoire de "+ p1.getPrenomNom());
				nbVictoireP1++;
			}else {
				System.out.println("Victoire de "+ p2.getPrenomNom());
				nbVictoireP2++;
			}
			
			System.out.println("Manche 3 - Esprit : ");
			al = r.nextInt(p1.getEsprit()+p2.getEsprit()) + 1;
			if(al<=p1.getEsprit()) {
				System.out.println("Victoire de "+ p1.getPrenomNom());
				nbVictoireP1++;
			}else {
				System.out.println("Victoire de "+ p2.getPrenomNom());
				nbVictoireP2++;
			}
			
			System.out.println("Manche 4 - Force : ");
			al = r.nextInt(p1.getForce()+p2.getForce()) + 1;
			if(al<=p1.getForce()) {
				System.out.println("Victoire de "+ p1.getPrenomNom());
				nbVictoireP1++;
			}else {
				System.out.println("Victoire de "+ p2.getPrenomNom());
				nbVictoireP2++;
			}
			
			System.out.println("Manche 5 - Intelligence : ");
			al = r.nextInt(p1.getIntelligence()+p2.getIntelligence()) + 1;
			if(al<=p1.getIntelligence()) {
				System.out.println("Victoire de "+ p1.getPrenomNom());
				nbVictoireP1++;
			}else {
				System.out.println("Victoire de "+ p2.getPrenomNom());
				nbVictoireP2++;
			}
			
			Random r2 = new Random();
			int mort = r2.nextInt(lieu.getMorts().size());
			
			
			if(nbVictoireP1<nbVictoireP2) {
				System.out.println("Victoire finale de " + p2.getPrenomNom());
				System.out.println(p1.getPrenomNom() + lieu.getMorts().get(mort));
				p2.setVictoire();
				p2.setNbPain(painAGagner);
				gagnant = p2;
			}else {
				System.out.println("Victoire finale de " + p1.getPrenomNom());
				System.out.println(p2.getPrenomNom() + lieu.getMorts().get(mort));
				p1.setVictoire();
				p1.setNbPain(painAGagner);
				gagnant = p1;
			}
			
	}
	
	public String toString() {
		String ch = super.toString();
		ch += " opposant : \n";
		ch += "\t\t-" + p1.getPrenom() + " " + p1.getNom();
		ch += " � " + p2.getPrenom() + " " + p2.getNom();
		return ch;
	}
	
}
