package metier.combat;

import java.io.Serializable;

public abstract class Combat implements Serializable{
	protected int id;
	protected String nom;
	protected TypeLieu lieu;
	protected int painAGagner;
	
	public Combat(String nom, TypeLieu lieu, int painAGagner) {
		this.nom = nom;
		this.lieu = lieu;
		this.painAGagner = painAGagner;
	}
	
	public int getId() {
		return id;
	}
	public String getNom() {
		return nom;
	}
	public TypeLieu getLieu() {
		return lieu;
	}
	public int getNbpainAGagner() {
		return painAGagner;
	}
	
	public abstract void lancerCombat();

	
	public String toString() {
		String ch = "";
		ch += "Le combat : " + nom + " lieu : " + lieu.getNom() + " rapporte " + painAGagner + "P";
		return ch;
	}
	
	
}
