package metier.stuff;

public enum Stuff {
	Branche(1,"Branche",5,1,2),
	Poele(2,"Poele",500,4,200);
	
	private int id;
	private String nom;
	private int cout;
	private int categegorie;
	private int gain;
	
	private Stuff(int id, String nom, int cout, int categegorie,int gain) {
		this.id = id;
		this.nom = nom;
		this.cout = cout;
		this.categegorie = categegorie;
		this.gain = gain;
	}

	public String getNom() {
		return nom;
	}
	public int getId() {
		return id;
	}
	public int getCout() {
		return cout;
	}
	public int getCategegorie() {
		return categegorie;
	}
	public int getGain() {
		return gain;
	}
}
