package test;

import java.util.ArrayList;
import java.util.List;

import metier.combat.GuerreDesGangs;
import metier.combat.TypeLieu;
import metier.gang.Gang;
import metier.gang.TypeGang;
import metier.personne.Prisonnier;

public class TestJeu {

	public static void main(String[] args) {
		
		Prisonnier p1 = new Prisonnier("qp", "1234", "Quentin", "P", 23, 60, 60, 60, 60, 60);
		Prisonnier p2 = new Prisonnier("ajc", "ajc", "Chris", "A", 31, 50, 50, 50, 50, 50);
		Prisonnier p3 = new Prisonnier("a", "b", "Jordan", "A", 26, 50, 50, 50, 50, 50);
		Prisonnier p4 = new Prisonnier("a", "b", "Marc", "A", 26, 50, 50, 50, 50, 50);
		Prisonnier p5 = new Prisonnier("a", "b", "Remi", "A", 26, 50, 50, 50, 50, 50);
		Prisonnier p6 = new Prisonnier("a", "b", "Alexis", "A", 26, 50, 50, 50, 50, 50);
		
		List<Prisonnier> prisonniers = new ArrayList<Prisonnier>();
		prisonniers.add(p1);
		prisonniers.add(p2);
		prisonniers.add(p3);
		prisonniers.add(p4);
		prisonniers.add(p5);
		prisonniers.add(p6);
		
		Gang g1 = new Gang("F�roce", TypeGang.ChaussetteBlanche);
		g1.ajouterMembre(p1);
		g1.ajouterMembre(p3);
		g1.ajouterMembre(p5);
		
		Gang g2 = new Gang("Malin",TypeGang.CasqueBleu);
		g2.ajouterMembre(p2);
		g2.ajouterMembre(p4);
		g2.ajouterMembre(p6);
		
		
		GuerreDesGangs gdg = new GuerreDesGangs("Moscou", TypeLieu.NEIGE, 5, g1, g2);
		gdg.lancerCombat();
		
		List<Prisonnier> vivants = new ArrayList<Prisonnier>();
		for(Prisonnier p : prisonniers) {
			for(Prisonnier pp : g1.getPrisonniers()) {
				if(p == pp) {
					vivants.add(p);
				}
			}
			for(Prisonnier pp : g2.getPrisonniers()) {
				if(p == pp) {
					vivants.add(p);
				}
			}
		}
		
		prisonniers.removeAll(prisonniers);
		prisonniers.addAll(vivants);
		
		System.out.println("Prisonniers : \n\t"+prisonniers);
		
		/*BattleRoyal r = new BattleRoyal("Le self", TypeLieu.CUISINE, prisonniers);
		r.lancerCombat();*/
		
		/*Duel d = new Duel("Le Bloc C", TypeLieu.CUISINE, p1, p2);
		d.lancerCombat();*/
		
	}

}
