package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import metier.Goulag;
import metier.combat.BattleRoyal;
import metier.combat.Combat;
import metier.combat.Duel;
import metier.combat.GuerreDesGangs;
import metier.combat.TypeLieu;
import metier.gang.Gang;
import metier.gang.TypeGang;
import metier.personne.Directeur;
import metier.personne.Personnage;
import metier.personne.Prisonnier;
import metier.stuff.Stuff;

public class Simulateur {
	static Goulag goulag = null;
	static Personnage user = null;
	
	/*******************************************
	 ****************SCANNER********************
	 *******************************************/
	static Scanner scString = new Scanner(System.in);
	static Scanner scInt = new Scanner(System.in);
	static Scanner scDouble = new Scanner(System.in);
	
	
	/*******************************************
	 ********GESTION FICHIER GOULAG*************
	 *******************************************/
	static final String cheminGoulag = "fichier\\goulag.txt";
	public static Goulag chargerGoulag() {
		File f = new File(cheminGoulag);
		Goulag goulag = null;
		try {
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);

			goulag = (Goulag) ois.readObject();
			
			ois.close();
			fis.close();
		} catch (IOException e) {
			System.out.println("Il n'y a pas de partie sauvegarder !");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return goulag;
	}
	
	public static void sauvegarderGoulag(Goulag goulag) {
		File f = new File(cheminGoulag);
		try {
			FileOutputStream fos = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(goulag);
			
			oos.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/*******************************************
	 ********GESTION NOUVEAU COMPTE*************
	 *******************************************/
	
	public static String login() {
		System.out.print("Entrez votre pseudo : ");
		String login = scString.nextLine();
		return login.toLowerCase();
	}
	
	public static boolean existeLogin(String login) {
		if(goulag.getDirecteur().getLogin().equals(login)) {
			return true;
		}
		for(Prisonnier p : goulag.getPrisonniersVivants()) {
			if(p.getLogin().equals(login)) {
				return true;
			}
		}
		for(Prisonnier p : goulag.getPrisonniersMorts()) {
			if(p.getLogin().equals(login)) {
				return true;
			}
		}
		return false;
	}
	
	public static String password() {
		System.out.print("Entrez votre password : ");
		String password = scString.nextLine();
		return password;
	}
	
	public static String prenom() {
		System.out.print("Entrez votre pr�nom : ");
		String prenom = scString.nextLine();
		return prenom;
	}
	
	public static String nom() {
		System.out.print("Entrez votre nom : ");
		String nom = scString.nextLine();
		return nom;
	}
	
	public static int age() {
		System.out.print("Entrez votre age : ");
		int age;
		age = scInt.nextInt();
		return age;
	}
	
	public static int point(String nom) {
		System.out.print("Entrer un nombre de point "+nom+" : ");
		int point = scInt.nextInt();
		while(point < 0) {
			System.out.println("Le nombre de point est inf�rieur � 0 !");
			System.out.print("Entrer un nombre de point pour "+nom+" : ");
			point = scInt.nextInt();
		}
		return point;
	}
	
	/*******************************************
	 ********GESTION NOUVEAU GOULAG*************
	 *******************************************/
	
	//Cr�er le Goulag
	public static Goulag creerGoulag() {
		Goulag g = null;
		System.out.print("Donnez un nom � votre Goulag : ");
		String nom = scString.nextLine();
		Directeur directeur = creerDirecteur();
		g = Goulag.getInstance(nom, directeur);
		return g;
	}
	
	//Cr�er le directeur du Goulag
	public static Directeur creerDirecteur() {
		Directeur d = null;
		String prenom = prenom();
		String nom = nom();
		String login = login();
		String password = password();
		int age = age();
		d = Directeur.getInstance(login, password, prenom, nom, age);
		return d;
	}
	
	/*******************************************
	 ***********GESTION DIRECTEUR***************
	 *******************************************/
	
	public static void creerPrisonnier() {
		String prenom = prenom();
		String nom = nom();
		String login = login();
		while(existeLogin(login)) {
			System.out.println("Le login existe d�j� !");
			login = login();
		}
		String password = password();
		int age = age();
		int force = point("la force");
		int agilite = point("l'agilit�");
		int endurance = point("l'endurence");
		int intelligence = point("l'intelligence");
		int esprit = point("l'esprit");
		Prisonnier p = new Prisonnier(login, password, prenom, nom, age, force, agilite, endurance, intelligence, esprit);
		goulag.ajouterPrisonnier(p);
	}
	
	public static void listerPrisonnier() {
		System.out.println("___________________________________________");
		System.out.println("Liste des prisonniers vivants : ");
		goulag.listerPrisonniersVivants();
		System.out.println("Liste des prisonniers morts : ");
		goulag.listerPrisonniersMorts();
		
	}
	
	public static int listerPrisonnierVivant() {
		int index = 1;
		if(goulag.getPrisonniersVivants().size()<1) {
			System.out.println("Il n'y a pas de prisonniers !");
		}else {
			for(Prisonnier p : goulag.getPrisonniersVivants()) {
				System.out.println((index++)+" - "+p.getPrenomNom());
			}
		}
		return index;
	}
	
	public static void tuerPrisonnier() {
		System.out.println("___________________________________________");
		System.out.println("TUER PRISONNIER");
		int nbP = listerPrisonnierVivant();
		if(nbP>1) {
			System.out.print("Tapez le num�ro du prisonnier � tuer : ");
			int choix = scInt.nextInt();
			while((choix-1)<0 || choix>goulag.getPrisonniersVivants().size()) {
				System.out.println("Le prisonnier n'existe pas !");
				System.out.println("___________________________________________");
				System.out.println("TUER PRISONNIER");
				listerPrisonnierVivant();
				System.out.print("Tapez le num�ro du prisonnier � tuer : ");
				choix = scInt.nextInt();
			}
			Prisonnier p = goulag.getPrisonniersVivants().get(choix-1);
			goulag.tuerPrisonnier(p);
		}
	}
	
	public static void gestionPrisonnierDirecteur() {
		System.out.println("___________________________________________");
		System.out.println("GESTION PRISONNIER");
		System.out.println("1 - Cr�er un prisonnier");
		System.out.println("2 - Lister les prisonniers");
		System.out.println("3 - Tuer un prisonnier");
		System.out.println("Autre - Retour");
		System.out.print("Tapez votre choix : ");
		String choix = scString.nextLine();
		System.out.println("");
		
		switch(choix) {
			case "1":
				creerPrisonnier();
				gestionPrisonnierDirecteur();
				break;
			case "2":
				listerPrisonnier();
				gestionPrisonnierDirecteur();
				break;
			case "3":
				tuerPrisonnier();
				gestionPrisonnierDirecteur();
				break;
			default:
				gestionDirecteur();
		}
	}
	
	public static String nomCombat() {
		String nom = "";
		System.out.print("Comment s'appelle votre Combat : ");
		nom = scString.nextLine();
		return nom;
	}
	
	public static int pointAGagner() {
		int nb = 0;
		System.out.print("Combien rapporte votre Combat (par Duel)  : ");
		nb = scInt.nextInt();
		while(nb<0) {
			System.out.println("Le nombre de points est inf�rieur � 0 !");
			System.out.print("Combien rapporte votre Combat (par Duel)  : ");
			nb = scInt.nextInt();
		}
		return nb;
	}
	
	public static void listTypeLieu() {
		int nb = 1;
		for(TypeLieu type : TypeLieu.values()) {
			System.out.println("\t"+(nb++)+" - "+type.getNom());
		}
	}
	
	public static TypeLieu typeLieu() {
		TypeLieu type = null;
		System.out.println("Choix du lieu : ");
		listTypeLieu();
		System.out.print("Tapez votre choix : ");
		int choix = scInt.nextInt();
		while(choix<1 || choix>TypeLieu.values().length) {
			System.out.println("Le num�ro n'est pas bon !");
			System.out.println("Choix du lieu : ");
			listTypeLieu();
			System.out.print("Tapez votre choix : ");
			choix = scInt.nextInt();
		}
		type = TypeLieu.values()[choix-1];
		return type;
	}
	
	public static int listerGang() {
		System.out.println("___________________________________________");
		System.out.println("LISTE DES GANG");
		int nb = 1;
		if(goulag.getGang().size()<1) {
			System.out.println("Il n'y a pas de gang !");
		}else {
			for(Gang g : goulag.getGang()) {
				System.out.println("\t"+(nb++)+" - "+g.getNom());
			}
		}
		return nb;
	}
	
	public static Gang choixGang() {
		Gang g = null;
		System.out.println("Choix du gang : ");
		listerGang();
		System.out.print("Tapez votre choix : ");
		int choix = scInt.nextInt();
		while(choix<1 || choix>goulag.getGang().size()) {
			System.out.println("Le num�ro n'est pas bon !");
			System.out.println("Choix du gang : ");
			listerGang();
			System.out.print("Tapez votre choix : ");
			choix = scInt.nextInt();
		}
		g = goulag.getGang().get(choix-1);
		return g;
	}
	
	
	public static Prisonnier choixPrisonnier() {
		Prisonnier p = null;
		int nbP = listerPrisonnierVivant();
		if(nbP>1) {
			System.out.print("Tapez le num�ro du prisonnier : ");
			int choix = scInt.nextInt();
			while((choix-1)<0 || choix>goulag.getPrisonniersVivants().size()) {
				System.out.println("Le prisonnier n'existe pas !");
				listerPrisonnierVivant();
				System.out.print("Tapez le num�ro du prisonnier : ");
				choix = scInt.nextInt();
			}
			p = goulag.getPrisonniersVivants().get(choix-1);
		}
		return p;
	}
	
	
	public static void creerDuel() {
		System.out.println("___________________________________________");
		System.out.println("CREER UN DUEL");
		if(goulag.getPrisonniersVivants().size()<2) {
			System.out.println("Il n'y a pas assez de prisonniers");
		}else {
			String nom = nomCombat();
			TypeLieu lieu = typeLieu();
			int painAGagner = pointAGagner();
			Prisonnier p1 = choixPrisonnier();
			Prisonnier p2 = choixPrisonnier();
			while(p1.equals(p2)) {
				System.out.println("Les deux prisonniers sont les m�ms !");
				p2 = choixPrisonnier();
			}
			Duel d = new Duel(nom, lieu, painAGagner, p1, p2);
			System.out.println("LANCER COMBAT");
			d.lancerCombat();
			if(d.getGagnant() == p1) {
				goulag.tuerPrisonnier(p2);
			}else {
				goulag.tuerPrisonnier(p1);;
			}
			goulag.archiverCombat(d);
		}
	}
	
	public static void creerGuerreDesGang() {
		System.out.println("___________________________________________");
		System.out.println("CREER UNE GUERRE DES GANGS");
		if(goulag.getGang().size()<2) {
			System.out.println("Il n'y a pas assez de gangs");
		}else {
			String nom = nomCombat();
			TypeLieu lieu = typeLieu();
			int painAGagner = pointAGagner();
			Gang gang1 = choixGang();
			Gang gang2 = choixGang();
			while(gang1.equals(gang2)) {
				System.out.println("Les deux gangs sont les m�mes !");
				gang2 = choixGang();
			}
			List<Prisonnier> tmp1 = new ArrayList<Prisonnier>(gang1.getPrisonniers());
			List<Prisonnier> tmp2 = new ArrayList<Prisonnier>(gang2.getPrisonniers());
			List<Prisonnier> tmpGoulag = new ArrayList<Prisonnier>(goulag.getPrisonniersVivants());
			
			GuerreDesGangs gdg = new GuerreDesGangs(nom, lieu, painAGagner, gang1, gang2);
			System.out.println("LANCER COMBAT");
			gdg.lancerCombat();
			
			System.out.println("gagnant "+gdg.getGagnant().getPrisonniers());
			
			System.out.println("tmp1 : "+tmp1);
			for(Prisonnier p : tmp1) {
				for(Prisonnier pp : gdg.getGagnant().getPrisonniers()) {
					if(p.getId() != pp.getId()) {
						System.out.println("passe par l�");
						goulag.tuerPrisonnier(p);
						p.quitterLeGang();
					}
				}
			}
			
			System.out.println("tmp 2 : "+tmp2);
			for(Prisonnier p : tmp2) {
				System.out.println("hey ooo");
				for(Prisonnier pp : gdg.getGagnant().getPrisonniers()) {
					System.out.println("hey ooo 2 hey ");
					if(p.getId() != pp.getId()) {
						System.out.println("passe par l�");
						goulag.tuerPrisonnier(p);
						p.quitterLeGang();
					}
				}
			}
			
			goulag.archiverCombat(gdg);
		}
	}
	
	public static void creerBattleRoyale() {
		System.out.println("___________________________________________");
		System.out.println("BATTLE ROYALE");
		if(goulag.getPrisonniersVivants().size()<2) {
			System.out.println("Il n'y a pas assez de prisonniers");
		}else {
			List<Prisonnier> tmp = new ArrayList<Prisonnier>();
			tmp.addAll(goulag.getPrisonniersVivants());
			String nom = nomCombat();
			TypeLieu lieu = typeLieu();
			int painAGagner = pointAGagner();
			BattleRoyal b = new BattleRoyal(nom, lieu, painAGagner, goulag.getPrisonniersVivants());
			System.out.println("LANCER COMBAT");
			b.lancerCombat();
			for(Prisonnier p : tmp) {
				if(b.getGagnant().getId() != p.getId()) {
					goulag.tuerPrisonnier(p);
				}
			}
			goulag.archiverCombat(b);
		}
	}
	
	public static void creerCombat() {
		System.out.println("___________________________________________");
		System.out.println("CREER UN COMBAT");
		System.out.println("1 - Duel");
		System.out.println("2 - Guerre des Gangs");
		System.out.println("3 - Battle Royale");
		System.out.println("Autre - Retour");
		System.out.print("Tapez votre choix : ");
		String choix = scString.nextLine();
		
		switch(choix) {
			case "1":
				creerDuel();
				break;
			case "2":
				creerGuerreDesGang();
				break;
			case "3":
				creerBattleRoyale();
				break;
			default:
				gestionCombatDirecteur();
		}
	}
	
	public static void historiqueCombats() {
		System.out.println("___________________________________________");
		System.out.println("HISTORIQUE DES COMBATS");
		goulag.listerCombatsHistorique();
	}	
	
	public static void gestionCombatDirecteur() {
		System.out.println("___________________________________________");
		System.out.println("GESTION COMBAT");
		System.out.println("1 - Cr�er un combat");
		System.out.println("2 - Historique des combats");
		System.out.println("Autre - Retour");
		System.out.print("Tapez votre choix : ");
		String choix = scString.nextLine();
		
		switch(choix) {
			case "1":
				creerCombat();
				gestionCombatDirecteur();
				break;
			case "2":
				historiqueCombats();
				gestionCombatDirecteur();
				break;
			default:
				gestionDirecteur();
		}
	}
	
	
	public static void gestionGangDirecteur() {
		System.out.println("___________________________________________");
		System.out.println("GESTION GANG");
		System.out.println("1 - Lister les membres d'un gang");
		System.out.println("Autre - Retour");
		System.out.print("Tapez votre choix : ");
		String choix = scString.nextLine();
		
		switch(choix) {
			case "1":
				listerGang();
				gestionGangDirecteur();
				break;
			default:
				gestionDirecteur();
		}
	}
	
	public static void changerNomGoulag() {
		System.out.println("___________________________________________");
		System.out.println("CHANGER LE NOM DU GOULAG");
		System.out.print("Entrer le nouveau nom du Goulag : ");
		String nom = scString.nextLine();
		goulag.setNom(nom);
		gestionDirecteur();		
	}
	
	public static void informationGoulag() {
		System.out.println("___________________________________________");
		System.out.println("INFORMATIONS SUR LE GOULAG");
		System.out.println(goulag);
		gestionDirecteur();
	}
	
	public static void gestionDirecteur() {
		System.out.println("___________________________________________");
		System.out.println("Bonjour Monsieur le Directeur "+user.getPrenomNom());
		System.out.println("Que voulez-vous faire ?");
		System.out.println("1 - Gestion des prisonniers");
		System.out.println("2 - Gestion des combats");
		System.out.println("3 - Gestion des gangs");
		System.out.println("4 - Changer le nom du Goulag");
		System.out.println("5 - Voir les informations du Goulag");
		System.out.println("Autre - Se d�connecter");
		System.out.print("Tapez votre choix : ");
		String choix = scString.nextLine();
		
		switch(choix) {
			case "1":
				gestionPrisonnierDirecteur();
				break;
			case "2":
				gestionCombatDirecteur();
				break;
			case "3":
				gestionGangDirecteur();
				break;
			case "4":
				changerNomGoulag();
				break;
			case "5":
				informationGoulag();
				break;
			default:
				jouer();
		}
	}
	
	/*******************************************
	 ***********GESTION PRISONNIER**************
	 *******************************************/
	
	public static String nomGang() {
		String nom;
		System.out.print("Quel nom voulez-vous donner pour votre gang : ");
		nom = scString.nextLine();
		return nom;
	}
	
	
	public static void listTypeGang() {
		int nb = 1;
		for(TypeGang type : TypeGang.values()) {
			System.out.println("\t"+(nb++)+" - "+type.getNom());
		}
	}
	
	public static TypeGang typeGang() {
		TypeGang type = null;
		System.out.println("Choix du type de gang : ");
		listTypeGang();
		System.out.print("Tapez votre choix : ");
		int choix = scInt.nextInt();
		while(choix<1 || choix>TypeGang.values().length) {
			System.out.println("Le num�ro n'est pas bon !");
			System.out.println("Choix du type de gang : ");
			listTypeGang();
			System.out.print("Tapez votre choix : ");
			choix = scInt.nextInt();
		}
		type = TypeGang.values()[choix-1];
		return type;
	}
	
	
	public static void creerGang() {
		System.out.println("___________________________________________");
		System.out.println("CREER GANG");
		if(((Prisonnier)user).getGang() != null) {
			System.out.println("Vous faite d�j� parti d'un gang !");
		}else {
			String nom = nomGang();
			TypeGang type = typeGang();
			Gang g = new Gang(nom, type);
			((Prisonnier) user).creerUnGang(nom,type);
			g.ajouterMembre((Prisonnier)user);
			goulag.ajouterGang(g);
		}
		gestionGangPrisonnier();
	}
	
	
	public static void rejoindreGang() {
		System.out.println("___________________________________________");
		System.out.println("REJOINDRE GANG");
		if(goulag.getGang().size()<1) {
			System.out.println("Il n'y a pas de gang !");
		}else if(((Prisonnier) user).getGang() != null){
			System.out.println("Vous faites d�j� parti d'un gang !");
		}else {
			Gang g = null;
			g = choixGang();
			((Prisonnier) user).entrerDansUnGang(g);
		}
		gestionGangPrisonnier();
	}
	
	public static void quitterGang() {
		System.out.println("___________________________________________");
		System.out.println("QUITTER GANG");
		if(((Prisonnier) user).getGang() == null) {
			System.out.println("Vous faites parti d'aucun gang !");
		}else {
			((Prisonnier) user).quitterLeGang();
		}
		gestionGangPrisonnier();
	}
	
	public static void gestionGangPrisonnier() {
		System.out.println("___________________________________________");
		System.out.println("GESTION GANG");
		System.out.println("Que voulez-vous faire ?");
		System.out.println("1 - Cr�er un gang");
		System.out.println("2 - Rejoindre un gang");
		System.out.println("3 - Quitter le gang");
		System.out.println("Autre - Retour");
		System.out.println("Tapez votre r�ponse : ");
		String choix = scString.nextLine();
		
		switch(choix) {
		case "1":
			creerGang();
			break;
		case "2":
			rejoindreGang();
			break;
		case "3":
			quitterGang();
			break;
		default:
			gestionPrisonnier();
		}
	}
	
	public static void mesCombats() {
		System.out.println("___________________________________________");
		System.out.println("MES COMBATS");
		for(Combat c : goulag.getCombatsHistorique()) {
			if(c instanceof Duel) {
				if(((Prisonnier) user) == ((Duel) c).getGagnant()) {
					System.out.println("\t- "+c);
				}
			}else if(c instanceof BattleRoyal) {
				if(((Prisonnier) user) == ((BattleRoyal) c).getGagnant()) {
					System.out.println("\t- "+c);
				}
			}else if(c instanceof GuerreDesGangs) {
				if(((Prisonnier) user).getGang() == ((GuerreDesGangs) c).getGagnant()) {
					System.out.println("\t- "+c);
				}
			}
		}
		gestionCombatPrisonnier();
	}
	
	public static void gestionCombatPrisonnier() {
		System.out.println("___________________________________________");
		System.out.println("GESTION COMBAT");
		System.out.println("Que voulez-vous faire ?");
		System.out.println("1 - Voir mes combats");
		System.out.println("2 - Retour");
		System.out.print("Tapez votre r�ponse : ");
		String choix = scString.nextLine();
		
		switch(choix) {
		case "1":
			mesCombats();
			break;
		default:
			gestionPrisonnier();
		}
	}
	public static void listerStuff() {
		int index = 1;
		for(Stuff s : Stuff.values()) {
			System.out.println((index++)+" - "+s.getNom() + " "+s.getCout()+"P"+ " "+s.getGain()+" point pour "+s.getCategegorie());
		}
	}
	
	public static Stuff choixStuff() {
		Stuff s = null;
		System.out.println("Choix du stuff : ");
		listerStuff();
		System.out.print("Tapez votre choix : ");
		int choix = scInt.nextInt();
		while(choix<1 || choix>Stuff.values().length) {
			System.out.println("Le num�ro n'est pas bon !");
			System.out.println("Choix du stuff : ");
			listerStuff();
			System.out.print("Tapez votre choix : ");
			choix = scInt.nextInt();
		}
		s = Stuff.values()[choix - 1];
		return s;
	}
	
	public static void gestionStuff() {
		System.out.println("___________________________________________");
		System.out.println("GESTION STUFF");
		Stuff s = choixStuff();
		((Prisonnier) user).acheterDuStuff(s);
		gestionPrisonnier();
	}
	
	public static void informationPrisonnier() {
		System.out.println("___________________________________________");
		System.out.println("MES INFORMATIONS");
		System.out.println(user);
		gestionPrisonnier();
	}
	
	public static void tricher() {
		System.out.println("___________________________________________");
		System.out.println("TRICHER");
		System.out.print("Tapez votre code : ");
		String code = scString.nextLine();
		((Prisonnier) user).tricher(code);
		gestionPrisonnier();
	}
	
	public static void gestionPrisonnier() {
		System.out.println("___________________________________________");
		System.out.println("Bonjour Prisonnier "+user.getPrenomNom());
		System.out.println("Que voulez-vous faire ?");
		System.out.println("1 - Gestion des gangs");
		System.out.println("2 - Gestion des combats");
		System.out.println("3 - Acheter du Stuff");
		System.out.println("4 - Voir mes informations");
		System.out.println("5 - Tricher");
		System.out.println("Autre - Se d�connecter");
		System.out.print("Tapez votre choix : ");
		String choix = scString.nextLine();
		
		switch(choix) {
		case "1":
			gestionGangPrisonnier();
			break;
		case "2":
			gestionCombatPrisonnier();
			break;
		case "3":
			gestionStuff();
			break;
		case "4":
			informationPrisonnier();
			break;
		case "5":
			tricher();
			break;
		default:
			jouer();
		}
	}
	

	/*******************************************
	 ***********GESTION CONNEXION***************
	 *******************************************/
	public static void seConnecter() {
		System.out.print("login : ");
		String login = scString.nextLine().toLowerCase();
		System.out.print("password : ");
		String password = scString.nextLine();
		
		if(login.equals(goulag.getDirecteur().getLogin()) && password.equals(goulag.getDirecteur().getPassword())) {
			user = goulag.getDirecteur();
			gestionDirecteur();
		}else {
			boolean trouve = false;
			for(Prisonnier p : goulag.getPrisonniersVivants()) {
				if(login.equals(p.getLogin()) && password.equals(p.getPassword())) {
					user = p;
					gestionPrisonnier();
					trouve = true;
					break;
				}
			}
			if(!trouve) {
				for(Prisonnier p : goulag.getPrisonniersMorts()) {
					if(login.equals(p.getLogin()) && password.equals(p.getPassword())) {
						System.out.println(p.getPrenom()+" "+p.getNom()+" vous ne pouvez pas vous connecter, car vous �tes mort !");
						break;
					}		
				}
				jouer();
			}
		}
	}
	
	/*******************************************
	 *************LANCER LE JEU*****************
	 *******************************************/
	public static void jouer() {
		System.out.println("___________________________________________");
		System.out.println("Que voulez-vous faire ?");
		System.out.println("1 - Se connecter");
		System.out.println("Autre - Quitter l'aplication");
		System.out.print("Tapez votre r�ponse : ");
		String choix = scString.nextLine();
		if(choix.equals("1")) {
			seConnecter();
		}else {
			System.out.println("A bient�t !");
		}
	}
	

	public static void main(String[] args) {
		goulag = chargerGoulag();
		
		if(goulag == null) {
			System.out.println("Bienvenue dans Goulag Simulator 2019");
			System.out.println("Vous allez devoir cr�er votre Goulag et votre Directeur \navant de pouvoir jouer pleinement � Goulag Simulator 2019 !");
			System.out.println("____________________________________________________");
			goulag = creerGoulag();
			jouer();
			sauvegarderGoulag(goulag);
		}else {
			System.out.println("Bienvenue dans Goulag Simulator 2019");
			System.out.println(goulag);
			jouer();
			sauvegarderGoulag(goulag);
		}
		
	}

}
