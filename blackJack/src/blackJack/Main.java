package blackJack;

import java.util.Random;
import java.util.Scanner;

public class Main {
	
	public static String couleurs[] = {"Coeur","Carreau","Tr�fle","Pique"};
	public static String valeurs[] = {"1","2","3","4","5","6","7","8","9","10","V","D","R"};
	public static Scanner scInt = new Scanner(System.in);
	public static Random rand = new Random();
	
	public static String[] initCartes() {
		String cartes[] = new String[52];
		int rang=0;
		
		for(int i=0;i<couleurs.length;i++) {
			for(int j=0;j<valeurs.length;j++) {
				cartes[rang++] = valeurs[j]+" "+couleurs[i];
			}
		}
		
		return cartes;
	}
	
	public static void afficherCartes(String cartes[]) {
		for(String c : cartes)
			System.out.println(c);
	}
	
	public static int calculPoints(String v) {
		if(v.equals("V") || v.equals("D") || v.equals("R") || v.equals("10")) {
			return 10;
		}else if(v.equals("1")) {
			return 1;
		}else {
			switch(v) {
			case "2":
				return 2;
			case "3":
				return 3;
			case "4":
				return 4;
			case "5":
				return 5;
			case "6":
				return 6;
			case "7":
				return 7;
			case "8":
				return 8;
			case "9":
				return 9;
			default:
				System.out.println("erreur");
				return 0;
			}
		}
	}
	
	public static void jouer() {
		String cartes[] = initCartes();
		String carteCache = "Aucune";
		
		//Nombre de jeux de carte
		int nombrePaquets = 0;
		do {
				System.out.println("Combien de jeux de carte, voulez-vous (entre 1 et 8)?");
				nombrePaquets = scInt.nextInt();
		}while(nombrePaquets<=0 && nombrePaquets>=8);
		
		
		//r�currence de la cartes
		int recCaretes[] = new int[52];
		
		System.out.println("Entrer un nombre de joueur entre 1 et 5");
		int nbJoueur = scInt.nextInt();
		while(nbJoueur <= 0 || nbJoueur >= 6) {
			System.out.println("Le nombre de joueur est incorrecte entre 1 et 5");
			nbJoueur = scInt.nextInt();
		}
		
		// tab des points des joueurs + le croupier en dernier
		int pointsJoueurs[] = new int[nbJoueur+1];
		
		//donne de carte
		int tour=0;
		
		System.out.println("**********Tour pr�alable ***************");
		while(tour != 2) {
			System.out.println("Tour n�"+(tour+1));
			
			for(int i=0;i<pointsJoueurs.length;i++) {

				//On distribut une carte tout le monde
				int numCarte = rand.nextInt(52);
				
				//tant que la carte est indisponible on repioche une carte
				while(recCaretes[numCarte] >=nombrePaquets) {
					numCarte = rand.nextInt(52);
				}

				String tabCouVal[] = cartes[numCarte].split(" ");
				String v = tabCouVal[0];

				pointsJoueurs[i] += calculPoints(v);

				int num = i+1;
				if(num != pointsJoueurs.length) {
					System.out.println("Joueur n�"+num+" a "+pointsJoueurs[i]+" points apr�s avoir tir� un "+cartes[numCarte]);
				}else {
					if(tour != 1) {
						System.out.println("Le croupier a "+pointsJoueurs[i]+" points apr�s avoir tir� un "+cartes[numCarte]);
					}
					carteCache = cartes[numCarte];
				}
			}
			
			//Deuxi�me tour
			tour++;
		}
		
		System.out.println("***********Tour joueur***********");
		
		for(int i=0;i<pointsJoueurs.length-1;i++) {
			System.out.println("Joueur n�"+(i+1));
			System.out.println("Vous avez "+pointsJoueurs[i]+" points");
			
			boolean next = true;
			while(next) {
				System.out.println("Voulez-vous continuer ?");
				System.out.println("1 - oui");
				System.out.println("2 - non");
				int choix = scInt.nextInt();
				System.out.println(choix);
				while(choix != 1 && choix != 2) {
					System.out.println("Incorrecte !");
					System.out.println("Voulez-vous continuer ?");
					System.out.println("1 - oui");
					System.out.println("2 - non");
					choix = scInt.nextInt();				
				}
				
				if(choix == 1) {
					
					//On distribut une carte tout le monde
					int numCarte = rand.nextInt(52);
					
					//tant que la carte est indisponible on repioche une carte
					while(recCaretes[numCarte] >=nombrePaquets) {
						numCarte = rand.nextInt(52);
					}

					System.out.println("Vous avez tir� "+cartes[numCarte]);
					String tabCouVal[] = cartes[numCarte].split(" ");
					String v = tabCouVal[0];

					pointsJoueurs[i] += calculPoints(v);
					
					System.out.println("Vous avez maintenant "+pointsJoueurs[i]+" points");
					if(pointsJoueurs[i]>21) {
						next = false;
						System.out.println("Vous �tes �limin�, vous avez plus de 21 points");
					}
					
				}else {
					next = false;
				}
			}
		}
		
		System.out.println("********Tour croupier********");
		
		int croup = pointsJoueurs.length-1;
		System.out.println("La carte cach� du croupier �tait "+ carteCache +",le croupier a donc "+pointsJoueurs[croup]+" points");
		while(pointsJoueurs[croup] <= 16) {
			//On distribut une carte tout le monde
			int numCarte = rand.nextInt(52);
			
			//tant que la carte est indisponible on repioche une carte
			while(recCaretes[numCarte] >=nombrePaquets) {
				numCarte = rand.nextInt(52);
			}

			System.out.println("Le croupier a tir� "+cartes[numCarte]);
			String tabCouVal[] = cartes[numCarte].split(" ");
			String v = tabCouVal[0];

			pointsJoueurs[croup] += calculPoints(v);
			
			System.out.println("Croupier a "+pointsJoueurs[croup]+" points");
		}
		System.out.println("******Compte des cartes*********");
		//Si le croupier a perdu
		if(pointsJoueurs[croup] > 21) {
			System.out.println("Les joueurs restant ont gagn� car le croupier a plus de 21 points :");
			for(int j=0;j<pointsJoueurs.length-1;j++) {
				if(pointsJoueurs[j]<=21) {
					System.out.println("Joueur n�"+(j+1)+" avec "+pointsJoueurs[j]+ " points");
				}
			}
		}else if(pointsJoueurs[croup] == 21) {
			for(int j=0;j<pointsJoueurs.length-1;j++) {
				if(pointsJoueurs[j]==21) {
					System.out.println("Le joueur n�"+(j+1)+" ne gagne pas mais reprend sa mise!");
				}
			}
		}else {
			System.out.println("Les joueurs suivant ont gagn� car -21 points et plus de points que le croupier :");
			for(int j=0;j<pointsJoueurs.length-1;j++) {
				if(pointsJoueurs[j] > pointsJoueurs[croup] && pointsJoueurs[j] <= 21) {
					System.out.println("Joueur n�"+(j+1)+" avec "+pointsJoueurs[j]+ " points");
					if(pointsJoueurs[j] == 21) {
						System.out.println("\tVous doublez vos gains, car vous avez fait blackJack");
					}
				}
			}
		}
		
		System.out.println("Fin de partie");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		jouer();
	}

}
