<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">
<head>
<%
	String curPage = "home";
%>
<meta charset="UTF-8">
<title>Liste des élèves de Poudlard</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">


</head>
<body>

	<%@ include file="inc/nav.jsp"%>

	<main class="row">
	<div class="col-md-2 col-lg-2 col-xl-2 col-sm-2"></div>
	<div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 corps">
		<div class="table-reponsive">
			<table class="table">
				<fieldset class="scheduler-border">
					<legend class="scheduler-border">Liste des élèves de
						Poudlard</legend>
					<thead class="thead-dark">
						<th scope="col">Prénom</th>
						<th scope="col">Nom</th>
						<th scope="col">Age</th>
						<th scope="col">Maison</th>
						<th scope="col">&nbsp;</th>
					</thead>
					<tbody>
						<c:forEach items="${eleves}" var="c">
							<tr>
								<td>${c.prenom}</td>
								<td>${c.nom}</td>
								<td>${c.age}</td>
								<td>${c.maison.nom}</td>
								<td><a href="infoEleve?uid=${c.idEleve}"><img
										src="images/user.svg" alt="${c.prenom}'s Vcard"
										title="${c.prenom}'s Vcard" class="imgListeAction" /></a> <a
									href="editEleve?uid=${c.idEleve}"><img
										src="images/edit.svg" alt="Edit ${c.prenom}"
										title="Edit ${c.prenom}" class="imgListeAction" /></a> <a
									href="deleteEleve?uid=${c.idEleve}"><img
										src="images/delete.svg" alt="Delete ${c.prenom}"
										title="Delete ${c.prenom}" class="imgListeAction" /></a></td>
							</tr>
						</c:forEach>
					</tbody>
				</fieldset>
			</table>
		</div>

	</div>
	<div class="col-md-2 col-lg-2 col-xl-2 col-sm-2"></div>
	</main>

	<%@ include file="inc/footer.jsp"%>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
		integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
		integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
		crossorigin="anonymous"></script>

</body>
</html>