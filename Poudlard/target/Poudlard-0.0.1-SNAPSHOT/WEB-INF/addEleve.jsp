<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">
<head>
<% String curPage = "home"; %>
<meta charset="UTF-8">
<title>Créer élève</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">


</head>
<body>

	<%@ include file="inc/nav.jsp"%>

			<main class="row">
				<div class="col-md-2 col-lg-2 col-xl-2 col-sm-2"></div>
				<div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 corps">
					<div class="row">
						<div class="col-sm-3"></div>
						<div class="col-sm-6">
							<form action="addEleve" name="addEleve" id="formEleve" method="POST">
								<fieldset class="scheduler-border">
								<legend class="legendCenter scheduler-border">Ajouter élève</legend>
								<div class="form-group row">
								    <label for="prenom" class="col-sm-5 col-form-label">Prénom</label>
								    <div class="col-sm-7">
								      	<input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prénom" required="required">
								    </div>
								</div>
								<div class="form-group row">
								    <label for="nom" class="col-sm-5 col-form-label">Nom</label>
								    <div class="col-sm-7">
								      	<input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" required="required">
								    </div>
								</div>
								<div class="form-group row">
								    <label for="age" class="col-sm-5 col-form-label">Age</label>
								    <div class="col-sm-7">
								      	<input type="number" class="form-control" id="age" name="age" placeholder="Age" required="required">
								    </div>
								</div>
								<div class="form-group row">
								    <label for="maison" class="col-sm-5 col-form-label">Maison</label>								
								    	<select class="form-control" id="maison" name="choixMaison" required="required">
								    		<c:forEach items="${maisons}"  var="m">
								    			<option id="uidMaison" value="${m.idMaison}">${m.nom}</option>
								    		</c:forEach>
										</select>
								</div>
								<div class="form-group row">
									<div class="col-sm-8"></div>
								    <div class="col-sm-4">
								      <button type="submit" class="btn btn-primary">Ajouter Elève</button>
								    </div>
								</div>
							</fieldset>
							</form>
						</div>
						<div class="col-sm-3"></div>
					</div>
					<p id=resForm></p>
				</div>
				<div class="col-md-2 col-lg-2 col-xl-2 col-sm-2"></div>
			</main>
	
	
	<%@ include file="inc/footer.jsp"%>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
		integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
		integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
		crossorigin="anonymous"></script>

</body>
</html>