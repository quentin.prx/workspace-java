
<header class="row">
	<div class="banniere col-12">
		<img src="images\\banniere.png" height="250" width="100%">
	</div>
</header>


<nav class="row navbar navbar-expand-sm bg-dark navbar-dark">
	<!-- Brand -->
	<a class="navbar-brand" href="index"> <img
		src="images\\logo2.png" alt="Logo" style="width: 40px;">
	</a>

	<!-- Toggler/collapsibe Button -->
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#collapsibleNavbar">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="collapsibleNavbar">
		<!-- Links -->
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="index">Accueil</a>
			</li>

			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbardrop"
				data-toggle="dropdown">Maison</a>
				<div class="dropdown-menu"> 
					<a class="dropdown-item" href="listeMaison">Lister les maisons</a>
					<a class="dropdown-item" href="coupe4Maisons">Coupe des 4 maisons</a>
				</div></li>

			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbardrop"
				data-toggle="dropdown">Eleve</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="addEleve">Cr�er un �l�ve</a>
					<a class="dropdown-item" href="listeEleve">Lister les �l�ves</a>
				</div></li>

			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbardrop"
				data-toggle="dropdown">Professeur</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="addProf">Cr�er un professeur</a>
					<a class="dropdown-item" href="listeProf">Lister les professeurs</a>
				</div></li>
		</ul>
	</div>
</nav>
