package test;

import java.sql.SQLException;

import dao.DAOEleve;
import dao.DAOMaison;
import dao.DAOProfesseur;

public class Test {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		DAOEleve daoEleve = new DAOEleve();
		DAOProfesseur daoProfesseur = new DAOProfesseur();
		DAOMaison daoMaison = new DAOMaison();
		
		System.out.println("ELEVE");
		System.out.println(daoEleve.findAll());
		System.out.println(daoEleve.findById(1));
		System.out.println(daoEleve.findByName("Potter"));
		
		System.out.println("Maison");
		System.out.println(daoMaison.findAll());
		System.out.println(daoMaison.findById(1));
		System.out.println(daoMaison.findByName("Poufsouffle"));
		
		System.out.println("PROFESSEUR");
		System.out.println(daoProfesseur.findAll());
		System.out.println(daoProfesseur.findById(1));
		System.out.println(daoProfesseur.findByName("Rogue"));
	}

}
