package model;

public class Maison {
	private int idMaison;
	private String nom;
	private int score;
	private String blason;
	private Professeur professeur;
	private int idProf;
	
	public Maison(int idMaison, String nom, int score, String blason, int idProf) {
		this.idMaison = idMaison;
		this.nom = nom;
		this.score = score;
		this.blason = blason;
		this.idProf = idProf;
	}
	
	public Maison(int idMaison, String nom, int score, String blason, Professeur professeur) {
		this.idMaison = idMaison;
		this.nom = nom;
		this.score = score;
		this.blason = blason;
		this.professeur = professeur;
	}
	
	public Maison() {
		
	} // sert pour le Servlet pour pouvoir cr�er une maison vide

	public int getIdMaison() {
		return idMaison;
	}

	public void setIdMaison(int idMaison) {
		this.idMaison = idMaison;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getBlason() {
		return blason;
	}

	public void setBlason(String blason) {
		this.blason = blason;
	}

	public int getIdProf() {
		return idProf;
	}

	public void setIdProf(int idProf) {
		this.idProf = idProf;
	}
	
	public Professeur getProfesseur() {
		return professeur;
	}

	public void setIdProf(Professeur professeur) {
		this.professeur = professeur;
	}

	@Override
	public String toString() {
		return "Maison [idMaison=" + idMaison + ", nom=" + nom + ", score=" + score + ", blason=" + blason + ", idProf="
				+ idProf + "]";
	}
	
	
}
