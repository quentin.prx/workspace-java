package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Eleve;
import model.Maison;
import model.Professeur;



public class DAOMaison implements DAO<Maison,Integer>{

	public void create(Maison e) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "" );
		
        PreparedStatement ps=conn.prepareStatement("INSERT INTO maison (nom,score,blason,idProfesseurMaison) VALUES (?,?,?,?)");
        ps.setString(1,e.getNom());
        ps.setInt(2,e.getScore());
        ps.setString(3,e.getBlason());
        ps.setInt(4, e.getProfesseur().getIdProf());
        ps.executeUpdate();
        ps.close();
        conn.close();
	}
	
	public Maison findById(Integer id) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "" );
		
		String sql = "SELECT * FROM maison where idMaison=?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		
		Maison m=null;
		while(rs.next()) {
			DAOProfesseur daoProfesseur = new DAOProfesseur();
			Professeur professeur = daoProfesseur.findById(rs.getInt("idProfesseurMaison"));
			m = new Maison(rs.getInt("idMaison"),rs.getString("nom"),rs.getInt("score"),rs.getString("blason"),professeur);	
			//m = new Maison(rs.getInt("idMaison"),rs.getString("nom"),rs.getInt("score"),rs.getString("blason"),rs.getInt("idProfesseurMaison"));	
		}
				
		rs.close();
		ps.close();
		conn.close();
		return m;
	}
	
	public Maison findByName(String nom) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "");
		
		PreparedStatement ps=conn.prepareStatement("select * from maison where nom=?");
		ps.setString(1,nom);
		ResultSet rs=ps.executeQuery();
		Maison m=null;
		while(rs.next())
		{
			DAOProfesseur daoProfesseur = new DAOProfesseur();
			Professeur professeur = daoProfesseur.findById(rs.getInt("idProfesseurMaison"));
			m = new Maison(rs.getInt("idMaison"),rs.getString("nom"),rs.getInt("score"),rs.getString("blason"),professeur);	
			//m = new Maison(rs.getInt("idMaison"),rs.getString("nom"),rs.getInt("score"),rs.getString("blason"),rs.getInt("idProfesseurMaison"));
		}
		rs.close();
		ps.close();
		conn.close();
		return m;
	}
	
	public List<Maison> findAll() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "" );
		
		String sql = "SELECT * FROM maison order by nom";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		List<Maison> maisons = new ArrayList<Maison>();
		while(rs.next()) {
			DAOProfesseur daoProfesseur = new DAOProfesseur();
			Professeur professeur = daoProfesseur.findById(rs.getInt("idProfesseurMaison"));
			maisons.add(new Maison(rs.getInt("idMaison"),rs.getString("nom"),rs.getInt("score"),rs.getString("blason"),professeur));	
			//maisons.add(new Maison(rs.getInt("idMaison"),rs.getString("nom"),rs.getInt("score"),rs.getString("blason"),rs.getInt("idProfesseurMaison")));	
		}
		
		rs.close();
		ps.close();
		conn.close();
		return maisons;
	}
	
	public List<Maison> findAllOrderByScore() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "" );
		
		String sql = "SELECT * FROM maison order by score desc";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		List<Maison> maisons = new ArrayList<Maison>();
		while(rs.next()) {
			DAOProfesseur daoProfesseur = new DAOProfesseur();
			Professeur professeur = daoProfesseur.findById(rs.getInt("idProfesseurMaison"));
			maisons.add(new Maison(rs.getInt("idMaison"),rs.getString("nom"),rs.getInt("score"),rs.getString("blason"),professeur));	
			//maisons.add(new Maison(rs.getInt("idMaison"),rs.getString("nom"),rs.getInt("score"),rs.getString("blason"),rs.getInt("idProfesseurMaison")));	
		}
		
		rs.close();
		ps.close();
		conn.close();
		return maisons;
	}
	
	public void update(Maison e) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "" );		
		
        PreparedStatement ps=conn.prepareStatement("UPDATE maison SET nom=?,score=?,blason=?,idProfesseurMaison=? where idMaison=?");
        ps.setString(1,e.getNom());
        ps.setInt(2,e.getScore());
        ps.setString(3,e.getBlason());
        ps.setInt(4, e.getIdProf());
        ps.setInt(5, e.getProfesseur().getIdProf());
        ps.executeUpdate();
        ps.close();
        conn.close();
	}
	
	public void updateScore(Integer id,Integer score) throws ClassNotFoundException, SQLException {
		System.out.println("id = "+id+" score "+score);
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "" );		
		
        PreparedStatement ps=conn.prepareStatement("UPDATE maison SET score=? where idMaison=?");
        ps.setInt(1,score);
        ps.setInt(2, id);
        ps.executeUpdate();
        ps.close();
        conn.close();
	}
	
	public void delete(Integer id) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "" );		
		
        PreparedStatement ps=conn.prepareStatement("DELETE FROM maison where idMaison=?");
        ps.setInt(1,id);
        ps.executeUpdate();
        ps.close();
        conn.close();
	}
	
	
	public List<Eleve> ElevesMaisonById(Integer id) throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "" );
		
		String sql = "SELECT * FROM eleve WHERE idMaisonEleve=? order by age, nom, prenom";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		
		List<Eleve> eleves = new ArrayList<Eleve>();
		while(rs.next()) {
			Maison maison = this.findById(1);
			eleves.add(new Eleve(rs.getInt("idEleve"),rs.getString("nom"),rs.getString("prenom"),rs.getInt("age"),maison));	
			//maisons.add(new Maison(rs.getInt("idMaison"),rs.getString("nom"),rs.getInt("score"),rs.getString("blason"),rs.getInt("idProfesseurMaison")));	
		}
		
		rs.close();
		ps.close();
		conn.close();
		return eleves;		
	}


}
