package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProfesseur;
import model.Professeur;


@WebServlet("/infoProf")
public class infoProf extends HttpServlet {
	static String BASEURL = "/WEB-INF";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Professeur e = null;

		DAOProfesseur daoProfesseur = new DAOProfesseur();
		try {
			e = daoProfesseur.findById(Integer.parseInt(request.getParameter("uid")));
		} catch (NumberFormatException | ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		
		request.setAttribute("c", e);
		
		this.getServletContext().getRequestDispatcher(BASEURL + "/infoProf.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
