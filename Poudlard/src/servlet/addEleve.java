package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOEleve;
import dao.DAOMaison;
import model.Eleve;
import model.Maison;

@WebServlet("/addEleve")
public class addEleve extends HttpServlet {
	static String BASEURL = "/WEB-INF";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DAOMaison daoMaison = new DAOMaison();
		List<Maison> maisons = null;
		
		try {
			maisons = daoMaison.findAll();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		request.setAttribute("maisons", maisons);
		this.getServletContext().getRequestDispatcher(BASEURL + "/addEleve.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		DAOMaison daoMaison = new DAOMaison();
		Maison maison = null;
		try {
		
			maison = daoMaison.findById(Integer.parseInt(request.getParameter("choixMaison")));
		} catch (NumberFormatException | ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		
		
		Eleve e = new Eleve(
				0,
				(String) request.getParameter("nom"), 
				(String) request.getParameter("prenom"), 
				Integer.parseInt(request.getParameter("age")),
				maison
			);
		
		DAOEleve daoEnfant = new DAOEleve();
		try {
			daoEnfant.create(e);
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		
		
		List<Eleve> c = new ArrayList<Eleve>();
		
		DAOEleve d = new DAOEleve();
		try {
			c = d.findAll();
			System.out.println(c);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		request.setAttribute("eleves", c);
		
		this.getServletContext().getRequestDispatcher(BASEURL + "/listeEleve.jsp").forward(request, response);
	}

}
