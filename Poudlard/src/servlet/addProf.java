package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProfesseur;
import model.Professeur;


@WebServlet("/addProf")
public class addProf extends HttpServlet {
	static String BASEURL = "/WEB-INF";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher(BASEURL + "/addProf.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Professeur p = new Professeur(
				0,
				(String) request.getParameter("nom"), 
				(String) request.getParameter("matiere")
			);
		
		DAOProfesseur daoProf = new DAOProfesseur();
		try {
			daoProf.create(p);
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		
		
		List<Professeur> c = new ArrayList<Professeur>();
		try {
			c = daoProf.findAll();
			System.out.println(c);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		request.setAttribute("professeurs", c);
		
		this.getServletContext().getRequestDispatcher(BASEURL + "/listeProf.jsp").forward(request, response);
	}

}
