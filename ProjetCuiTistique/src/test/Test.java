package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import metier.bar.Bar;
import metier.bar.Boisson;
import metier.bar.TypeBar;
import metier.bar.TypeBoisson;
import metier.compte.Compte;
import metier.compte.Consommateur;
import metier.compte.Gerant;
import metier.ville.Ville;


public class Test {
	
	//Chemin du fichier pour sauvegarder/charger une ville
	static String chemin = "fichier\\ville4.txt";
	
	//Les Scanner
	static Scanner scString = new Scanner(System.in);
	static Scanner scInt = new Scanner(System.in);
	static Scanner scDouble = new  Scanner(System.in);
	
	//La ville
	static Ville ville = Ville.chargerVille();
	
	//Utilisateur connect�
	static Compte user = null;
	//Son index
	static int indexUser;
	
	//On sauvegarde une ville
	public static void sauvegarderVille(Ville ville) {
		try {
			File f = new File(chemin);
			FileOutputStream fos = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			//Sauvegarder la ville dans un fichier
			oos.writeObject(ville);			
						
			oos.close();
			fos.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//On charge une ville
	public static Ville chargerVille() {
		//Ville ville=null;
		try {
			File f = new File(chemin);
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			//On r�cup�re la ville dans le fichier
			ville = (Ville)ois.readObject();
			
			ois.close();
			fis.close();
			
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		return ville;
	}
	
	
	
	//test si le login est d�j� pr�sent
	public static boolean testLogin(String login) {
		for(Compte c : ville.getComptes()) {
			if(c.getLogin().equals(login))
				return true;
		}
		return false;
	}
	
	
	//Cr�er un compte
	public static void creerCompte() {
		System.out.println("\n_____________________________________________________________________\n");
		System.out.println("Quel type de compte voulez-vous cr�er ?");
		System.out.println("1 - Gerant");
		System.out.println("2 - Consommateur");
		System.out.println("Autre - Retour");
		
		String choix = scString.nextLine();
		
		switch(choix) {
			case "1":
				creerGerant();
				break;
			case "2":
				creerConsommateur();
				break;
		default:
			lancer();
		}		
	}
	
	//Cr�er un consommateur
	public static void creerConsommateur() {
		System.out.println("\n_____________________________________________________________________\n");
		System.out.println("Bienvenue");
		System.out.println("Entrez votre pr�nom : ");
		String prenom = scString.nextLine();
		System.out.println("Entrez votre nom : ");
		String nom = scString.nextLine();
		
		System.out.println("Entrez votre login : ");
		String login = scString.nextLine();
		while(testLogin(login)) {
			System.out.println("Le login existe d�j� !");
			System.out.println("Entrez votre login : ");
			login = scString.nextLine();
		}
		
		System.out.println("Entrez votre mot de passe");
		String password = scString.nextLine();
		
		user = new Consommateur(prenom, nom, login, password);
		ville.ajouterCompte(user);
				//sauvegarderVille(ville);
		Ville.sauvegarderVille(ville);
		choixAction();
	}
	
	//Liste les types de bar
	public static void listeTypeBar() {
		int i = 1;
		for(TypeBar tb : TypeBar.values()) {
			System.out.println(i+" - " + tb );
			i++;
		}
	}
	
	//Liste les types de boisson
	public static void listeTypeBoisson() {
		int i = 1;
		for(TypeBoisson tb : TypeBoisson.values()) {
			System.out.println(i+" - " + tb );
			i++;
		}
	}
	
	//lister tous les bars
	public static int listeBar() {
		System.out.println("\n");
		int i = 1;
		int nbBar = 0;
		for(Compte c : ville.getComptes()) {
			if(c instanceof Gerant) {
				System.out.println(i+" - " + ((Gerant) c).getBar());
				nbBar++;
			}
			i++;
		}
		if(nbBar == 0) {
			System.out.println("Il n'y a pas de bar");
			gestionConsommateur();
		}
		
		return nbBar;
	}
	
	//Cr�er un g�rant
	public static void creerGerant() {
		System.out.println("\n_____________________________________________________________________\n");
		System.out.println("Bienvenue");
		System.out.println("Entrez votre pr�nom : ");
		String prenom = scString.nextLine();
		System.out.println("Entrez votre nom : ");
		String nom = scString.nextLine();
		
		System.out.println("Entrez votre login : ");
		String login = scString.nextLine();
		while(testLogin(login)) {
			System.out.println("Le login existe d�j� !");
			System.out.println("Entrez votre login : ");
			login = scString.nextLine();
		}
		
		System.out.println("Entrez votre mot de passe");
		String password = scString.nextLine();
		
		System.out.println("\n");
		
		System.out.println("Il faut maintenant cr�er votre bar :");
		System.out.println("Quel est son nom :");
		String nomBar = scString.nextLine();
		System.out.println("Quel est son adresse :");
		String adresse = scString.nextLine();
		System.out.println("Quel est son type :");
		listeTypeBar();
		int choix = scInt.nextInt();
		while(choix < 1 || choix > TypeBar.values().length) {
			System.out.println("Votre choix est incorrecte !!");
			System.out.println("Quel est son type :");
			listeTypeBar();
			choix = scInt.nextInt();
		}
				
		user = new Gerant(prenom, nom, login, password, new Bar(nomBar, adresse, TypeBar.values()[choix-1]));
		ville.ajouterCompte(user);
				//sauvegarderVille(ville);
		Ville.sauvegarderVille(ville);
		choixAction();
	}
	
	
	//cr�er une ville
	public static Ville creerVille() {
		//Ville ville=null;
		System.out.println("Quel est le nom de votre ville ?");
		String nom = scString.nextLine();
		ville = Ville.getInstanceVille(nom);
		return ville;
	}
	
	
	//test utilisateur
	public static boolean connexionOk(String login, String password) {
		for(Compte c : ville.getComptes()) {
			if(c.getLogin().equals(login) && c.getPassword().equals(password)) {
				user = c;
				indexUser = ville.getComptes().indexOf(user);
				return true;
			}
		}
		return false;
	}
	
	
	//Se connecter
	public static void seConnecter() {
		System.out.println("\n_____________________________________________________________________\n");
		System.out.println("Se connecter � l'application");
		System.out.println("Entrer votre login :");
		String login = scString.nextLine();
		System.out.println("Entrer votre mot de passe");
		String password = scString.nextLine();
		
		if(connexionOk(login, password)) {
			System.out.println("\n_____________________________________________________________________\n");
			System.out.println("Vous �tes bien connect�");
			System.out.println("Bienvenu "+user.toString());
			if(user instanceof Consommateur) {
				gestionConsommateur();
			}else if(user instanceof Gerant) {
				gestionGerant();
			}else {
				System.out.println("Un probl�me est survenu");
			}
		}else {
			System.out.println("Les identifiants sont incorrectes");
			choixAction();
		}
	}
	
	
	//Gestion d'un consommateur
	
	//Bar existe
	public static boolean barExiste(int id) {
		ville = Ville.chargerVille();
		int index = 1;
		for(Compte c : ville.getComptes()) {
			if(c instanceof Gerant) {
				if(index == id)
					return true;
			}
			index++;
		}
		return false;
	}
	
	public static void listeBoissonChoixConsommateur(int choix) {
		ville = Ville.chargerVille();
		int i = 1;
		for(Boisson b : ((Gerant)(ville.getComptes().get(choix))).getBar().getBoissons()) {
			System.out.println(i+" "+b);
			i++;
		}
	}
	
	public static void noterBoissonBar(int numBar,int numBoisson) {
		System.out.println("\n");
		System.out.println("Donner une note entre 0 et 5");
		double note = scDouble.nextDouble();
		while(note<0 || note > 5) {
			System.out.println("\n");
			System.out.println("La note n'est pas valide");
			System.out.println("Donner une note entre 0 et 5");
			note = scDouble.nextDouble();
		}
		
		
		((Consommateur)user).noterBoisson(numBoisson, numBar,note);
		Ville.sauvegarderVille(ville);
	}
	
	
	
	public static void choixActionBoissonConsommateur(int choix) {
		if(((Gerant)(ville.getComptes().get(choix))).getBar().getBoissons().size()<1) {
			System.out.println("Il n'y a pas de boisson");
			gestionConsommateur();
		}else {
			listeBoissonChoixConsommateur(choix);
			System.out.println("\n");
			System.out.println("Quel boisson voulez-vous noter ou -1 pour revenir � l'accueil ?");
			int choixb = scInt.nextInt();
			while((choixb < 0 || choixb > ((Gerant)(ville.getComptes().get(choix))).getBar().getBoissons().size()) && choixb != -1) {
				System.out.println("\n");
				System.out.println("Le num�ro n'est pas bon");
				listeBoissonChoixConsommateur(choix);
				System.out.println("Quel boisson voulez-vous noter ou -1 pour revenir � l'accueil ?");
				choixb = scInt.nextInt();
			}
			if(choixb == -1) {
				gestionConsommateur();
			}else {
				noterBoissonBar(choix, choixb-1);
				Ville.sauvegarderVille(ville);
				gestionConsommateur();
			}
		}
	}
	
	//Les actions lorsque le bar est choisi
	public static void choixActionBarConsommateur(int choix) {
		System.out.println("\n_____________________________________________________________________\n");
		System.out.println("Que voulez-vous faire avec le bar ?");
		System.out.println("1 - Mettre le bar en favorie");
		System.out.println("2 - Noter le bar");
		System.out.println("3 - Lister ses boissons");
		System.out.println("Autre - Retour � l'acueil");
		String choixAction = scString.nextLine();
		
		switch(choixAction) {
			case "1":
				((Consommateur)user).mettreBarFavori(((Gerant)(ville.getComptes().get(choix))).getBar());
				ville.getComptes().set(indexUser, (Consommateur)user);
				Ville.sauvegarderVille(ville);
				gestionConsommateur();
				break;
			case "2":
				System.out.println("\n");
				System.out.println("Donner une note entre 0 et 5");
				double note = scDouble.nextDouble();
				while(note < 0 || note > 5) {
					System.out.println("\n");
					System.out.println("La note est incorrecte");
					System.out.println("Donner une note entre 0 et 5");
					note = scDouble.nextDouble();
				}
				((Consommateur)user).noterBar(choix, note);
				Ville.sauvegarderVille(ville);
				gestionConsommateur();
				break;
			case "3":
				choixActionBoissonConsommateur(choix);
				break;
			default:
				gestionConsommateur();
		}
	}
	
	//Choix du bar
	public static void choixDuBar() {
		System.out.println("\n");
		System.out.println("Selectionner un bar ou -1 pour revenir � l'accueil : ");
		int choixBar = scInt.nextInt();

		while(!barExiste(choixBar)) {
			if(choixBar == -1) {
				break;
			}else {
				System.out.println("\n");
				System.out.println("Le bar n'existe pas");
				listeBar();
				System.out.println("Selectionner un bar ou -1 pour revenier � l'accueil : ");
				choixBar = scInt.nextInt();
			}
		}
		if(choixBar == -1) {
			gestionConsommateur();
		}else {
			choixActionBarConsommateur(choixBar-1);
		}
		
	}
	
	//Actions possible du consommateur
	public static void choixActionConsommateur() {
		System.out.println("\n_____________________________________________________________________\n");
		System.out.println("Que voulez-vous faire");
		System.out.println("1 - Lister les bars par type de bar");
		System.out.println("2 - Lister les bars par type de boisson");
		System.out.println("3 - Lister les bars par nom de boisson");
		System.out.println("4 - Lister tous les bars");
		System.out.println("5 - Lister ses bars favories");
		System.out.println("6 - Avoir la meilleur boisson (qualit�/prix)");
		System.out.println("7 - Avoir la meilleur boisson (degr� d'alcool/Prix)");
		System.out.println("8 - Avoir la meilleure boisson (note)");
		System.out.println("Autre - Se d�connecter");
		
		String choix = scString.nextLine();
		
		Consommateur c = (Consommateur) user;
		boolean quitter = false;
		
		switch(choix){
			case "1":
				listeTypeBar();
				System.out.println("Entrer le numero du type de bar rechercher :");
				int choixTypeBar = scInt.nextInt();
				while(choixTypeBar < 1 || choixTypeBar > TypeBar.values().length) {
					System.out.println("\n");
					listeTypeBar();
					System.out.println("Le num�ro n'existe pas");
					System.out.println("Entrer le num�ro du type de bar rechercher :");
					choixTypeBar = scInt.nextInt();
				}
				c.listerBarParTypeBar(TypeBar.values()[choixTypeBar-1]);
				gestionConsommateur();
				break;
			case "2":
				listeTypeBoisson();
				System.out.println("\n");
				System.out.println("Entrer le numero du type de bar rechercher :");
				int choixTypeBoisson = scInt.nextInt();
				while(choixTypeBoisson < 1 || choixTypeBoisson > TypeBoisson.values().length) {
					System.out.println("\n");
					System.out.println("Le num�ro n'existe pas");
					System.out.println("Entrer le num�ro du type de boisson rechercher :");
					choixTypeBar = scInt.nextInt();
				}
				c.listerBarParTypeBoisson(TypeBoisson.values()[choixTypeBoisson-1]);
				gestionConsommateur();
				break;
			case "3":
				System.out.println("\n");
				System.out.println("Entrer le nom d'une boisson");
				String nom = scString.nextLine();
				c.listerBarParBoisson(nom);
				gestionConsommateur();
				break;
			case "4":
				int nb = listeBar();
				if(nb>0)
					choixDuBar();
				break;
			case "5":
				System.out.println("\n");
				c.listerBarsFavoris();
				gestionConsommateur();
				break;
			case "6":
				System.out.println("\n");
				c.afficherBestBoissonQtePrix();
				gestionConsommateur();
				break;
			case "7":
				System.out.println("\n");
				c.afficherBestBoissonDgrPrix();
				gestionConsommateur();
				break;
			case "8":
				System.out.println("\n");
				c.afficherBestBoissonNote();
				gestionConsommateur();
				break;
			default:
				System.out.println("A bient�t");
				Ville.sauvegarderVille(ville);
				lancer();
				quitter = true;
		}
		
	}
	
	public static void gestionConsommateur() {
		choixActionConsommateur();
	}
	
	//Gestion d'un Gerant
	
	public static void ajouterBoisson() {
		System.out.println("\n_____________________________________________________________________\n");
		System.out.println("Entrez le nom de votre boisson");
		String nom = scString.nextLine();
		
		System.out.println("Entrez un type de boisson");
		listeTypeBoisson();
		int nbBoisson = scInt.nextInt();
		while(nbBoisson<0 || nbBoisson > TypeBoisson.values().length) {
			System.out.println("Le num�ro demand� n'est pas pr�sent");
			System.out.println("Entrez un type de boisson");
			listeTypeBoisson();
			nbBoisson = scInt.nextInt();
		}
		TypeBoisson type = TypeBoisson.values()[nbBoisson-1];
		
		System.out.println("Entrez le degr� d'alcool");
		double degre = scDouble.nextDouble();
		while(degre < 0 || degre > 100) {
			System.out.println("Le degr� d'alcool est inf�rieur � 0 ou sup�rieur � 100");
			System.out.println("Entrez le degr� d'alcool");
			degre = scDouble.nextDouble();
		}
		
		System.out.println("Entrez la quantit�");
		int quantite = scInt.nextInt();
		while(quantite < 0) {
			System.out.println("La quantit� de la boisson est inf�rieur � 0");
			System.out.println("Entrez la quantit�");
			quantite = scInt.nextInt();
		}
		
		System.out.println("Entrez le prix");
		double prix = scDouble.nextDouble();
		while(prix < 0) {
			System.out.println("Le prix de la boisson est inf�rieur � 0");
			System.out.println("Entrez le prix");
			prix = scDouble.nextDouble();
		}
		Boisson b = new Boisson(nom, type, degre, quantite, prix);
		((Gerant)user).ajouterBoisson(b);
				//sauvegarderVille(ville);
		Ville.sauvegarderVille(ville);
		gestionGerant();
	}
	
	public static Boisson modifierBoisson(int choix) {
		System.out.println("\n_____________________________________________________________________\n");
		Boisson b=((Gerant)user).getBar().getBoissons().get(choix);
		System.out.println("Entrer le nom de la boisson");
		String nom = scString.nextLine();
				
		System.out.println("Entrez le degr� d'alcool");
		double degre = scDouble.nextDouble();
		while(degre < 0 || degre > 100) {
			System.out.println("Le degr� d'alcool est inf�rieur � 0 ou sup�rieur � 100");
			System.out.println("Entrez le degr� d'alcool");
			degre = scDouble.nextDouble();
		}
		
		System.out.println("Entrez la quantit�");
		int quantite = scInt.nextInt();
		while(quantite < 0) {
			System.out.println("La quantit� de la boisson est inf�rieur � 0");
			System.out.println("Entrez la quantit�");
			quantite = scInt.nextInt();
		}
		
		System.out.println("Entrez le prix");
		double prix = scDouble.nextDouble();
		while(prix < 0) {
			System.out.println("Le prix de la boisson est inf�rieur � 0");
			System.out.println("Entrez le prix");
			prix = scDouble.nextDouble();
		}
		b.setDegre(degre);
		b.setNom(nom);
		b.setPrix(prix);
		b.setQuantite(quantite);
		return b;
	}
	
	public static void choixActionBoisson(int choix) {
		System.out.println("\n_____________________________________________________________________\n");
		System.out.println("Que voulez-vous faire avec votre boisson");
		System.out.println("1 - Voir la note");
		System.out.println("2 - Modifier la boisson");
		System.out.println("3 - Supprimer la boisson");
		System.out.println("Autre - Retour � l'accueil");
		
		String choixB = scString.nextLine();
		
		switch(choixB) {
			case "1":
				System.out.println("La note de la boisson est "+((Gerant)user).getBar().getBoissons().get(choix).getNote());
				gestionGerant();
				break;
			case "2":
				Boisson b = modifierBoisson(choix);
				((Gerant)user).modifierBoisson(choix,b);
				
				Ville.sauvegarderVille(ville);
				gestionGerant();
				break;
			case "3":
				((Gerant)user).supprimerBoisson(choix);
				gestionGerant();
				break;
			default:
					gestionGerant();
		}
		
	}
	
	public static void choixBoissonGerant() {
		System.out.println("\n_____________________________________________________________________\n");
		System.out.println("Choisir une boisson � g�rer ou -1 pour revenir � l'accueil :");
		int choix = scInt.nextInt();

		while(choix < 1 || choix > ((Gerant)user).getBar().getBoissons().size()) {
			if(choix == -1) {
				break;
			}else {
				System.out.println("Le num�ro est incorrecte");
				listerBoisson();
				System.out.println("Choisir une boisson � g�rer ou -1 pour revenir � l'accueil :");
				choix = scInt.nextInt();
			}
		}
		if(choix == -1) {
			gestionGerant();
		}else {
			choixActionBoisson(choix-1);
		}
	}
	
	public static int listerBoisson() {
		Gerant g = (Gerant) user;
		int i=1;
		for(Boisson b : g.getBar().getBoissons()) {
			System.out.println(i+" " + b);
			i++;
		}
		return i;
	}
	
	public static void listerBoissonChoix() {
		int i = listerBoisson();
		if(i<=1) {
			System.out.println("Il n'y a pas de boisson");
			gestionGerant();
		}else {
			System.out.println("nombre de boissons : "+(i-1));
			choixBoissonGerant();
		}
	}
	
	public static void choixActionGerant() {
		System.out.println("\n_____________________________________________________________________\n");
		System.out.println("Que voulez-vous faire ?");
		System.out.println("1 - Ajouter une boisson");
		System.out.println("2 - Lister les boissons");
		System.out.println("3 - Voir la note du bar");
		System.out.println("Autre - Se d�connecter");
		
		String choix = scString.nextLine();
		
		Gerant g = (Gerant)user;
		
		switch(choix) {
			case "1":
				ajouterBoisson();
				break;
			case "2":
				listerBoissonChoix();
				break;
			case "3":
				System.out.println("La note de votre bar est " + g.voirNoteBar());
				choixActionGerant();
				break;
			default:
				System.out.println("A bient�t");
				Ville.chargerVille();
				lancer();
		
		}
	}
	
	public static void gestionGerant() {
		choixActionGerant();
	}
	
	
	//Choix pour cr�er un compte ou se connecter
	public static void choixAction() {
		System.out.println("\n_____________________________________________________________________\n");
		System.out.println("Que voulez-vous faire ?");
		System.out.println("1 - Cr�er un compte");
		System.out.println("2 - Se connecter");
		System.out.println("Autre - Quitter l'application");
		
		String choix = scString.nextLine();
		
		switch(choix) {
			case "1":
				creerCompte();
				break;
			case "2":
				seConnecter();
				break;
		default:
			System.out.println("A bient�t");
			Ville.sauvegarderVille(ville);
		}
	}
	
	//lancer application
	public static void lancer() {
		//Ville ville = null;
		if(ville==null) {
			System.out.println("Bienvenue il faut cr�er votre ville");
			ville = creerVille();
			System.out.println("Bienvenue sur : "+ville.getNom());
			sauvegarderVille(ville);
			Ville.sauvegarderVille(ville);
			creerCompte();
		}else {
			System.out.println("Bienvenue sur "+ville.getNom());
			choixAction();
		}		
	}

	public static void main(String[] args) {
		
		lancer();
		
		/*Ville ville;
		ville = Ville.getInstanceVille("Tours");
		
		//On cr�e des objets
		Bar bar1 = new Bar("Le Pastis", "Rue de la soif", TypeBar.Argentain);
		Gerant g1 =  new Gerant("Jordan", "Abid", "ajc", "ajc", bar1);
		Consommateur cons1 = new Consommateur("Gaitan", "Secret", "Biere", "Belge");
		
		//Ajout des comptes dans la ville
		ville.ajouterCompte(g1);
		ville.ajouterCompte(cons1);
		
		//On sauvegarde la classe
		sauvegarderVille(ville);
		
		ville = null;
		
		//On charge la ville
		ville = chargerVille();
		
		
		Gerant g2 = (Gerant) ville.getComptes().get(0);
		g2.ajouterBoisson(new Boisson("Licorne", TypeBoisson.Biere, 7.5, 50, 5.6));
		g2.ajouterBoisson(new Boisson("Mont-Blanc",TypeBoisson.Vodka,35,15,7));
		
		Bar bar2 = new Bar("Rhum", "rty", TypeBar.Argentain);
		Gerant g3 = new Gerant("Claude", "Lo", "r", "r", bar2);
		g3.ajouterBoisson(new Boisson("Azerty", TypeBoisson.VinBlanc, 0, 0, 0));
		
		ville.ajouterCompte(g3);
		
		for(Compte c : ville.getComptes()) {
			System.out.println(c);
		}
		
		cons1.noterBar(((Gerant) (ville.getComptes().get(0))).getBar(), 5);
		cons1.noterBar(((Gerant) (ville.getComptes().get(0))).getBar(), 3);
		System.out.println(g2);
		
		sauvegarderVille(ville);*/


	}

}
