package metier.ville;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import metier.compte.Compte;


public class Ville implements Serializable {

	private static final long serialVersionUID = 1L;
	private static Ville ville;
    private String nom;
    private List<Compte> comptes;
	//Chemin du fichier pour sauvegarder/charger une ville
	private static String chemin = "fichier\\ville4.txt";

	
    private Ville(String nom) {
    	this.nom = nom;
    	comptes = new  ArrayList<Compte>();
    }
    
    
	public static Ville getInstanceVille(String nom) {
		if(ville == null) {
			ville = new Ville(nom);
		}
		return ville;
	}
	
	public static Ville getVille() {
		return ville;
	}
		
	//retourn le nom de la ville
	public String getNom() {
		return nom;
	}

	//retourne la liste des comptes
	public List<Compte> getComptes() {
		return comptes;
	}
	
	//ajoute un compte
	public void ajouterCompte(Compte compte) {
		comptes.add(compte);
	}
	
	//retire un compte
	public void retirerCompte(Compte compte) {
		
	}
	
	//modifie un compte
	public void modifierCompte(Compte compte) {
		
	}

	//sauvegarder la ville
	public static void sauvegarderVille(Ville ville) {
		try {
			File f = new File(chemin);
			FileOutputStream fos = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			//Sauvegarder la ville dans un fichier
			oos.writeObject(ville);			
						
			oos.close();
			fos.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//charger la ville
	public static Ville chargerVille() {
		//Ville ville=null;
		try {
			File f = new File(chemin);
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			//On r�cup�re la ville dans le fichier
			ville = (Ville)ois.readObject();
			
			ois.close();
			fis.close();
			
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		return ville;
	}
	
	
	
	//test si le login est d�j� pr�sent
	public static boolean testLogin(String login) {
		for(Compte c : ville.getComptes()) {
			if(c.getLogin().equals(login))
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Ville [nom=" + nom + ", comptes=" + comptes + "]";
	}
}