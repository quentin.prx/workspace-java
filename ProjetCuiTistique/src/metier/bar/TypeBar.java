package metier.bar;

/**
 * 
 */
public enum TypeBar {
    Irlandais("Irlandais"),
    Argentan("Argentin"),
    Breton("Breton"),
    Americain("Américain"),
    Ecossais("Ecossais"),
    Canadien("Canadien");
	
	String type;
	
	private TypeBar(String type) {
		this.type = type;
	}
	
	public String getNom() {
		return type;
	}
}