package metier.bar;

import java.io.Serializable;


public class Boisson implements Serializable {

	private static final long serialVersionUID = 1L;
	private String nom;
    private double note;
    private int nbreNote;
    private TypeBoisson type;
    private double degre;
    private int quantite;
    private double prix;
    
	public Boisson(String nom, TypeBoisson type, double degre, int quantite, double prix) {
		this.nom = nom;
		this.type = type;
		this.degre = degre;
		this.quantite = quantite;
		this.prix = prix;
		this.note = 0.0;
		this.nbreNote = 0;
	}

	//retourne le nom de la boisson
	public String getNom() {
		return nom;
	}

	//modifie le nom de la boisson
	public void setNom(String nom) {
		this.nom = nom;
	}

	//retourne le degr� d'alcool
	public double getDegre() {
		return degre;
	}

	//modifie le degr� d'alcool
	public void setDegre(double degre) {
		this.degre = degre;
	}

	//retourne la quantit� en cl
	public int getQuantite() {
		return quantite;
	}
	
	//modifie la quantit� en cl
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	//retourne le prix
	public double getPrix() {
		return prix;
	}

	
	//modifie le prix
	public void setPrix(double prix) {
		this.prix = prix;
	}

	//retourne la note de la boisson
	public double getNote() {
		return note;
	}
	
	//modifie la note de la boisson en fonction de l'ancienne note et du nombre de notes
	public void setNote(double note) {
		this.note = (this.note*nbreNote + note)/(nbreNote+1);
		setNbreNote();
	}

	//retourne le nombre de notes
	public int getNbreNote() {
		return nbreNote;
	}

	//incr�mente le nombre de notes
	private void setNbreNote() {
		nbreNote++;
	}
	
	//retourne le type de boisson
	public TypeBoisson getType() {
		return type;
	}

	//retourne les informaions sur la boisson
	public String toString() {
		String ch = "";
		ch = quantite + "cl de " + type.getNom() + " : " + nom + " � " + degre + "% co�te " + prix + "�";  
		return ch;
	}

}