package metier.bar;

/**
 * 
 */
public enum TypeBoisson {
    Biere("Bi�re"),
    Vodka("Vodka"),
    VinBlanc("Vin blanc"),
    Whiskey("Whiskey"),
	Whisky("Whisky"),
	VinRouge("Vin rouge"),
	Absinthe("Absinthe"),
	Tequila("Tequila"),
	Gin("Gin"),
	EauDeVie("Eau de vie"),
	Liqueur("Liqueur");
	
	String type;
	
	private TypeBoisson(String type) {
		this.type = type;
	}
	
	public String getNom() {
		return type;
	}
	
}