package metier.bar;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Bar implements Serializable{

	private static final long serialVersionUID = 1L;
	private String nom;
    private double note;
    private int nbreNote;
    private String adresse;
    private TypeBar type;
    private List<Boisson> boissons;
    
    
	public Bar(String nom, String adresse, TypeBar type) {
		this.nom = nom;
		this.adresse = adresse;
		this.type = type;
		this.note = 0;
		this.nbreNote = 0;
		boissons = new ArrayList<Boisson>();
	}
	
	//retourn le nom du bar
	public String getNom() {
		return nom;
	}
	
	//modifie le nom du bar
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	//retourne le note du bar
	public double getNote() {
		return note;
	}
	
	//modifie la note du bar en fonction de l'ancienne note et du nombre de notes
	public void setNote(double note) {
		this.note = (this.note * nbreNote + note)/(nbreNote + 1);
		setNbreNote();
	}
	
	//retourne le nombre de note
	public int getNbreNote() {
		return nbreNote;
	}
	
	//incr�mente le nombre de notes
	private void setNbreNote() {
		nbreNote++;
	}
	
	//retourne l'adresse du bar
	public String getAdresse() {
		return adresse;
	}
	
	//modifie l'adresse du bar
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	//retourne le type de bar
	public TypeBar getType() {
		return type;
	}
	
	//modifie de type de bar
	public void setType(TypeBar type) {
		this.type = type;
	}
	
	//liste les boissons du bar
	public List<Boisson> getBoissons() {
		return boissons;
	}

	//ajoute une boisson au bar
    public void ajouterBoisson(Boisson boisson) {
       	boissons.add(boisson);
    	
    }

    //modifie une boisson du bar
    public void modifierBoisson(int index,Boisson boisson) {
    	
    }

    //supprime une boisson du bar
    public void supprimerBoisson(int boisson) {
    	boissons.remove(boisson);
    }

    //retourn les informations d'un bar
	public String toString() {
		
		String ch = "";
		ch = nom + " est un bar " + type.getNom() + " situ� � " + adresse;
		
		return ch;
	}
  


}