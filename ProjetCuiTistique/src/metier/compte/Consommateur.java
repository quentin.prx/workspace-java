package metier.compte;

import java.util.ArrayList;
import java.util.List;

import metier.bar.Bar;
import metier.bar.Boisson;
import metier.bar.TypeBar;
import metier.bar.TypeBoisson;
import metier.ville.Ville;


public class Consommateur extends Compte {

	private static final long serialVersionUID = 1L;
	private List<Bar> barsFavoris;

    public Consommateur(String prenom, String nom, String login, String password) {
		super(prenom, nom, login, password);
		barsFavoris = new ArrayList<Bar>();
	}

    //Liste tous les bars de la ville qui ont une boisson avec le nom en param�tre
    public void listerBarParBoisson(String nom) {
    	Ville ville = Ville.getVille();
    	for(Compte c : ville.getComptes()) {
    		if(c instanceof Gerant) {
    			for(Boisson b : ((Gerant)c).getBar().getBoissons()) {
    				if(b.getNom().equals(nom)) {
    					System.out.println(b);
    				}
    			}
    		}
    	}
    }

    //Liste tous les bars de la ville qui ont le m�me type de bar que le type de bar en param�tre
    public void listerBarParTypeBar(TypeBar type) {
    	Ville ville = Ville.getVille();
    	for(Compte c : ville.getComptes()) {
    		if(c instanceof Gerant) {
    			if(((Gerant)c).getBar().getType().equals(type)) {
	    			for(Boisson b : ((Gerant)c).getBar().getBoissons()) {
	    					System.out.println(b);
	    			}
    			}
    		}
    	}
    }
    
    //Liste tous les bars de la ville qui ont le m�me type de boisson que le type de boisson en param�tre
    public void listerBarParTypeBoisson(TypeBoisson type) {
    	Ville ville = Ville.getVille();
    	for(Compte c : ville.getComptes()) {
    		if(c instanceof Gerant) {
    			for(Boisson b : ((Gerant)c).getBar().getBoissons()) {
    				if(b.getType().equals(type)) {
    					System.out.println(b);
    				}
    			}
    		}
    	}
    }

    //Liste tous les bars favoris du consommateur
    public void listerBarsFavoris() {
    	for (Bar bf : barsFavoris) {
    		System.out.println(bf);
    	}
    	if(barsFavoris.size()<=0) {
    		System.out.println("Il n'y a pas de bars dans vos favoris");
    	}
    }

    //retourne la boisson de la ville ayant le meilleur rapport Quantit�/Prix
    public void afficherBestBoissonQtePrix() {
    	Ville ville = Ville.getVille();
    	Boisson best = null;
    	double max = -1.0;
    	for(Compte c : ville.getComptes()) {
    		if(c instanceof Gerant) {
    			for(Boisson b : ((Gerant)c).getBar().getBoissons()) {
    				double tmp = b.getQuantite()/b.getPrix();
    				if(tmp>max) {
    					max = tmp;
    					best = b;
    				}
    			}
    		}
    	}
    	System.out.println(best);
    }

    //affiche la boisson de la ville ayant le meilleur rapport Degr� d'alcool/Prix
    public void afficherBestBoissonDgrPrix() {
    	Ville ville = Ville.getVille();
    	Boisson best = null;
    	double max = -1.0;
    	for(Compte c : ville.getComptes()) {
    		if(c instanceof Gerant) {
    			for(Boisson b : ((Gerant)c).getBar().getBoissons()) {
    				double tmp = b.getDegre()/b.getPrix();
    				if(tmp>max) {
    					max = tmp;
    					best = b;
    				}
    			}
    		}
    	}
    	
    	System.out.println(best);
    }

    //affiche la boisson de la ville qui a la meilleure note
    public void afficherBestBoissonNote() {
    	Ville ville = Ville.getVille();
    	Boisson best = null;
    	double max = -1.0;
    	for(Compte c : ville.getComptes()) {
    		if(c instanceof Gerant) {
    			for(Boisson b : ((Gerant)c).getBar().getBoissons()) {
    				double tmp = b.getNote();
    				if(tmp>max) {
    					max = tmp;
    					best = b;
    				}
    			}
    		}
    	}
    	
    	System.out.println(best);
    }

    //Permet de mettre un bar en favori, s'il n'est pas pr�sent
    public void mettreBarFavori(Bar bar) {
    	boolean test = false;
    	for (Bar bf : barsFavoris) {
    		if (bf.getNom().equals(bar.getNom()) && bf.getType().equals(bar.getType()) && bf.getAdresse().equals(bar.getAdresse())) {
    			test = true;
    		}
    	}

    	if (!test) {
    		barsFavoris.add(bar);
    		
    	}else {
    		System.out.println("Ce bar est deja dans vos favoris");
    	}
    }

    //Permet de noter un bar de la ville
    public void noterBar(int bar,double note) {
    	Ville ville = Ville.getVille();
    	((Gerant)(ville.getComptes().get(bar))).getBar().setNote(note);
    }

    //Permet de retirer un bar de ses favoris, s'il existe
    public void retirerBarFavori(Bar bar) {
    	barsFavoris.remove(bar);
    	
    }

    //Permet de lister les boissons d'un bar de la ville
    public void listerBoissonBar(Bar bar) {
    	Ville ville = Ville.getVille();
    	for(Compte c : ville.getComptes()) {
    		if(c instanceof Gerant) {
    			if(((Gerant)c).getBar().getNom().equals(bar.getNom())) {
    				for(Boisson b : ((Gerant)c).getBar().getBoissons()) {
    					System.out.println(b);
    				}
    			}
    		}
    	}
    }

    //Permet de noter une boisson d'un bar de la ville
    public void noterBoisson(int boisson, int bar, double note) {
    	Ville ville = Ville.getVille();
    	((Gerant)(ville.getComptes().get(bar))).getBar().getBoissons().get(boisson).setNote(note);
    	System.out.println(((Gerant)ville.getComptes().get(bar)));
    }

    //Permet de lister tous les bars li�s � ses boissons favories
    public void listerBoissonBarsFavoris() {
    	int cpt = 0;
    	for(Bar b : barsFavoris) {
    		System.out.println(b);
    		cpt++;
    	}
    	
    	if(cpt<=0) {
    		System.out.println("Il n'y a pas de bar dans vos favoris");
    	}
    }


	//Affiche les informations du consommateur
	public String toString() {
		return super.toString();
	}
    

}