package metier.compte;

import java.io.Serializable;


public abstract class Compte implements Serializable {

	private static final long serialVersionUID = 1L;
	protected int id;
	protected static int cpt;
    protected String prenom;
    protected String nom;
    protected String login;
    protected String password;
    
	public Compte(String prenom, String nom, String login, String password) {
		this.prenom = prenom;
		this.nom = nom;
		this.login = login;
		this.password = password;
		this.id = cpt++;
	}

	//retourne la mot de passe
	public String getPassword() {
		return password;
	}

	//modifie le mot de passe
	public void setPassword(String password) {
		this.password = password;
	}

	//retourne le pr�nom
	public String getPrenom() {
		return prenom;
	}

	//retourne le nom
	public String getNom() {
		return nom;
	}

	//retourne le login
	public String getLogin() {
		return login;
	}

	//retourne l'id du compte
	public int getId() {
		return this.id;
	}
	
	//affiche les informations "primaires" du compte
	public String toString() {		
		return prenom + " " + nom;
	}
	
	
	
	
}