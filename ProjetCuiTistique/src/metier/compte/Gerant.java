package metier.compte;

import metier.bar.Bar;
import metier.bar.Boisson;

public class Gerant extends Compte {

	private static final long serialVersionUID = 1L;
	private Bar bar;

    public Gerant(String prenom, String nom, String login, String password, Bar bar) {
		super(prenom, nom, login, password);
		this.bar = bar;
	}
    

	public Bar getBar() {
		return bar;
	}

	//Ajoute une boisson au bar du g�rant, si elle n'existe pas
    public void ajouterBoisson(Boisson boisson) {
    	bar.ajouterBoisson(boisson);
    }

    //Modifie une boisson contenue dans le bar du g�rant, si elle existe
    public void modifierBoisson(int index,Boisson boisson) {
    	bar.modifierBoisson(index,boisson);
    }

    //Supprime une boison contenue dans le bar du g�rant, si elle existe
    public void supprimerBoisson(int boisson) {
    	bar.supprimerBoisson(boisson);
    }

    //Liste toutes les boissons du bar du g�rant
    public void listerBoisson() {
    	for (Boisson b:bar.getBoissons()) {
    		System.out.println(b);
    	}
    }

    //Permet de voir la note du bar g�rant
    public double voirNoteBar() {
        return bar.getNote();
    }

    //Permet de voir la note d'une boisson dans le bar du g�rant, si elle existe
    public double voirNoteBoisson(Boisson boisson) {
        return boisson.getNote();
    }

    //retourne vrai si la boisson existe dans le bar, faux sinon
    public boolean existeBoisson(Boisson boisson) {
    	for (Boisson b:bar.getBoissons()) {
    		if (b.getNom() == boisson.getNom() && b.getType() == boisson.getType()) {
    			return true;
    		}
    	}
    	return false; 
    	
    }

	//Affiche les informations du g�rant
	public String toString() {
		String ch = "";
		ch = super.toString() + " - Bar : " + bar.getNom();
		return ch;
	}

    
}