package test;

import exception.PoidsNegatifException;
import metier.Chien;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Chien c = null;
		
		try {
			c = new Chien(12);
		} catch (PoidsNegatifException e) {
			System.out.println(e.getMessage());
			try {
				c = new Chien(0);
			} catch (PoidsNegatifException e1) {
				e1.getMessage();
			}
		}
		
		System.out.println(c);
		
		try {
			c.setPoids(-12);
		} catch (PoidsNegatifException e) {
			e.getMessage();
			try {
				c.setPoids(0);
			} catch (PoidsNegatifException e1) {
				e1.getMessage();
			}
		}
		
		System.out.println(c);
	}

}
