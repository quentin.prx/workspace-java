package metier;

import exception.PoidsNegatifException;

public class Chien {
	private int poids;

	public Chien(int poids) throws PoidsNegatifException {
		if(poids<0) {
			throw new PoidsNegatifException("Le poids est inf�rieur � 0 !!");
		}
		this.poids = poids;
	}

	public int getPoids() {
		return poids;
	}

	public void setPoids(int poids) throws PoidsNegatifException {
		if(poids < 0) {
			throw new PoidsNegatifException("Le poids est inf�rieur � 0 !");
		}
		this.poids = poids;
	}

	public String toString() {
		return "Chien [poids=" + poids + "]";
	}
	
	
}
