package metier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ville implements Serializable {
	private static Ville ville = null;
	private String nom;
	private List<String> bars;
	private List<String> comptes;
	
	private Ville(String nom) {
		bars = new ArrayList<>();
		comptes = new ArrayList<>();
	}
	
	public Ville getInstanceVille(String nom) {
		if(ville == null) {
			ville = new Ville(nom);
		}
		return ville;
	}
	
	public void addBar(String bar) {
		bars.add(bar);
	}
	
	

}
