package DAO;

import java.sql.Connection;

import connection.Connexion;


public class DAOFactory extends AbstractDAOFactory{
  protected static final Connection conn = Connexion.getInstance();   

  public DAO getEnfantDAO(){
    return new DAOEnfant(conn);
  }
}