package Test;

import java.sql.SQLException;

import DAO.AbstractDAOFactory;
import DAO.DAOEnfant;
import modele.Enfant;

public class Test {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		AbstractDAOFactory adf = AbstractDAOFactory.getFactory(AbstractDAOFactory.DAO_FACTORY);
		Enfant e = new Enfant("Poussin","Piou",3);
		DAOEnfant daoENfant = (DAOEnfant) adf.getEnfantDAO();
		
		System.out.println("Cr�ation d'un enfant");
		daoENfant.create(e);
		System.out.println("L'enfant a �t� cr��");
		
		System.out.println("On cherche l'enfant par son nom");
		e=daoENfant.findByName("Poussin");
		
		System.out.println("On prend l'ID de l'enfant cr��");
		int idCreation = e.getId();
		
		
		System.out.println("On recherche tous les enfants");
		System.out.println(daoENfant.findAll());
		
		System.out.println("On affiche un enfant par son ID");
		System.out.println(daoENfant.findById(idCreation));
		
		System.out.println("On r�cupp�re un enfant par son ID");
		Enfant e2 = daoENfant.findById(idCreation);
		
		System.out.println("On change le nom de l'enfant r�cup�r�");
		e2.setNom("Archive");
		daoENfant.update(e2);
		
		System.out.println("On r�affiche l'enfant apr�s le changemet");
		System.out.println(daoENfant.findById(idCreation));
		
		
		System.out.println("On supprime un enfant par son ID");
		daoENfant.delete(idCreation);
		
		
		System.out.println("On affiche tous les enfants apr�s la suppression");
		System.out.println(daoENfant.findAll());
		
		
		System.out.println("On se d�connecte");
		daoENfant.seDeconnecterDAO();
	}
}
