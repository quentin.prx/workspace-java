package model;

public class Eleve {

	private int idEleve;
	private String nom;
	private String prenom;
	private int age;
	private Maison maison;
	
	public Eleve(int idEleve, String nom, String prenom, int age, Maison maison) {
		this.idEleve = idEleve;
		this.nom = nom;
		this.prenom = prenom;
		this.age=age;
		this.maison=maison;
	}
	
	public Eleve(String nom, String prenom, int age, Maison maison) {
		this.idEleve = 0;
		this.nom = nom;
		this.prenom = prenom;
		this.age=age;
		this.maison=maison;
	}
	
	public Eleve() {
		
	}

	public int getIdEleve() {
		return idEleve;
	}

	public void setIdEleve(int idEleve) {
		this.idEleve = idEleve;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	
	public Maison getMaison() {
		return maison;
	}
	
	public void setMaison(Maison maison) {
		this.maison=maison;
	}

	@Override
	public String toString() {
		return "Eleve [idEleve=" + idEleve + ", nom=" + nom + ", prenom=" + prenom + ", age=" + age + ", maison="
				+ maison + "]";
	}

	
}
