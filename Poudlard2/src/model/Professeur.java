package model;

public class Professeur {

	private int idProf;
	private String nom;
	private String matiere;
	
	public Professeur(int idProf, String nom, String matiere) {
		this.idProf = idProf;
		this.nom = nom;
		this.matiere = matiere;
	}
	
	public Professeur(String nom, String matiere) {
		this.idProf = 0;
		this.nom = nom;
		this.matiere = matiere;
	}
	
	public Professeur() {
		
	}
	
	public int getIdProf() {
		return idProf;
	}
	public void setIdProf(int idProf) {
		this.idProf = idProf;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getMatiere() {
		return matiere;
	}
	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}

	@Override
	public String toString() {
		return "Professeur [idProf=" + idProf + ", nom=" + nom + ", matiere=" + matiere + "]";
	}
	
	
	
	
}
