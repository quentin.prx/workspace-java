package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOEleve;
import dao.DAOMaison;
import dao.DAOProfesseur;
import model.Eleve;
import model.Maison;
import model.Professeur;


@WebServlet("/editProf")
public class editProf extends HttpServlet {
	static String BASEURL = "/WEB-INF";
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Professeur e = null;

		DAOProfesseur daoProfesseur = new DAOProfesseur();
		try {
			e = daoProfesseur.findById(Integer.parseInt(request.getParameter("uid")));
		} catch (NumberFormatException | ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		
		request.setAttribute("c", e);
		
		this.getServletContext().getRequestDispatcher(BASEURL + "/editProf.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Professeur e = new Professeur(
				Integer.parseInt(request.getParameter("uid")),  
				(String) request.getParameter("nom"), 
				(String) request.getParameter("matiere")	
		);
		
		DAOProfesseur daoProfesseur = new DAOProfesseur();
		try {
			daoProfesseur.update(e);
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		
		request.setAttribute("c", e);
		
		this.getServletContext().getRequestDispatcher(BASEURL + "/editProf.jsp").forward(request, response);
	}

}
