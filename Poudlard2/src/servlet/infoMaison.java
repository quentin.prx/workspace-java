package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOMaison;
import model.Eleve;
import model.Maison;

@WebServlet("/infoMaison")
public class infoMaison extends HttpServlet {
	static String BASEURL = "/WEB-INF";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Maison m = null;

		DAOMaison daoMaison = new DAOMaison();
		List<Eleve> eleves = new ArrayList<Eleve>();
		try {
			m = daoMaison.findById(Integer.parseInt(request.getParameter("uid")));
			eleves = daoMaison.ElevesMaisonById(m.getIdMaison());
		} catch (NumberFormatException | ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		
		request.setAttribute("c", m);
		request.setAttribute("eleves", eleves);
		
		this.getServletContext().getRequestDispatcher(BASEURL + "/infoMaison.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
