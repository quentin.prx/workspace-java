package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AbstractDAOFactory;
import dao.DAOEleve;
import model.Eleve;

@WebServlet("/deleteEleve")
public class deleteEleve extends HttpServlet {
	static String BASEURL = "/WEB-INF";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractDAOFactory adf = AbstractDAOFactory.getFactory(AbstractDAOFactory.DAO_FACTORY);
		DAOEleve daoEleve = (DAOEleve) adf.getEleveDAO();
		try {
			daoEleve.delete(Integer.parseInt(request.getParameter("uid")));
		} catch (NumberFormatException | ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		List<Eleve> c = new ArrayList<Eleve>();
		
		DAOEleve d = (DAOEleve) adf.getEleveDAO();
		try {
			c = d.findAll();
			System.out.println(c);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		request.setAttribute("eleves", c);
		
		this.getServletContext().getRequestDispatcher(BASEURL + "/listeEleve.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
