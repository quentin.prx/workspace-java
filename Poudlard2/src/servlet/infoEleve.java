package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOEleve;
import model.Eleve;


@WebServlet("/infoEleve")
public class infoEleve extends HttpServlet {
	static String BASEURL = "/WEB-INF";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Eleve e = null;

		DAOEleve daoEleve = new DAOEleve();
		try {
			e = daoEleve.findById(Integer.parseInt(request.getParameter("uid")));
		} catch (NumberFormatException | ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		
		request.setAttribute("c", e);
		
		this.getServletContext().getRequestDispatcher(BASEURL + "/infoEleve.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
