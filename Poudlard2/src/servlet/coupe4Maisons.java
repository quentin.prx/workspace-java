package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AbstractDAOFactory;
import dao.DAOMaison;
import model.Maison;


@WebServlet("/coupe4Maisons")
public class coupe4Maisons extends HttpServlet {
	static String BASEURL = "/WEB-INF";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractDAOFactory adf = AbstractDAOFactory.getFactory(AbstractDAOFactory.DAO_FACTORY);
		List<Maison> m = new ArrayList<Maison>();

		DAOMaison dm = (DAOMaison) adf.getMaisonDAO();
		try {
			m = dm.findAllOrderByScore();
			System.out.println(m);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		request.setAttribute("maisons", m);


		this.getServletContext().getRequestDispatcher(BASEURL + "/coupe4Maisons.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
