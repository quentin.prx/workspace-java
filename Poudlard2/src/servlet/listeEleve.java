package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOEleve;
import model.Eleve;


@WebServlet("/listeEleve")
public class listeEleve extends HttpServlet {
	
	static String BASEURL = "/WEB-INF";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Eleve> c = new ArrayList<Eleve>();
		
		DAOEleve d = new DAOEleve();
		try {
			c = d.findAll();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		String json = "";
		try {
			json = d.findAllJson();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.setAttribute("json", json);
		request.setAttribute("eleves", c);
		
		this.getServletContext().getRequestDispatcher(BASEURL + "/listeEleve.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
