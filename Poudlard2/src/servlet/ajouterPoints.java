package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AbstractDAOFactory;
import dao.DAOEleve;
import dao.DAOMaison;
import model.Eleve;
import model.Maison;


@WebServlet("/ajouterPoints")
public class ajouterPoints extends HttpServlet {
	static String BASEURL = "/WEB-INF";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Maison e = null;
		AbstractDAOFactory adf = AbstractDAOFactory.getFactory(AbstractDAOFactory.DAO_FACTORY);
		DAOMaison daoMaison = (DAOMaison) adf.getMaisonDAO();
		try {
			e = daoMaison.findById(Integer.parseInt(request.getParameter("uid")));
		} catch (NumberFormatException | ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}

		request.setAttribute("c", e);

		this.getServletContext().getRequestDispatcher(BASEURL + "/ajouterPoints.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractDAOFactory adf = AbstractDAOFactory.getFactory(AbstractDAOFactory.DAO_FACTORY);
		DAOMaison daoMaison = (DAOMaison) adf.getMaisonDAO();
		Maison maison = null;
		try {
			maison = daoMaison.findById(Integer.parseInt(request.getParameter("uid")));
			Integer newScore = maison.getScore() + Integer.parseInt(request.getParameter("score"));
			System.out.println(newScore);
			daoMaison.updateScore(Integer.parseInt(request.getParameter("uid")),newScore);
		} catch (NumberFormatException | ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}

		List<Maison> m = new ArrayList<Maison>();

		DAOMaison dm = (DAOMaison) adf.getMaisonDAO();
		try {
			m = dm.findAll();
			System.out.println(m);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		request.setAttribute("maisons", m);

		this.getServletContext().getRequestDispatcher(BASEURL + "/listeMaison.jsp").forward(request, response);
	}

}
