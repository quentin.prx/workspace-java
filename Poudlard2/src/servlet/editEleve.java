package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOEleve;
import dao.DAOMaison;
import model.Eleve;
import model.Maison;


@WebServlet("/editEleve")
public class editEleve extends HttpServlet {
	static String BASEURL = "/WEB-INF";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Eleve e = null;

		DAOEleve daoEleve = new DAOEleve();
		try {
			e = daoEleve.findById(Integer.parseInt(request.getParameter("uid")));
		} catch (NumberFormatException | ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		
		request.setAttribute("c", e);
		
		this.getServletContext().getRequestDispatcher(BASEURL + "/editEleve.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DAOMaison daoMaison = new DAOMaison();
		Maison maison = null;
		try {
			maison = daoMaison.findById(Integer.parseInt(request.getParameter("uidMaison")));
		} catch (NumberFormatException | ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}

		Eleve e = new Eleve(
				Integer.parseInt(request.getParameter("uid")),  
				(String) request.getParameter("nom"), 
				(String) request.getParameter("prenom"), 
				(int) Integer.parseInt(request.getParameter("age")),
				maison		
		);
		
		DAOEleve daoEleve = new DAOEleve();
		try {
			daoEleve.update(e);
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		
		request.setAttribute("c", e);
		
		this.getServletContext().getRequestDispatcher(BASEURL + "/editEleve.jsp").forward(request, response);
	}

}
