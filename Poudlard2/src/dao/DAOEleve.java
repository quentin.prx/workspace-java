package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.Eleve;
import model.Maison;

public class DAOEleve extends DAO<Eleve,Integer>{

	public DAOEleve(Connection conn) {
		super(conn);
	}

	public void create(Eleve e) throws ClassNotFoundException, SQLException {
		PreparedStatement ps=conn.prepareStatement("INSERT INTO eleve (nom,prenom,age,idMaisonEleve) VALUES (?,?,?,?)");
        ps.setString(1,e.getNom());
        ps.setString(2,e.getPrenom());
        ps.setInt(3,e.getAge());
        ps.setInt(4, e.getMaison().getIdMaison());
        ps.executeUpdate();
        ps.close();
        conn.close();
	}
	
	public Eleve findById(Integer id) throws ClassNotFoundException, SQLException {
		String sql = "SELECT * FROM eleve where idEleve=?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		
		Eleve e=null;
		while(rs.next()) {
			DAOMaison daoMaison = new DAOMaison(conn);
			Maison maison = daoMaison.findById(rs.getInt("idMaisonEleve"));
			e = new Eleve(rs.getInt("idEleve"),rs.getString("nom"),rs.getString("prenom"),rs.getInt("age"),maison);
		}
				
		rs.close();
		ps.close();
		conn.close();
		return e;
	}
	
	public Eleve findByName(String nom) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "");
		
		PreparedStatement ps=conn.prepareStatement("select * from eleve where nom=?");
		ps.setString(1,nom);
		ResultSet rs=ps.executeQuery();
		Eleve e=null;
		while(rs.next())
		{
			DAOMaison daoMaison = new DAOMaison(conn);
			Maison maison = daoMaison.findById(rs.getInt("idMaisonEleve"));
			e = new Eleve(rs.getInt("idEleve"),rs.getString("nom"),rs.getString("prenom"),rs.getInt("age"),maison);
		}
		rs.close();
		ps.close();
		conn.close();
		return e;
	}
	
	public List<Eleve> findAll() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "" );
		
		String sql = "SELECT * FROM eleve order by age, prenom, nom";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		List<Eleve> eleves = new ArrayList<Eleve>();
		while(rs.next()) {
				DAOMaison daoMaison = new DAOMaison(conn);
				Maison maison = daoMaison.findById(rs.getInt("idMaisonEleve"));
				eleves.add(new Eleve(rs.getInt("idEleve"), rs.getString("nom"), rs.getString("prenom"), rs.getInt("age"), maison));
		}
		
		rs.close();
		ps.close();
		conn.close();
		return eleves;
	}
	
	public String findAllJson() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "" );
		
		String sql = "SELECT * FROM eleve order by age, prenom, nom";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		List<Eleve> eleves = new ArrayList<Eleve>();
		while(rs.next()) {
				DAOMaison daoMaison = new DAOMaison(conn);
				Maison maison = daoMaison.findById(rs.getInt("idMaisonEleve"));
				eleves.add(new Eleve(rs.getInt("idEleve"), rs.getString("nom"), rs.getString("prenom"), rs.getInt("age"), maison));
		}
		
		rs.close();
		ps.close();
		conn.close();
		
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		
		System.out.println("el�ves "+gson.toJson(eleves));
		
		
		
		return gson.toJson(eleves);
	}
	
	
	public void update(Eleve e) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "" );		
		
        PreparedStatement ps=conn.prepareStatement("UPDATE eleve SET nom=?,prenom=?,age=?,idMaisonEleve=? where idEleve=?");
        ps.setString(1,e.getNom());
        ps.setString(2,e.getPrenom());
        ps.setInt(3,e.getAge());
        ps.setInt(4, e.getMaison().getIdMaison());
        ps.setInt(5, e.getIdEleve());
        ps.executeUpdate();
        ps.close();
        conn.close();
	}
	
	public void delete(Integer id) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/poudlard", "root", "" );		
		
        PreparedStatement ps=conn.prepareStatement("DELETE FROM eleve where idEleve=?");
        ps.setInt(1,id);
        ps.executeUpdate();
        ps.close();
        conn.close();
	}
}
