package dao;

import java.sql.Connection;

import connexion.Connexion;
import model.Eleve;
import model.Maison;
import model.Professeur;


public class DAOFactory extends AbstractDAOFactory {
	protected static final Connection conn = Connexion.getInstance();

	public DAO<Eleve,Integer> getEleveDAO() {
		return new DAOEleve(conn);
	}

	public DAO<Maison,Integer> getMaisonDAO() {
		return new DAOMaison(conn);
	}

	public DAO<Professeur,Integer> getProfesseurDAO() {
		return new DAOProfesseur(conn);
	}

}
