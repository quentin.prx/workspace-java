package dao;

import model.Eleve;
import model.Maison;
import model.Professeur;

public abstract class AbstractDAOFactory {
	public static final int DAO_FACTORY = 0;
	  public static final int XML_DAO_FACTORY = 1;

	  public abstract DAO<Eleve,Integer> getEleveDAO();
	  public abstract DAO<Maison,Integer> getMaisonDAO();
	  public abstract DAO<Professeur,Integer> getProfesseurDAO();
	   
	  public static AbstractDAOFactory getFactory(int type){
	    switch(type){
	      case DAO_FACTORY:
	        return new DAOFactory();
	      case XML_DAO_FACTORY: 
	        return new XMLDAOFactory();
	      default:
	        return null;
	    }
	  }
}
