package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Professeur;

public class DAOProfesseur extends DAO<Professeur,Integer> {

	public DAOProfesseur(Connection conn) {
		super(conn);
	}

	public void create(Professeur e) throws ClassNotFoundException, SQLException {
		PreparedStatement ps=conn.prepareStatement("INSERT INTO professeur (nom,matiere) VALUES (?,?)");
        ps.setString(1,e.getNom());
        ps.setString(2,e.getMatiere());
        ps.executeUpdate();
        ps.close();
        conn.close();
	}
	
	public Professeur findById(Integer id) throws ClassNotFoundException, SQLException {
		String sql = "SELECT * FROM Professeur where idProfesseur=?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		
		Professeur p=null;
		while(rs.next()) {
			p = new Professeur(rs.getInt("idProfesseur"),rs.getString("nom"),rs.getString("matiere"));	
		}
				
		rs.close();
		ps.close();
		conn.close();
		return p;
	}
	
	public Professeur findByName(String nom) throws ClassNotFoundException, SQLException {
		PreparedStatement ps=conn.prepareStatement("select * from professeur where nom=?");
		ps.setString(1,nom);
		ResultSet rs=ps.executeQuery();
		Professeur p=null;
		while(rs.next())
		{
			p = new Professeur(rs.getInt("idProfesseur"),rs.getString("nom"),rs.getString("matiere"));
		}
		rs.close();
		ps.close();
		conn.close();
		return p;
	}
	
	public List<Professeur> findAll() throws ClassNotFoundException, SQLException {
		String sql = "SELECT * FROM professeur order by nom";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		List<Professeur> profs = new ArrayList<Professeur>();
		while(rs.next()) {
				profs.add(new Professeur(rs.getInt("idProfesseur"), rs.getString("nom"), rs.getString("matiere")));		
		}
		
		rs.close();
		ps.close();
		conn.close();
		return profs;
	}
	
	public void update(Professeur e) throws ClassNotFoundException, SQLException {
		PreparedStatement ps=conn.prepareStatement("UPDATE professeur SET nom=?,matiere=? where idProfesseur=?");
        ps.setString(1,e.getNom());
        ps.setString(2,e.getMatiere());
        ps.setInt(3, e.getIdProf());
        ps.executeUpdate();
        ps.close();
        conn.close();
	}
	
	public void delete(Integer id) throws ClassNotFoundException, SQLException {
		PreparedStatement ps=conn.prepareStatement("DELETE FROM professeur where idProfesseur=?");
        ps.setInt(1,id);
        ps.executeUpdate();
        ps.close();
        conn.close();
	}

}
