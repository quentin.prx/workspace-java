package dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public abstract class DAO<T,K> {
	  protected Connection conn = null;
	   
	  public DAO(Connection conn){
	    this.conn = conn;
	  }
	  
	  public void seDeconnecterDAO() {
		  try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	  }
	  
	public abstract void create(T t) throws ClassNotFoundException, SQLException;
	public abstract T findById(K id) throws ClassNotFoundException, SQLException;
	public abstract List<T> findAll() throws ClassNotFoundException, SQLException;
	public abstract void update(T obj) throws ClassNotFoundException, SQLException;
	public abstract void delete(K id) throws ClassNotFoundException, SQLException;
}
