package metier;

public enum Civilite {
	
	HOMME("Homme"),
	FEMME("Femme"),
	AUTRE("Autre");
	
	private String sexe;
	
	private Civilite(String sexe) {
		// TODO Auto-generated constructor stub
		this.sexe = sexe;
	}
	
	public String getSexe() {
		return sexe;
	}
	
	public String toString() {
		return this.sexe;
	}

}
