package metier;

public class Rectangle {
	int longueur;
	int largeur;
	
	public Rectangle(int longueur,int largeur) {
		this.longueur = longueur;
		this.largeur = largeur;
	}
	
	public int perimetre() {
		int perimetre = (longueur+largeur)*2;
		System.out.println("Perimetre "+perimetre);
		return perimetre;
	}
	
	public int aire() {
		int aire = longueur*largeur;
		System.out.println("Aire "+aire);
		return aire;
	}
	
	public boolean isCarre() {
		return this.largeur == this.longueur;
	}

	public String toString() {
		String ch = "Rectangle :\n";
		ch += "Longueur : " + this.longueur + "cm\n";
		ch += "Largeur : " + this.largeur + "cm\n";
		ch += "Perimetre : " + this.perimetre() + "cm\n";
		ch += "Aire : " + this.aire() + "cm�";
		return ch; 
	}

	public int getLongueur() {
		return longueur;
	}

	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}

	public int getLargeur() {
		return largeur;
	}

	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}
	
	

}
