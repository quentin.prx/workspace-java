package metier;

public class Personne {
	private String nom;
	private String prenom;
	private Civilite sexe;
	private int age;
	private static int cptPersonne = 0;
	private Adresse adresse;

	
	
	public Personne(String prenom,String  nom,int age,Civilite sexe,String numero,String voie,String codePostal, String ville) {
		cptPersonne++;
		this.nom = nom.toUpperCase();
		this.prenom = prenom;
		this.age = age;
		this.sexe = sexe;
		this.adresse = new Adresse(numero, voie, codePostal, ville);
	}
	
	public Personne(String nom, String prenom, int age, Adresse adresse) {
		this.nom = nom.toUpperCase();
		this.prenom = prenom;
		this.age = age;
		this.adresse = adresse;
		cptPersonne++;
	}


	public Personne(String nom, int age){
		this.nom = nom.toUpperCase();
		this.prenom = "Toto";
		this.age = age;
		this.adresse = new Adresse("", "", "", "");
		cptPersonne++;
	}
	
	
	public Personne(int age, String nom){
		this.nom = nom.toUpperCase();
		this.prenom = "Toto";
		this.age = age;
		this.adresse = new Adresse("", "", "", "");
	}
	
	public Personne(String nom){
		this.nom = nom.toUpperCase();
		this.prenom = "Toto";
		this.age = 0;
		this.adresse = new Adresse("", "", "", "");
	}
	
	public static int getCptPersonne() {
		return cptPersonne;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
		public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public Adresse getAdresse() {
		return adresse;
	}


	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}


	public String toString() {
		return "Je m'appelle "+prenom+" "+nom+" j'ai "+age+" ans et je suis un(e) "+sexe+"\n" + adresse;
		//return "Personne [nom=" + nom + ", age=" + age + "]";
	}



}