package metier;

public class Cercle {
	private double rayon;
	private final double PI = Math.PI;
	
	public Cercle(double rayon) {
		this.rayon = rayon;
	}
	
	public double getRayon() {
		return this.rayon;
	}
	
	public void setRayon(double rayon) {
		this.rayon = rayon;
	}

	public double perimetre() {
		double perimetre = 2*PI*rayon;
		System.out.println("Perimetre "+perimetre);
		return perimetre;
	}
	
	public double aire() {
		double aire = PI*(rayon*rayon);
		System.out.println("Aire : "+aire);
		return aire;
	}
	
	public String toString() {
		String ch = "Cercle :\n";
		ch += "Cercle : " + this.rayon + "cm\n";
		ch += "Perimetre : " + this.perimetre() + "cm\n";
		ch += "Aire : " + this.aire() + "cm�";
		return ch; 
	}
	
}
