package metier;

public class Adresse {
	private String numero;
	private String voie;
	private String codePostal;
	private String ville;
	
	public Adresse(String numero, String voie, String codePostal, String ville) {
		this.numero = numero;
		this.voie = voie;
		this.codePostal = codePostal;
		this.ville = ville;
	}
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getVoie() {
		return voie;
	}
	public void setVoie(String voie) {
		this.voie = voie;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	
	
	public String toString() {
		return numero + " " + voie + "\n" + codePostal + " " + ville;
	}
	
	
	
	
}
