package test;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

import metier.Personne;

public class Test {
	
	public static Scanner scInt = new Scanner(System.in);
	public static Scanner scString = new Scanner(System.in);

	public static double moyenneAge(Personne personnes[]) {
		double moy = 0.0;
		
		for(Personne p : personnes)
			moy += (double)p.getAge();
		
		moy /= (double)personnes.length;
		
		return moy;
	}
	
	public static Personne[] initPersonnes(int nb) {
		Personne personnes[] = new Personne[nb];
		
		System.out.println("Saisir "+nb+" personnes :");
		for(int i=0;i<personnes.length;i++) {
			System.out.println("Personne n�"+(i+1));
			System.out.println("Donnez votre nom :");
			String nom = scString.next();
			System.out.println("Donnez votre �ge :");
			int age = scInt.nextInt();
			personnes[i] = new Personne(nom, age);
		}
		
		return personnes;
	}
	
	
	public static List<Personne> initPersonnesList(int nb) {
		List<Personne> personnes = new ArrayList<Personne>();
		
		System.out.println("Saisir "+nb+" personnes :");
		int i = 0;
		while(i < nb) {
			System.out.println("Personne n�"+(i+1));
			System.out.println("Donnez votre nom :");
			String nom = scString.next();
			System.out.println("Donnez votre �ge :");
			int age = scInt.nextInt();
			personnes.add(new Personne(nom, age));
			i++;
		}
		
		return personnes;
	}
	
	public static void afficherPersonnes(Personne[] personnes) {
		System.out.println("Afficher les "+personnes.length+" personnes :");
		for(Personne p : personnes)
			System.out.println(p);
	}

	public static void afficherPersonnesList(List<Personne> personnes) {
		System.out.println("Afficher les "+personnes.size()+" personnes :");
		for(Personne p : personnes)
			System.out.println(p);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Personne personnes[] = initPersonnes(4);
		List<Personne> personnesList = new ArrayList<Personne>();
		personnesList = initPersonnesList(4);
		
		//afficherPersonnes(personnes);
		
		afficherPersonnesList(personnesList);
		
		//Remove une personne
		System.out.println("Choisir la personne ?");
		int nb = 1;
		for(Personne p : personnesList) {
			System.out.println((nb++)+" - "+p.getNom());
		}
		int choix = scInt.nextInt();
		Personne p2 = personnesList.get(choix-1);
		System.out.println(p2);
		personnesList.remove(p2);
		afficherPersonnesList(personnesList);
		
		//Nombre de personne
		System.out.println("Il y a "+Personne.getCptPersonne()+" personnes");
		
		//System.out.println("La moyenne d'�ge est de "+moyenneAge(personnes));
		
	}

}
