package test;

import java.util.Scanner;

import metier.Adresse;
import metier.Civilite;
import metier.Personne;

public class Test2 {
	public static void main(String[] args) {
		Adresse adr = new Adresse("4","Rue Gambetta","37000","Tours");
		Personne pers = new Personne("Abid","Jordan",25,adr);
		System.out.println(pers);
		
		Personne pers2 = new Personne("Marcel", "Proust", 32,Civilite.HOMME, "4 B", "Rue Gambetta", "37000", "Tours");
		System.out.println(pers2);
		
		System.out.println("Qui suis-je ?");
		Scanner sc = new Scanner(System.in);
		String civ = sc.nextLine();
		Personne pers3 = new Personne("Marcel", "Proust", 32,Civilite.valueOf(civ), "4 B", "Rue Gambetta", "37000", "Tours");
		System.out.println(pers3);
	}
}
